# pse_frontend

The frontend component of the PSE app.

## Development Setup

1. You need a recent NodeJS and NPM version (tested with Node v12.9.0 and NPM 6.10.2).

```bash
$ node -v
v12.9.0
$ npm -v
6.10.2
```

2. Install frontend project dependencies:

```bash
$ npm install
```

## Running the app

To start the development server:

```bash
$ npm run start
```

## App Structure

App built with `bulma` as css framework and `lit-html` for HTML rendering.

* `app.js`: Responsible for rendering the DOM on state change
* `router`: Writes current path and params to UI state
* `store/api.js`: Backend calls
* `store/store`: Managing UI state and backend data
* `ui`: Templates, Components and Component factories
* `util`: Libraries
* `util/component`: Prototype for all components. Look here to understand how our component concept works.


Project comes with some eslint rules. `npm run check` and fix errors before merging to develop.
