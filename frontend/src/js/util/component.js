import {TemplateResult} from 'lit-html';

export const Component = {
  /**
   * Is called before the very first render to set initial states.
   */
  init() {
    this.initChildren();
  },
  /**
   * Can be used in overwritten init functions.
   */
  initChildren() {
    if (!this._children) {
      return;
    }
    for (let i = 0; i < this._children.length; i++) {
      const child = this._children[i];
      child.init();
    }
  },
  /**
   * The model function calculates the state of a component.
   * A component must implement its own model function.
   * Typically, a model fetches data from uiState and store
   * and should handle cases, where the store does not (yet)
   * provide backend data.
   * @return { Object } model
   */
  model() {
    return {};
  },

  /**
   * The view function renders a template based on model data.
   * A component must implement its own view function.
   * @param {Object} model
   * @throws Will throw an error if view function of root prototype is called.
   * @return {TemplateResult} Rendered lit-html template.
   * */
  view(model) {
    console.log('WARNING: Trying to render a component without view!');
    return new TemplateResult();
  },

  /**
   * The render function passes the component model into the view.
   * In general, there is no need for a component to overwrite this function.
   * @return {TemplateResult} Component view according to component model.
   */
  render() {
    return (this.view(this.model()));
  },
  /**
   * Anything that needs to happen after DOM is in place.
   */
  onRendered() {
    this.onRenderedChildren();
  },
  /**
   * Can be used to notify children if onRendered is overWritten.
   */
  onRenderedChildren() {
    if (!this._children) {
      return;
    }
    for (let i = 0; i < this._children.length; i++) {
      const child = this._children[i];
      child.onRendered();
    }
  },
  /**
   * Register a child component so it gets notified on init and
   * onRendered.
   * @param {Component} child
   */
  registerChild(child) {
    if (!this._children) {
      this._children = [];
    }
    this._children.push(child);
  },
};
