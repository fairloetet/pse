import {url} from '../util/config.js';
import {store} from './store';
/**
 * API call to create a new entity as specified by key
 * (products, materials, ...)
 * @param {String} key Key specifying the type of entity created.
 * @param {Object} data Additional data as required by backend.
 * @param {Function} cb Callback to execute after backend call.
 */
export async function create(key, data, cb) {
  const payload = {data};
  const request = {
    method: 'POST',
    body: JSON.stringify(payload),
  };
  const fetchURL = url + key;
  const callback = async function(response) {
    if (cb) {
      return cb(response);
    } else {
      return store.fetchListFromBackend(key);
    }
  };
  return callBackend(fetchURL, request).then(callback);
}
/**
 * API call to delete an entity.
 * @param {String} key Key specifying the type of entity deleted.
 * @param {Number} id ID of the entity.
 */
export async function destroy(key, id) {
  const request = {
    method: 'DELETE',
  };
  const fetchUrl = url + key + '/' + id;
  const callback = async function() {
    return store.fetchListFromBackend(key);
  };
  return callBackend(fetchUrl, request, () => {}).then(callback);
}
/**
 * API call to update an entity.
 * @param {String} key Specifies the type of entity to update.
 * @param {Number} id The id of the entity.
 * @param {Object} data Data to update.
 * @param {String} update Whether to call backend to
 * fetch new values for 'single' entity or full 'list'.
 */
export async function update(key, id, data, update = 'single') {
  const payload = {data};
  const fetchUrl = url + key + '/' + id;
  const request = {
    method: 'PATCH',
    body: JSON.stringify(payload),
  };

  const callback = async function() {
    if (update == 'single') {
      return store.fetchItemFromBackend(key, id);
    } else if (update == 'list') {
      return store.fetchListFromBackend(key);
    }
  };
  return callBackend(fetchUrl, request, () => {}).then(callback);
}

/**
 * Update a relationship between two entities
 * (Currently only product <->component)
 * @param {String} parentKey Specify the type of entity to update.
 * @param {Number} parentId Id of the entity.
 * @param {Number} childId Id of child entity.
 * @param {Object} data Fields of the assignment.
 */
export async function updateRelationship(parentKey, parentId, childId, data) {
  const payload = {data};
  const fetchUrl = url + 'products/{product_id}/components/' + childId +
  '?component_id=' + parentId;
  const request = {
    method: 'PATCH',
    body: JSON.stringify(payload),
  };

  const callback = async function() {
    return store.fetchItemFromBackend(parentKey, parentId);
  };
  return callBackend(fetchUrl, request, () => {}).then(callback);
}
/**
 * API function to specify a relationship between
 * a parent entity and children.
 * @param {String} parentKey Specifies the type of the parent entity.
 * @param {String} childKey Specifies the type of the child entity.
 * @param {Number} parentId ID of parent entity.
 * @param {Number} data Relationship data.
 */
export async function set(parentKey, childKey, parentId, data) {
  const payload = {data};
  const fetchUrl = url + parentKey + '/' + parentId + '/' + childKey + '/set';
  const request = {
    method: 'POST',
    body: JSON.stringify(payload),
  };
  return callBackend(fetchUrl, request, () => {});
}
/**
 * API call to specify a one-to-many relationship of two entities.
 * Adds one child.
 * @param {*} parentKey Specifies type of parent entity.
 * @param {*} childKey Specifies type of child entity.
 * @param {*} parentId ID of parent entity.
 * @param {*} childId ID of child entity.
 */
export async function add(parentKey, childKey, parentId, childId) {
  const payload = {data: {component_id: childId, amount: 1}};
  const fetchUrl = url + parentKey + '/' + parentId + '/' + childKey + '/add';
  const request = {
    method: 'POST',
    body: JSON.stringify(payload),
  };
  const callback = async function() {
    return store.fetchItemFromBackend(parentKey, parentId).then(() => {
      return store.fetchItemFromBackend(childKey, childId);
    });
  };
  return callBackend(fetchUrl, request, () => {}).then(callback);
}
/**
 * API call to specify a one-to-many relationship of two entities.
 * Removes one child.
 * @param {String} parentKey Specifies type of parent entity.
 * @param {String} childKey Specifies type of child entitiy.
 * @param {Number} parentId ID of parent entity.
 * @param {Number} childId ID of child entity.
 */
export async function remove(parentKey, childKey, parentId, childId) {
  const payload = {data: childId};
  const request = {
    method: 'POST',
    body: JSON.stringify(payload),
  };
  const fetchUrl = url + parentKey + '/' +
    parentId + '/' + childKey + '/remove';
  const callback = async function() {
    return store.fetchItemFromBackend(parentKey, parentId).then(() => {
      return store.fetchItemFromBackend(childKey, childId);
    });
  };
  return callBackend(fetchUrl, request, () => {}).then(callback);
}
/**
 * API call to specify an ordered-list relationship of two entities.
 * Adds one child.
 * @param {*} parentKey Specifies type of parent entity.
 * @param {*} childKey Specifies type of child entity.
 * @param {*} parentId ID of parent entity.
 * @param {*} childId ID of child entity.
 */
export async function addToOrderedList(parentKey, childKey, parentId, childId) {
  const payload = {data: {component_id: childId, amount: 1}};
  const fetchUrl = url + parentKey + '/' + parentId + '/' + childKey;
  const request = {
    method: 'POST',
    body: JSON.stringify(payload),
  };
  const callback = async function() {
    return store.fetchItemFromBackend(parentKey, parentId).then(() => {
      return store.fetchItemFromBackend(childKey, childId);
    });
  };
  return callBackend(fetchUrl, request, () => {}).then(callback);
}
/**
 * API call to specify an ordered-list relationship of two entities.
 * Removes one assignment.
 * @param {*} parentKey Specifies type of parent entity.
 * @param {*} childKey Specifies type of child entity.
 * @param {*} parentId ID of parent entity.
 * @param {*} assignmentId ID of assignment object.
 */
export async function removeFromOrderedList(parentKey, childKey,
    parentId, assignmentId) {
  const fetchUrl = url + parentKey + '/' + parentId + '/' +
    childKey + '/' + assignmentId;
  const request = {
    method: 'DELETE',
  };
  const callback = async function() {
    return store.fetchItemFromBackend(parentKey, parentId);
  };
  return callBackend(fetchUrl, request, () => {}).then(callback);
}
/**
 * Performs a call to backend.
 * @param {String} fetchUrl Url to call.
 * @param {Object} request Request to send.
 * @param {Function} callback To be called on success.
 */
async function callBackend(fetchUrl, request) {
  return fetch(fetchUrl, request)
      .then(handleErrors)
      .then((response) => {
        if (response.status == 201) {
          return response.json();
        } else {
          return response;
        }
      })
      .catch((error) => console.log('API ERROR: ' + error));
}
/**
 * Throws error if HTTP response is not ok.
 * @param {Object} response HTTP response.
 * @throws {Error} if response is not ok.
 * @return {Object} Unmodified HTTP response.
 */
function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
