import {url} from '../util/config.js';
import {updateUI} from '../../app.js';
/**
 * This component is responsible for managing the ui state.
 */
export const uiState = {
  /**
   * Returns a stored value.
   * @param {String} key The key of the value to retrieve.
   * @return {*} The stored value.
   */
  get(key) {
    return this['_' + key];
  },
  /**
   * Set a value for a given key.
   * Set silent to true if router should not be notified of changes.
   * @param {String} key
   * @param {*} value
   * @param {Boolean} silent
   */
  set(key, value, silent) {
    this['_' + key] = value;
    if (!silent) {
      this.notify();
    }
  },
  /**
   * Calls router to update view.
   */
  notify() {
    updateUI();
  },
};

/**
 * This component is responsible for managing backend data.
 */
export const store = {
  /**
   * Fetches a report for a product from backend.
   * Will store the result and notify router.
   * @param {Number} id Product ID:
   */
  async fetchReportFromBackend(id) {
    return fetch(url + 'products/' + id + '/report')
        .then((resp) => resp.json())
        .then((json) => {
          this['report-' + id] = json.data;
          this.notify();
        });
  },
  /**
   * Fetches an entity from backend.
   * Will store the result and notify router.
   * @param {String} key Specify the type of entity.
   * @param {Number} id The id of the entity.
   */
  async fetchItemFromBackend(key, id) {
    return fetch(url + key + '/' + id)
        .then((resp) => resp.json())
        .then((json) => {
          this[key + '-' + id] = json.data;
          this[key + '-' + id].included = json.included;
          this.notify();
        });
  },
  /**
   * Fetches a list of entities from backend.
   * Will store the list locally and notify router.
   * @param {String} key Specify type of entities to fetch.
   */
  async fetchListFromBackend(key) {
    return fetch(url + key)
        .then((resp) => resp.json())
        .then((json) => {
          this[key] = json.data;
          this.notify();
        });
  },
  /**
   * Get a specific entity.
   * Will trigger a backend fetch if not stored locally.
   * @param {String} key Specify type of entity.
   * @param {Number} id ID of the entity.
   * @return {*}
   */
  getItem(key, id) {
    if (!this[key + '-' + id]) {
      this.fetchItemFromBackend(key, id);
      this[key + '-' + id] = {loading: true};
    }
    return this[key + '-' + id];
  },
  /**
   * Get a list of entities.
   * Will trigger a backend fetch if list is not stored locally.
   * @param {String} key Specify the type of entities.
   * @return {Array}
   */
  getList(key) {
    if (!this[key]) {
      this.fetchListFromBackend(key);
      this[key] = {loading: true};
    }
    return this[key];
  },
  /**
   * Get a report for a product.
   * Will trigger backend fetch if report is not stored locally.
   * @param {Number} id Product ID.
   * @return {Object} The report for the product.
   */
  getReport(id) {
    if (!this['report-' + id]) {
      this.fetchReportFromBackend(id);
      this['report-' + id] = {loading: true};
    }
    return this['report-' + id];
  },
  /**
   * Delete a stored report (e.g. after product configuration has changed)
   * @param {Number} id Product ID.
   */
  invalidateReport(id) {
    delete this['report-' + id];
  },
  /**
   * Delete a stored list.
   * @param {Number} key List ID.
   */
  invalidateList(key) {
    delete this[key];
  },
  /**
   * Calls router to update view.
   */
  notify() {
    updateUI();
  },
};
