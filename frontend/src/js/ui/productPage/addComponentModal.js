import {html} from 'lit-html';
import {uiState, store} from '../../store/store';
import {Component} from '../../util/component';
import {buildModal} from '../shared/modalFactory';
import {addToOrderedList} from '../../store/api';
import {repeat} from 'lit-html/directives/repeat';
import Fuse from 'fuse.js';

export const component = Object.create(Component);
component.modalId = 'configure';
component.init = function() {
  uiState.set('configure-selected-components', [], true);
  uiState.set('components-search-term', '', true);
};
component.model = function() {
  let components = store.getList('components');
  components = !components.loading ? components : [];
  uiState.set('filtered-components', components, true);
  const productId = uiState.get('params').productId;

  let searchTerm = uiState.get('components-search-term') ;
  if (searchTerm) {
    let options = {
      caseSensitive: true,
      shouldSort: true,
      threshold: 0.6,
      location: 0,
      distance: 100,
      findAllMatches: true,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: [
        "name",
        "slug"
      ]
    };
    let fuse = new Fuse(components, options); // "list" is the item array

    components = fuse.search(searchTerm);
  }

  const isActive = uiState.get(this.modalId + '-modalIsActive');
  return {productId, components, isActive};
};
component.view = function({productId, components, isActive}) {
  const modalBody = html`
    <article class="panel">
      <p class="panel-heading">
      Component Library
      </p>
      <div class="panel-block">
      <p class="control has-icons-left">
      <input class="input" type="text" placeholder="Search"
      @keyup=${() => onSearchInput(event.target)}
      >
      </p>
      </div>

      ${repeat(
      components,
      (component) => component.id,
      (component) => html`
          <label 
            @click=${() => {}}
          class="panel-block">
            <input id="checkbox-${component.id}" type="checkbox" 
            @change=${
  (context) => onCheckBoxClick(context.target, component.id)}>
            ${component.name} (${component.weight} g)
          </label>
        `,
  )}
    </article>`;
  const modal = buildModal(this.modalId, 'Add Component to Product',
      modalBody, 'Add selected', () => {
        onAddComponentsToProductClick(productId);
      });
  return modal(isActive);
};

/**
 * Call Backend to add Components to Product.
 * @param {Number} productId The product to add components to.
 */
async function onAddComponentsToProductClick(productId) {
  const components = uiState.get('configure-selected-components');
  for (let i = 0; i < components.length; i++) {
    document.getElementById('checkbox-' + components[i]).checked = false;
    await addToOrderedList('products', 'components', productId, components[i]);
  }
  uiState.set('configure-selected-components', []);
}

/**
 * Add an component id to a list in UI state.
 * This list is later processed when user wants to save selected components.
 * @param {HTMLElement} checkbox The corresponding checkbox.
 * @param {Number} componentId Id of the component.
 */
function onCheckBoxClick(checkbox, componentId) {
  let components = uiState.get('configure-selected-components');
  if (checkbox.checked) {
    components.push(componentId);
  } else {
    components = components.filter((item) => item !== componentId);
  }
  uiState.set('configure-selected-components', components, true);
}


function onSearchInput(input) {
  uiState.set('components-search-term', input.value);
}