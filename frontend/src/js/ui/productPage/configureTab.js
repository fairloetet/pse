import {html} from 'lit-html';
import {repeat} from 'lit-html/directives/repeat';
import {Component} from '../../util/component';
import {uiState, store} from '../../store/store';
import {component as addComponentModal} from './addComponentModal';
import {removeFromOrderedList, updateRelationship} from '../../store/api';
import {onNavItemClick} from '../../router/router';

export const component = Object.create(Component);
component.init = function() {
  uiState.set('configure-modalIsActive', false, true);
  this.initChildren();
};
component.model = function() {
  const productId = uiState.get('params').productId;
  let product = store.getItem('products', productId);
  if (!product.loading) {
    for (let i = 0; i < product.components.length; i++) {
      const assignment = product.components[i];
      const component = product.included.components.find((element) => {
        return element.id == assignment.component_id;
      });
      assignment.name = component.name;
      assignment.weight = component.weight;
    }
  } else {
    product = {components: []};
  }
  return {product};
};
component.view = function({product}) {
  const template = html`
    <div class="container configure">
    <div class="section">
    <h1 class="title is-1">Configure Product Components</h1>
    <h4 class="big-source">
    Configure your product using our component library.
    </h4>

    <table class="table is-fullwidth">
    <thead>
        <tr>
        <th>Component Name</th>
        <th>Weight</th>
        <th>Amount</th>
        <th>Actions</th>
        </tr>
    </thead>
    <tbody>
      ${repeat(
      product.components,
      (assignment) => assignment.id,
      (assignment) => html`
        <tr>
        <td> 
        ${assignment.name}
        </td>
        <td>
        ${assignment.weight} g
        </td>
        <td contenteditable 
        @input=${
  (context) => onAmountInput(context.target, assignment.id, product.id)}>
        ${assignment.amount}
        </td>
        <td>
        <span class="is-pulled-right">
            <a
            @click=${
  () => onRemoveComponentFromProductClick(assignment.id, product.id)}
              ><i class="material-icons">delete</i></a
            >
        </span>
        </td>
        </tr>
        `,
  )}
    </tbody>
    </table>
    </div>
    <div class="section">
    <div class="field">
    <div class="label small-source">
        Add missing parts from our library:
    </div>
    <div class="control">
      <button class="button animated"
      @click=${() => onOpenModalClick()}
      >Open Library</button>
    </div>
    </div>
    <div class="section is-clearfix">
    <button 
        @click=${() => onNavItemClick('/product/' + product.id + '/basic')}
        class="button animated is-primary is-large is-pulled-left">Back
    </button>

    <button 
        @click=${() => onNavItemClick('/product/' + product.id + '/review')}
        class="button animated is-primary is-large is-pulled-right">Next
    </button>
    </div>

    ${addComponentModal.render()}
`;
  return template;
};
component.registerChild(addComponentModal);

/**
 * Sends API call to remove component from product
 * @param {Number} assignmentId
 * @param {Number} productId
 */
function onRemoveComponentFromProductClick(assignmentId, productId) {
  removeFromOrderedList('products', 'components', productId, assignmentId);
}

/**
 * Change UI state to show modal
 */
function onOpenModalClick() {
  uiState.set('configure-modalIsActive', true);
}

/**
 * Call Backend to store changed component amounts
 * @param {HTMLElement} element Editable HTML Element.
 * @param {Number} assignmentId ID of product<->component assignment.
 * @param {Number} productId Product to be edited.
 */
function onAmountInput(element, assignmentId, productId) {
  const amount = parseInt(element.textContent);
  if (amount) {
    updateRelationship('products', productId,
        assignmentId, {amount}).then(() => {
      element.textContent = amount;
    });
  }
}
