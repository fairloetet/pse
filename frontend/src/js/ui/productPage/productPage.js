import {html} from 'lit-html';
import {template as header} from '../shared/header';
import {template as footer} from '../shared/footer';
import {component as configureTab} from './configureTab';
import {component as basicTab} from './basicTab';
import {component as reviewTab} from './reviewTab';
import {onNavItemClick} from '../../router/router';
import {uiState, store} from '../../store/store';
import {Component} from '../../util/component';


export const component = Object.create(Component);
component.model = function() {
  const params = uiState.get('params');
  const productId = params.productId;

  const activeTab = params.tab;
  let product = store.getItem('products', productId);
  if (product.loading) {
    product = {name: '', components: []};
  }

  return {product, activeTab};
};
component.view = function({product, activeTab}) {
  const template = html`
    ${header}
    <section class="section product-progress">
    <div class="container">
    <ul class="progress-tracker progress-tracker--text progress-tracker--center">
      <li class="progress-step  ${activeTab == 'basic' ? 'is-active' : 'is-complete'}"
          @click=${() => onNavItemClick('/product/' + product.id + '/basic')}
      >
        <div class="progress-marker big-source" data-text="1"></div>
        <div class="progress-text small-source">
            <span>Enter Basic Information</span>
        </div>
      </li>
      <li class="progress-step ${activeTab == 'configure' ? 'is-active' : (activeTab == 'review' ? 'is-complete' : '') }"
          @click=${() => onNavItemClick('/product/' + product.id + '/configure')}
      >
        <div class="progress-marker big-source" data-text="2"></div>
        <div class="progress-text small-source">
            <span>Configure Product Components</span>
        </div>
      </li>
      <li class="progress-step ${activeTab == 'review' ? 'is-active' : ''}"
          @click=${() => onNavItemClick('/product/' + product.id + '/review')}
      >
        <div class="progress-marker big-source" data-text="3"

        ></div>
        <div class="progress-text small-source">
            <span>Review</span>
        </div>
      </li>
  </ul>
    </div>
    </section>
    ${getTabTemplate(activeTab)}
    ${footer}
  `;
  return template;
};
component.registerChild(basicTab);
component.registerChild(configureTab);
component.registerChild(reviewTab);

/**
 * Triggers tab render.
 * @param {String} activeTab ID of active tab.
 * @return {*}
 */
function getTabTemplate(activeTab) {
  if (activeTab == 'basic') {
    return basicTab.render();
  } else if (activeTab == 'configure') {
    return configureTab.render();
  } else if (activeTab == 'review') {
    return reviewTab.render();
  }
}
