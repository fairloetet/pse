import {html} from 'lit-html';
import {repeat} from 'lit-html/directives/repeat';
import {Component} from '../../util/component';
import {uiState, store} from '../../store/store';
import {onNavItemClick} from '../../router/router';

export const component = Object.create(Component);
component.model = function() {
  const productId = uiState.get('params').productId;
  let product = store.getItem('products', productId);
  // Todo: maybe change to conditional render and introduce loading state
  if (!product.loading) {
    for (let i = 0; i < product.components.length; i++) {
      const assignment = product.components[i];
      const component = product.included.components.find((element) => {
        return element.id == assignment.component_id;
      });
      assignment.name = component.name;
      assignment.weight = component.weight;
    }
  } else {
    product = {components: []};
  }
  return {product};
};
component.view = function({product}) {
  const template = html`
    <div class="container review">
    <div class="section">
    <h1 class="title is-1">Review your product data</h1>
    <h4 class="big-source">
    Everything OK? Then head over to view your 
    <a @click=${() => onNavItemClick('/report/' + product.id)}>
    social impact report</a>.
    </h4>

    <div class="columns">
    <div class="column">
    <h4 class="title is-4">Product Name</h4>
    </div>
    <div class="column is-half small-source">
    ${product.name}
    </div>
    <div class="column">
    <button class="button animated"
      @click=${() => onNavItemClick('/product/' + product.id + '/basic')}
    >Edit</button>
    </div>
    </div>
    <div class="columns">
    <div class="column">
    <h4 class="title is-4">Product Description</h4>
    </div>
    <div class="column is-half small-source">
    ${product.description}
    </div>
    <div class="column">
    <button class="button animated"
      @click=${() => onNavItemClick('/product/' + product.id + '/basic')}
    >Edit</button>
    </div>
    </div>
    <div class="columns">
    <div class="column">
    <h4 class="title is-4">Product Components</h4>
    </div>
    <div class="column is-half small-source">
    <ul>
    ${repeat(
      product.components,
      (assignment) => assignment.id,
      (assignment) => html`
        <li>
        ${assignment.amount} X 
        ${assignment.name} (${assignment.weight} g)
        </li>
        `,
  )}
    </ul>
    </div>
    <div class="column">
    <button class="button animated"
      @click=${() => onNavItemClick('/product/' + product.id + '/configure')}
    >Edit</button>
    </div>
    </div>

    <button class="button animated" 
    @click=${() => onNavItemClick('/report/' + product.id)}>
    View Report</button>
    </div>
    <div class="section is-clearfix">
    <button 
        @click=${() => onNavItemClick('/product/' + product.id + '/configure')}
        class="button animated is-primary is-large is-pulled-left">Back
    </button>
    </div>

    </div>

`;
  return template;
};
