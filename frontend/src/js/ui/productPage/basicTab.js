import {html} from 'lit-html';
import {Component} from '../../util/component';
import {uiState, store} from '../../store/store';
import {onNavItemClick} from '../../router/router';
import {update} from '../../store/api';

export const component = Object.create(Component);
component.model = function() {
  const productId = uiState.get('params').productId;
  let product = store.getItem('products', productId);
  if (product.loading) {
    product = {components: []};
  }
  return {product};
};
component.view = function({product}) {
  const template = html`
    <div class="container basic">
    <div class="section">
    <h1 class="title is-1">Enter Basic Information</h1>
    <h4 class="big-source">
    Enter some basic information about your product to get started.
    </h4>
    <div class="field">
        <label class="label small-source">Product Name</label>
        <div class="control">
          <input
            id="input-product-name"
            class="input"
            type="text"
            placeholder="Product Name"
            @keyup=${() => onFormKeypress(product.id)}
            value=${product.name}
          />
        </div>

        <p class="help">The name of the product you create a report for.</p> 
    </div>
    <div class="field">
        <label class="label small-source">Product Description</label>
        <div class="control">
          <textarea id="textarea-product-description" 
          class="textarea" placeholder="Product Description"
          @keyup=${() => onFormKeypress(product.id)}
          >${product.description}</textarea>
        </div>

        <p class="help">
        Describe your product in a few sentences for your report readers.
        </p> 
    </div>
    </div>

    <div class="section is-clearfix">
    <button 
        @click=${() => onNavItemClick('/product/' +
            product.id + '/configure', product.id)}
    class="button animated is-primary is-large is-pulled-right">Next</button>
    </div>

    </div>`;
  return template;
};


let timeoutId;
/**
 * Saves product fields when user stops typing.
 * @param {Number} productId ID of product data to save.
 */
function onFormKeypress(productId) {
  // If a timer was already started, clear it.
  if (timeoutId) clearTimeout(timeoutId);

  // Set timer that will save comment when it fires.
  timeoutId = setTimeout(function() {
    const name = document.getElementById('input-product-name').value;
    const description =
      document.getElementById('textarea-product-description').value;
    const payload = {name, description};

    update('products', productId, payload, 'single');
    store.invalidateList('products');
  }, 750);
}
