import {html, TemplateResult} from 'lit-html';
import {repeat} from 'lit-html/directives/repeat';
import {uiState, store} from '../../store/store';
import {destroy, create, update, set} from '../../store/api';
import {Component} from '../../util/component';
import {buildModal} from '../shared/modalFactory';
/**
 * Creates a new tab page with edit functionality for entities
 * like components, materials, ...
 * @param {String} primaryKey Specify type of entity
 * @param {String} secondaryKey Specify type of relationship entity
 * @param {String} primaryName Readable name for UI
 * @param {String} secondaryName Readable name for UI
 * @param {Array} primaryFields ids of entity properties
 * @return {Component}
 */
export function buildAdminTab(primaryKey, secondaryKey,
    primaryName, secondaryName, primaryFields) {
  const createModal = buildCreateModal(primaryKey, primaryName, primaryFields);
  const editModal = buildEditModal(primaryKey,
      secondaryKey, primaryName, secondaryName, primaryFields);
  const component = Object.create(Component);
  component.init = function() {
    uiState.set(createModal.modalId + '-modalIsActive', false, true);
    uiState.set(editModal.modalId + '-modalIsActive', false, true);
  };
  component.model = function() {
    let primaryEntities = store.getList(primaryKey);
    const createModalActive = uiState.get(createModal.modalId +
      '-modalIsActive');
    const editModalActive = uiState.get(editModal.modalId + '-modalIsActive');
    primaryEntities = !primaryEntities.loading ? primaryEntities : [];
    return {primaryEntities, createModalActive, editModalActive};
  };
  component.view = function({primaryEntities, createModalActive,
    editModalActive}) {
    return html`
    <section class="section">
      <div class="container">
        <div class="level">
          <div class="level-left">
            <h1 class="title is-1 level-item">${primaryName} List</h1>
          </div>
          <div class="level-right">
            <button
              @click=${() => onOpenCreateModalClick()}
              class="button level-item"
            >
              Add ${primaryName}
            </button>
          </div>
        </div>
      </div>
    </section>
  
    <section class="section">
      <div class="container">
        <table class="table is-fullwidth">
          <thead>
            <tr>
              <th>${primaryName}</th>
              <th>
                <span class="is-pulled-right">
                  Actions
                </span>
              </th>
            </tr>
          </thead>
          <tbody>
            ${repeat(
      primaryEntities,
      (primaryEntity) => primaryEntity.id,
      (primaryEntity) => html`
                <tr>
                  <td>
                    ${primaryEntity.name}
                  </td>
                  <td>
                    <span class="is-pulled-right">
                      <a
                        @click=${() => onDeleteEntityClick(primaryEntity.id)}
                        ><i class="material-icons">delete</i></a
                      ><a
                        @click=${() => onOpenEditModalClick(primaryEntity.id)}
                        ><i class="material-icons">edit</i></a
                      >
                    </span>
                  </td>
                </tr>
              `,
  )}
          </tbody>
        </table>
      </div>
    </section>
    ${createModalActive ? createModal.render() : ''}
    ${editModalActive ? editModal.render() : ''}
  `;
  };
  /**
   * Opens create modal.
   */
  function onOpenCreateModalClick() {
    uiState.set(createModal.modalId + '-modalIsActive', true);
  }
  /**
   * Fires a backend call to delete a component;
   * @param {Number} id
   */
  function onDeleteEntityClick(id) {
    destroy(primaryKey, id);
  }
  /**
   * Opens an edit modal specifying which component to edit.
   * @param {Number} id
   */
  function onOpenEditModalClick(id) {
    uiState.set(editModal.modalId + '-' + primaryKey + '-id', id);
    uiState.set(editModal.modalId + '-modalIsActive', true);
  }
  return component;
}
/**
 * Builds a modal to create a new entity.
 * @param {String} key Secify type of entity
 * @param {name} name Readable name for entity
 * @param {Array} fields Array of field ids
 * @return {Component}
 */
function buildCreateModal(key, name, fields) {
  const component = Object.create(Component);
  component.modalId = 'create-'+ key + '-modal',
  component.model = function() {
    const isActive = uiState.get(this.modalId + '-modalIsActive');
    return {isActive};
  };
  component.view = function({isActive}) {
    const modalBody = html`
    ${repeat(fields,
      (field) => field,
      (field) => html`
      <div class="field">
        <label class="label">${name} ${field}</label>
        <div class="control">
          <input
            id="input-${key}-${field}"
            class="input"
            type="text"
            placeholder="${name} "
          />
        </div>`,
  )}
      </div>
    `;
    const view = buildModal(this.modalId, 'Create new ' + name,
        modalBody, 'Save ' + name, onCreateClick);
    return view(isActive);
  };
  /**
   * Triggers a backend call to create a new entity.
   */
  function onCreateClick() {
    const entity = {};
    for (let i = 0; i < fields.length; i++) {
      const field = fields[i];
      const value = document.getElementById('input-' + key + '-' + field).value;
      document.getElementById('input-' + key + '-' + field).value = '';
      entity[field] = value;
    }
    create(key, entity);
    uiState.set(component.modalId + '-modalIsActive', false);
  }
  return component;
}

/**
 * Creates a modal to edit entity properties
 * @param {String} primaryKey Specify type of entity
 * @param {String} secondaryKey Specify type of relationship entity
 * @param {String} primaryName Readable name for UI
 * @param {String} secondaryName Readable name for UI
 * @param {Array} fields ids of entity properties
 * @return {Component}
 */
function buildEditModal(primaryKey, secondaryKey,
    primaryName, secondaryName, fields) {
  const component = Object.create(Component);
  component.modalId = 'edit-' + primaryKey + '-modal';
  component.model = function() {
    const id = uiState.get(this.modalId + '-' + primaryKey + '-id');
    let primaryEntities = store.getList(primaryKey);
    let secondaryEntities = store.getList(secondaryKey);
    secondaryEntities = !secondaryEntities.loading ? secondaryEntities : [];
    primaryEntities = !primaryEntities.loading ? primaryEntities : [];
    let entity = primaryEntities.find((elem) => elem.id == id);
    entity = entity ? entity : {};

    for (let i = 0; i < secondaryEntities.length; i++) {
      secondaryEntities[i].amount = getAmount(secondaryEntities[i].id,
          entity[secondaryKey], secondaryKey);
    }
    return {entity, secondaryEntities};
  };
  component.view = function({entity, secondaryEntities}) {
    const body = modalBody(entity, secondaryEntities);
    const callback = () => onCloseModalClick(entity.id);
    const view = buildModal(this.modalId,
        'Edit ' + primaryName, body, 'Close', callback, false);
    return view(true);
  };

  /**
   * Closes Modal
   */
  function onCloseModalClick() {
    uiState.set(component.modalId + '-edit-modalIsActive', false);
    store.invalidateList(primaryKey);
  }

  /**
   * Triggers a backend call to store
   * @param {*} input Editable HTML Element.
   * @param {*} primaryId ID of primary entity in relationship.
   * @param {*} secondaryId ID of secondary entity in relationship.
   */
  function onAmountInput(input, primaryId, secondaryId) {
    const data = [];
    const amount = input.textContent;
    if (parseFloat(amount) || parseInt(amount) === 0) {
      // only update if amount is different than 0.0
      const secondaryIdentifier = getSecondaryIdentifier(secondaryKey);
      const entry = {};
      entry[secondaryIdentifier] = secondaryId;
      entry['amount'] = parseFloat(amount);
      data.push(entry);
    }
    set(primaryKey, secondaryKey, primaryId, data).then(() => {
      input.textContent = amount;
    });
  }

  let timeoutId;
  /**
   * Triggers a backend call to store entity fields.
   * Only after user stopped typing.
   * @param {*} id ID of entity to update.
   */
  function onFormInput(id) {
    if (timeoutId) clearTimeout(timeoutId);
    timeoutId = setTimeout(() => {
      const payload = {};
      for (let i = 0; i < fields.length; i++) {
        const field = fields[i];
        const value = document.getElementById('edit-' +
          primaryKey + '-' + field).value;
        payload[field] = value;
      }
      update(primaryKey, id, payload, 'list');
    }, 750);
  }
  /**
   * Create the body of the edit modal.
   * @param {Object} entity
   * @param {Array} secondaryEntities
   * @return {TemplateResult}
   */
  const modalBody = (entity, secondaryEntities) => html`
    ${repeat(fields,
      (field) => field,
      (field) => html`
    <div class="field">
      <label class="label">${primaryName} ${field}</label>
      <div class="control">
        <input id="edit-${primaryKey}-${field}" 
            type="text" value=${entity[field]}
            @keyup=${() => onFormInput(entity.id)}
             />
      </div>
    </div>`,
  )}
    <table id="${primaryKey}-${secondaryKey}-table" class="table is-fullwidth">
      <thead>
        <tr>
          <th>${secondaryName}</th>
          <th>Amount</th>
        </tr>
      </thead>
      <tbody>
        ${repeat(
      secondaryEntities,
      (secondaryEntity) => secondaryEntity.id,
      (secondaryEntity) => html`
            <tr>
            <td>${secondaryEntity.name}</td>
            <td
              contenteditable="true"
              id=${primaryKey + '-' + secondaryKey +
                '-table-' + secondaryEntity.id}
              @input=${
  (context) => onAmountInput(context.target, entity.id, secondaryEntity.id)}
            >
            ${secondaryEntity.amount}
            </td>
            </tr>
          `,
  )}
      </tbody>
    </table>
  `;

  /**
   * Checks whether a value for the relationship is available.
   * Otherwise returns 0.0
   * @param {Number} secondaryId Entity ID
   * @param {Array} secondaryEntities List of entities
   * @param {String} secondaryKey Specify type of secondary entity
   * @return {Number}
   */
  function getAmount(secondaryId, secondaryEntities, secondaryKey) {
    const secondaryIdentifier = getSecondaryIdentifier(secondaryKey);
    const entry = secondaryEntities.find(
        (entry) => entry[secondaryIdentifier] == secondaryId);
    if (entry) {
      return entry.amount;
    } else {
      return 0.0;
    }
  }
  /**
   * Transform a type string to a field string for backend to consume
   * @param {String} secondaryKey
   * @return {String}
   */
  function getSecondaryIdentifier(secondaryKey) {
    let secondaryIdentifier = '';
    if (secondaryKey == 'materials') {
      secondaryIdentifier = 'material_id';
    } else if (secondaryKey == 'components') {
      secondaryIdentifier = 'component_id';
    } else if (secondaryKey == 'countries') {
      secondaryIdentifier = 'country_id';
    } else if (secondaryKey == 'indicators') {
      secondaryIdentifier = 'indicator_id';
    }

    return secondaryIdentifier;
  }

  return component;
}
