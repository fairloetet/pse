import {html, TemplateResult} from 'lit-html';
import {template as header} from '../shared/header';
import {uiState} from '../../store/store';
import {onNavItemClick} from '../../router/router';
import {Component} from '../../util/component';
import {buildAdminTab} from './adminTabFactory';

const componentTab = buildAdminTab('components',
    'materials', 'Component', 'Material', ['name', 'weight']);
const indicatorTab = buildAdminTab('indicators', 'countries',
    'Indicator', 'Country', ['name']);
const materialTab = buildAdminTab('materials', 'countries',
    'Material', 'Country', ['name']);
const countryTab = buildAdminTab('countries', 'indicators',
    'Country', 'Indicatory', ['name', 'code']);

export const component = Object.create(Component);
component.model = function() {
  const params = uiState.get('params');
  const activeTab = params.tab;
  return {activeTab};
};
component.view = function({activeTab}) {
  return html`
    ${header}
    <div class="tabs is-centered">
      <ul>
        <li
          class="${activeTab == 'materials' ? 'is-active' : ''}"
          @click=${() => onNavItemClick('/admin/materials')}
        >
          <a>Materials</a>
        </li>
        <li
          class="${activeTab == 'components' ? 'is-active' : ''}"
          @click=${() => onNavItemClick('/admin/components')}
        >
          <a>Components</a>
        </li>
        <li
          class="${activeTab == 'countries' ? 'is-active' : ''}"
          @click=${() => onNavItemClick('/admin/countries')}
        >
          <a>Countries</a>
        </li>
        <li
          class="${activeTab == 'indicators' ? 'is-active' : ''}"
          @click=${() => onNavItemClick('/admin/indicators')}
        >
          <a>Indicators</a>
        </li>
      </ul>
    </div>

    ${getTabTemplate(activeTab)}
    `;
};
component.registerChild(componentTab);
component.registerChild(indicatorTab);
component.registerChild(materialTab);
component.registerChild(countryTab);
/**
 * Triggers tab render.
 * @param {String} activeTab ID of active tab.
 * @return {TemplateResult}
 */
function getTabTemplate(activeTab) {
  if (activeTab == 'materials') {
    return materialTab.render();
  } else if (activeTab == 'components') {
    return componentTab.render();
  } else if (activeTab == 'countries') {
    return countryTab.render();
  } else if (activeTab == 'indicators') {
    return indicatorTab.render();
  }
}
