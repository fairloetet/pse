import {html} from 'lit-html';
import {repeat} from 'lit-html/directives/repeat';
import {template as header} from '../shared/header';
import {template as footer} from '../shared/footer';
import {destroy, create} from '../../store/api';
import caticon from '../../../img/icon-thecat.png';
import defaulticon from '../../../img/icon-theapproach.png';
import {onNavItemClick} from '../../router/router';
import {store} from '../../store/store';
import {Component} from '../../util/component';

const icon = CATMODE ? caticon : defaulticon;

export const component = Object.create(Component);
component.model = function() {
  let products = store.getList('products');
  products = !products.loading ? products : [];
  return {products};
};
component.view = function({products}) {
  return html`
  ${header}
  <div class="homepage">
  <section class="section">
    <div class="container">
      <div class="level">
        <div class="level-left">
          <h1 class="title is-1 level-item">Product List</h1>
        </div>
        <div class="level-right">
          <button @click=${() => onCreateProductClick()} 
          class="button animated level-item">
            Create product
          </button>
        </div>
      </div>
    </div>
  </section>
  <section class="section">
    <div class="container">
      <div class="" style="">
        ${repeat(
      products,
      (product) => product.id,
      (product) => html`
            <div class="" style="width: 14rem; display: inline-block; margin: 1rem;">
              <div class="card" style="">
                <header class="card-header product">
                  <p
                    @click=${() => onNavItemClick('/product/' +
                      product.id + '/basic')}
                    class="card-header-title"
                  >${product.name?product.name:html`<i>Untitled Product ${product.id}</i>`}</p>
                </header>
                <div
                  @click=${() => onNavItemClick('/product/' + product.id +
                    '/basic')}
                  class="card-content"
                >
                  <img src=${icon} />
                </div>
                <footer class="card-footer">
                <a class="card-footer-item"
                    @click=${() => onNavItemClick('/product/' + product.id +
                      '/basic')}
                    >Edit</a>

                  <a class="card-footer-item"
                    @click=${() => onNavItemClick('/report/' + product.id)}
                    >Report</a>

                  <a class="card-footer-item has-text-danger"
                    @click=${() => onDeleteProductClick(product.id)}
                    >Delete</a>
                </footer>
              </div>
            </div>
          `,
  )}
      </div>
    </div>
  </section>
  </div>
  ${footer}

`;
};

/**
 * Sends a call to API to delete product
 * @param {Number} productId
 */
function onDeleteProductClick(productId) {
  destroy('products', productId);
}

/**
 * Sends a call to API to create a new product.
 */
function onCreateProductClick() {
  create('products', {
    'name': '',
    'description': '',
  }, (response) => {
    const productID = response.data.id;
    onNavItemClick('/product/' + productID + '/basic');
    store.invalidateList('products');
  });
}
