import {html, TemplateResult} from 'lit-html';
import {uiState} from '../../store/store';

/**
 * Factory function to create a new modal to be used in a view.
 * @param {String} id Unique id of the modal.
 * @param {String} title Title text.
 * @param {TemplateResult} body The body of the modal.
 * @param {String} buttonText Text for the success button.
 * @param {Function} successCallback Function to call on success button click.
 * @param {Boolean} showCancel Whether to show Cancel buttons.
 * @return {TemplateResult} The full modal.
 */
export function buildModal(id, title, body, buttonText,
    successCallback, showCancel = true) {
  const templateFunction = (isActive) => html`
  <div id="${id}" class="modal ${isActive ? 'is-active' : ''}">
    <div class="modal-background"></div>
    <div class="modal-card">
      <header class="modal-card-head">
        <p class="modal-card-title">${title}</p>

        ${
          showCancel?
          html`
        <button
          @click=${() => onCancelClick(id)}
          class="delete"
          aria-label="close"
        ></button>
          `: ''}
      </header>
      <section class="modal-card-body">
       ${body} 
      </section>
      <footer class="modal-card-foot">
        <button
          @click=${() => onSaveClick(id, successCallback)}
          class="button is-success"
        >
        ${buttonText}
        </button>
        ${
          showCancel?
          html`
          <button @click=${() => onCancelClick(id)} class="button">
          Cancel
          </button>
          `: ''}
      </footer>
    </div>
  </div>
`;
  return templateFunction;
}

/**
 * A click on a modal executes the given callback and closes the modal.
 * @param {String} id The modal ID.
 * @param {Function} successCallback Function to call.
 */
function onSaveClick(id, successCallback) {
  successCallback();
  uiState.set(id + '-modalIsActive', false);
}

/**
 * Close the modal.
 * @param {String} id The modal ID.
 */
function onCancelClick(id) {
  uiState.set(id + '-modalIsActive', false);
}

