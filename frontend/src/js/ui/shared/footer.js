import {html} from 'lit-html';

export const template = html`
<footer class="footer has-background-white">
    <div class="has-text-centered">
      <a href="https://fairtronics.org/imprint">Imprint</a>
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      &nbsp;
      <a href="https://fairtronics.org/privacy">Privacy</a>
    </div>
</footer>`;
