import {html} from 'lit-html';
import {onNavItemClick} from '../../router/router';
import logo from '../../../assets/logo/logo-light.svg';

export const template = html`
  <nav class="navbar is-fixed-top has-background-blue-yonder is-spaced" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
      <a @click=${() => onNavItemClick('')} 
        class="navbar-item">
            <img src=${logo} alt="Fairtronics"></a>
    </div>
    <div class="navbar-start">
        <a class="navbar-item" href="${ HOMEPAGE }/browse">Browse Reports</a>
        <a class="navbar-item"i @click=${() => onNavItemClick('/product/create')}>
    Create Report
    </a>
    <a class="navbar-item" href="${ HOMEPAGE }/how-it-works">
    How it works
    </a>
    </div>
    <div class="navbar-end">
     <div class="is-invisible" class="navbar-item">
        <a class="right" 
          @click=${() => onNavItemClick('/admin/materials')}>Admin</a>
     </div
     ><div class="navbar-item has-dropdown is-hoverable right">
        <a class="navbar-item">
           <i class="material-icons" style="vertical-align: middle; display: inline-block; padding-right: 0.25rem;">account_circle</i>Demo-User
        </a>
        <div class="navbar-dropdown">
        <a class="navbar-item" style="color: black;"
        @click=${() => onNavItemClick('/')}
        >My Products</a>
        </div>
      </div>
    </div>
  </nav>
`;
