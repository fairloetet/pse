import {html} from 'lit-html';
import {template as header} from '../shared/header';
import {Component} from '../../util/component';
import {repeat} from 'lit-html/directives/repeat';
import {uiState, store} from '../../store/store';
import {template as footer} from '../shared/footer';
import highIcon from '../../../assets/report/high_risk.svg';
import mediumIcon from '../../../assets/report/medium_risk.svg';
import lowIcon from '../../../assets/report/low_risk.svg';
import unknownIcon from '../../../assets/report/unknown_risk_v2.svg';
import componentIcon from '../../../assets/report/icon-component.svg';
import countryIcon from '../../../assets/report/icon-country.svg';
import materialIcon from '../../../assets/report/icon-material.svg';
import summaryIcon from '../../../assets/report/icon-summary.svg';
import {colorPalette} from '../../../styles/echarts-theme.js';

const echarts = require('echarts');

export const component = Object.create(Component);
component.model = function() {
  const productId = uiState.get('params').productId;

  let product = store.getItem('products', productId);
  product = !product.loading ? product : {};

  const report = store.getReport(productId);
  this.report = report;

  if (!report.loading && report.components.length > 0 &&
    report.materials.length > 0 && report.countries.length > 0) {
    // assemble hotspot data
    const componentHotspots = filterHotspots(report.components);
    const materialHotspots = filterHotspots(report.materials);
    const countryHotspots = filterHotspots(report.countries);
    const hotspots = {componentHotspots, materialHotspots, countryHotspots};
    console.log(product);
    console.log(report);
    return {product, report, product, hotspots};
  } else {
    return {
      product,
      report: {
        components: [{risks: []}],
        materials: [{risks: []}],
        countries: [{risks: []}],
        weight: 0.0
      },
      hotspots: {
        componentHotspots: [],
        materialHotspots: [],
        countryHotspots: [],
      }};
  }
};
component.view = function({product, report, hotspots}) {
  const template = html`
    ${header}
    ${
        report.components[0] ?
        html`
    <section class="section report small-source">
      <div class="container">
        <div class="columns">
        <div class="column is-one-quarter">
        <aside class="menu sticky">
            <p class="menu-label">Outline</p>
            <ul class="menu-list">
                <li><a class="is-uppercase" @click=${() => onOutlineClick('title-summary')}>Summary</a></li>
                <li><a class="is-uppercase" @click=${() => onOutlineClick('title-components')}>Component Hotspots</a></li>
                <li><a class="is-uppercase" @click=${() => onOutlineClick('title-materials')}>Material Hotspots</a></li>
                <li><a class="is-uppercase" @click=${() => onOutlineClick('title-countries')}>Country Hotspots</a></li>
            </ul>
            <p class="menu-label">Labels</p>
            <ul class="menu-list">
                <li style="margin-bottom: 1rem;"><p style="vertical-align: middle;"><img width="80" src=${highIcon} style="vertical-align: middle;"> = High Risk</p></li>
                <li style="margin-bottom: 1rem;"><p style="vertical-align: middle;"><img width="80" src=${mediumIcon} style="vertical-align: middle;"> = Medium Risk</p></li>
                <li style="margin-bottom: 1rem;"><p style="vertical-align: middle;"><img width="80" src=${lowIcon} style="vertical-align: middle;"> = Low Risk</p></li>
                <li style="margin-bottom: 1rem;"><p style="vertical-align: middle;"><img width="80" src=${unknownIcon} style="vertical-align: middle;"> = Unknown Risk</p></li>
            </ul>
        </aside>
        </div>
        <div class="column ">

        <h1 class="title is-1 is-spaced">
          Report for ${product.name}
        </h1>
      
        <div class="report-headline">
            <img src=${summaryIcon} style="vertical-align: middle; display: inline-block; padding-right: 0.5rem;">
            <h2 id="title-summary" class="title is-2" style="vertical-align: middle; display: inline-block;">Summary</h2>
        </div>

        <p class="">
        ${product.description}
        </p>
      <br/>
        <p class="">
        The analysis of 
        <b>${product.name}</b> covers 
        <b>${report.components.length} components</b>,
        <b>${report.materials.length} materials</b> 
        and considers social impacts in 
        <b>${report.countries.length} countries</b>.
        </p>

        <div class="report-headline">
            <img src=${componentIcon} style="vertical-align: middle; display: inline-block; padding-right: 0.5rem;">
            <h2 id="title-components" class="title is-2" style="vertical-align: middle; display: inline-block;">Component Hotspots</h2>
        </div>
        <h3 class="title is-3">Components by share in weight</h3>
        <div class="box columns">
          <div class="column">
          <p>
          Total weight: ${report.weight.toFixed(2)} g<br/><br/>
          </p>

          <p>
          The tree map shows the contribution of each 
          component to the total weight considered in the analysis.
          </p>
          </div>
          <div class="column">
            <div class="chart" id="component-chart" 
            style="width:600px; height: 400px"></div>
          </div>
        </div>
        <h3 class="title is-3">
        Components with high weight contribution and severe impacts
        </h3>
        ${repeat(
            hotspots.componentHotspots,
            (hotspot) => hotspot.id,
            (hotspot) => html`
        <div class="box">
        <div class="columns">
          <div class="column">
          <h4 class="title is-4">${hotspot.name}</h4>
          <p>Share in weight: ${(hotspot.activity * 100).toFixed(2)}%</p>
          </div>
          <div class="column">
          <table class="table is-bordered">
              <thead>
                  <tr>
                      <th>Impact Category</th>
                      <th>Risk Level</th>
                  </tr>
              </thead>
              <tbody>
              ${repeat(
                hotspot.risks,
                (risk) => risk.name,
                (risk) => html`
              <tr>
                  <td>${risk.name}</td>
                  <td>
                  <img width="80" 
                  src=${risk.level === 'HIGH' ?
                  highIcon : (risk.level === 'LOW' ? lowIcon : (risk.level === 'MEDIUM' ? mediumIcon : unknownIcon))}
                  >
                  </td>
              </tr>
                  `,
            )}
              </tbody>
          </table>
          </div>

        </div>
        </div>
            `,
        )}
        <h3 class="title is-3">Component overview</h3>
        <div class="box">
          <div class="table-container" style="width: 800px">
          <table class="table" style="width: 2000px">
            <thead>
                <tr>
                    <th style="width: 200px">Component</th>
                    <th style="width: 200px">Share in weight</th>
                    ${repeat(
            report.components[0].risks,
            (risk) => risk.name,
            (risk) => html`
                          <th style="width: 200px">${risk.name}</th>
                          `,
        )}
                  </tr>
              </thead>
              <tfoot>
                  <tr>
                      <th>Component</th>
                      <th>Share in weight</th>
                      ${repeat(
            report.components[0].risks,
            (risk) => risk.name,
            (risk) => html`
                          <th>${risk.name}</th>
                          `,
        )}
                 </tr>
              </tfoot>
              <tbody>
              ${repeat(
            report.components,
            (component) => component.id,
            (component) => html`
                <tr>
                    <th>${component.name}</th>
                    <td>${(component.activity * 100).toFixed(2)}%</td>
                    ${repeat(
                component.risks,
                (risk) => risk.name,
                (risk) => html`
                <td>
                <img width="80" 
                src=${risk.level === 'HIGH' ?
                  highIcon : (risk.level === 'LOW' ? lowIcon : (risk.level === 'MEDIUM' ? mediumIcon : unknownIcon))}
                >
                </td>
                `,
            )}
              </tr>
              `,
        )}
            </tbody>
          </table>
          </div>
          <div class="table-fader"></div>
        </div>
        <div class="report-headline">
            <img src=${materialIcon} style="vertical-align: middle; display: inline-block; padding-right: 0.5rem">
            <h2 id="title-materials" class="title is-2" style="vertical-align: middle; display: inline-block;">Material Hotspots</h2>
        </div>
        <h3 class="title is-3">Materials by share in weight</h3>
        <div class="box columns">
            <div class="column">
            <p>
            Total weight: ${report.weight.toFixed(2)} g 
            </p>

            <p>
            The tree map shows the contribution of each material
            to the total weight considered in the analysis.
            </p>
            </div>
            <div class="column">
                <div class="chart" id="material-chart" 
                style="width:600px; height: 400px"></div>
            </div>
        </div>

        <h3 class="title is-3">
        Materials with high weight contribution and severe impacts
        </h3>
        ${repeat(
            hotspots.materialHotspots,
            (hotspot) => hotspot.id,
            (hotspot) => html`
        <div class="box">
        <div class="columns">
            <div class="column">
            <h4 class="title is-4">${hotspot.name}</h4>
            <p>Share in weight: ${(hotspot.activity * 100).toFixed(2)}%</p>
            </div>
            <div class="column">
            <table class="table is-bordered">
                <thead>
                    <tr>
                        <th>Impact Category</th>
                        <th>Risk Level</th>
                    </tr>
                </thead>
                <tbody>
                ${repeat(
                hotspot.risks,
                (risk) => risk.name,
                (risk) => html`
                <tr>
                    <td>${risk.name}</td>
                    <td>
                    <img width="80"
                    src=${risk.level === 'HIGH' ?
                    highIcon : (risk.level === 'LOW' ? lowIcon : (risk.level === 'MEDIUM' ? mediumIcon : unknownIcon))}
                    </td>
                </tr>
                `,
            )}
                </tbody>
            </table>
            </div>
        </div>
        </div>
            `,
        )}
 
        <h3 class="title is-3">Material overview</h3>
        <div class="box">
            <div class="table-container" style="width: 800px">
            <table class="table" style="width: 2000px">
                <thead>
                    <tr>
                        <th style="width: 200px">Material</th>
                        <th style="width: 200px">Share in weight</th>
                        ${repeat(
            report.materials[0].risks,
            (risk) => risk.name,
            (risk) => html`
                            <th style="width: 200px">${risk.name}</th>
                            `,
        )}
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Material</th>
                        <th>Share in weight</th>
                        ${repeat(
            report.materials[0].risks,
            (risk) => risk.name,
            (risk) => html`
                            <th>${risk.name}</th>
                            `,
        )}
                   </tr>
                </tfoot>
                <tbody>
                ${repeat(
            report.materials,
            (material) => material.id,
            (material) => html`
                  <tr>
                      <th>${material.name}</th>
                      <td>${(material.activity * 100).toFixed(2)}%</td>
                      ${repeat(
                material.risks,
                (risk) => risk.name,
                (risk) => html`
                      <td>
                      <img width="80" 
                      src=${risk.level === 'HIGH' ?
                    highIcon : (risk.level === 'LOW' ? lowIcon : (risk.level === 'MEDIUM' ? mediumIcon : unknownIcon))}>
                      </td>
                      `,
            )}
                  </tr>
                  `,
        )}
                </tbody>
            </table>
            </div>
        </div>

        <div class="report-headline">
            <img src=${countryIcon} style="vertical-align: middle; display: inline-block; padding-right: 0.5rem;">
            <h2 id="title-countries" class="title is-2" style="vertical-align: middle; display: inline-block;">Country Hotspots</h2>
        </div>
        <h3 class="title is-3">Country production by share in weight</h3>
        <div class="box columns">
            <div class="column">
            <p>
            Total weight: ${report.weight.toFixed(2)} g 
            </p>

            <p>
            The tree map shows the contribution of each country production 
            to the total weight considered in the analysis.
            </p>
            </div>
            <div class="column">
                <div class="chart" id="country-chart" 
                style="width:600px; height: 400px"></div>
            </div>
        </div>
        <h3 class="title is-3">
        Country with high weight contribution and severe impacts
        </h3>
        ${repeat(
            hotspots.countryHotspots,
            (hotspot) => hotspot.id,
            (hotspot) => html`
        <div class="box">
        <div class="columns">

            <div class="column">
            <h4 class="title is-4">${hotspot.name}</h4>
            <p>Share in weight: ${(hotspot.activity * 100).toFixed(2)}%</p>
            </div>
            <div class="column">
            <table class="table is-bordered">
                <thead>
                    <tr>
                        <th>Impact Category</th>
                        <th>Risk Level</th>
                    </tr>
                </thead>
                <tbody>
                ${repeat(
                hotspot.risks,
                (risk) => risk.name,
                (risk) => html`
                  <tr>
                      <td>${risk.name}</td>
                      <td>
                      <img width="80" 
                      src=${risk.level === 'HIGH' ?
                    highIcon : (risk.level === 'LOW' ? lowIcon : (risk.level === 'MEDIUM' ? mediumIcon : unknownIcon))}>
                      </td>
                  </tr>
                  `,
            )}
                </tbody>
            </table>
            </div>
        </div>
        </div>
            `,
        )}
 
        <h3 class="title is-3">Country overview</h3>
        <div class="box">
            <div class="table-container" style="width: 800px">
            <table class="table" style="width: 2000px">
                <thead>
                    <tr>
                        <th style="width: 200px">Component</th>
                        <th style="width: 200px">Share in weight</th>
                        ${repeat(
            report.countries[0].risks,
            (risk) => risk.name,
            (risk) => html`
                            <th style="width: 200px">${risk.name}</th>
                            `,
        )}
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Component</th>
                        <th>Share in weight</th>
                        ${repeat(
            report.countries[0].risks,
            (risk) => risk.name,
            (risk) => html`
                            <th>${risk.name}</th>
                            `,
        )}
                   </tr>
                </tfoot>
                <tbody>
                ${repeat(
            report.countries,
            (country) => country.id,
            (country) => html`
               <tr>
                      <th>${country.name}</th>
                      <td>${(country.activity * 100).toFixed(2)}%</td>
                      ${repeat(
                country.risks,
                (risk) => risk.name,
                (risk) => html`
                      <td>
                      <img width="80" 
                      src=${risk.level === 'HIGH' ?
                      highIcon : (risk.level === 'LOW' ? lowIcon : (risk.level === 'MEDIUM' ? mediumIcon : unknownIcon))}>
                      </td>
                      `,
            )}
                  </tr>
                  `,
        )}
                </tbody>
            </table>
            </div>
        </div>

      </div> <!-- column -->
      </div> <!-- columns -->
      </div> <!-- container -->
    </section>
        ` :
        ''
}

    ${footer}
  `;
  return template;
};
component.onRendered = function() {
  const report = this.report;
  if (report.loading) return;
  const componentChart = echarts.init(
      document.getElementById('component-chart'));
  const componentData = [];
  for (let i = 0; i < report.components.length; i++) {
    componentData.push({
      name: report.components[i].name,
      value: report.components[i].activity,
    });
  }
  const componentOptions = {
    series: [
      {
        type: 'treemap',
        data: componentData,
        levels: [{
          color: colorPalette
        }]

      },
    ],
  };
  componentChart.setOption(componentOptions);

  const materialChart = echarts.init(document.getElementById('material-chart'));
  const materialData = [];
  for (let i = 0; i < report.materials.length; i++) {
    materialData.push({
      name: report.materials[i].name,
      value: report.materials[i].activity,
    });
  }

  const materialOptions = {
    series: [
      {
        type: 'treemap',
        data: materialData,
        levels: [{
          color: colorPalette
        }]

      },
    ],
  };
  materialChart.setOption(materialOptions);

  const countryChart = echarts.init(document.getElementById('country-chart'));
  const countryData = [];
  for (let i = 0; i < report.countries.length; i++) {
    countryData.push({
      name: report.countries[i].name,
      value: report.countries[i].activity,
    });
  }

  const countryOptions = {
    series: [
      {
        type: 'treemap',
        data: countryData,
        levels: [{
          color: colorPalette
        }]
      },
    ],
  };
  countryChart.setOption(countryOptions);

  this.onRenderedChildren();
};

/**
 * Jump to corresponding section when user clicks on Outline.
 * @param {String} titleID ID of HTMl element to jump to.
 */
function onOutlineClick(titleID) {
  const titleElement = document.getElementById(titleID);
  titleElement.scrollIntoView();
}

/**
 * Extracts hotspots from an array of entities
 * (components, countries, materials).
 * Returns up to two entities with highest activity.
 * Filters risks to include only those on total level.
 * By default, checks HIGH level first, then MEDIUM, then LOW.
 * @param {Array} entities Entities to filter.
 * @param {String} level Risk level to filter for.
 * @return {Array} filtered Hotspots.
 */
function filterHotspots(entities, level = 'HIGH') {
  let hotspots = [];
  for (let i = 0; i < entities.length; i++) {
    if (entities[i].riskLevel == level) {
      hotspots.push({...entities[i]});
    }
  }

  if (hotspots.length > 0) {
    hotspots = hotspots.sort((el1, el2) => {
      return el2.activity - el1.activity;
    });
    hotspots = hotspots.slice(0, 2);
    for (let i = 0; i < hotspots.length; i++) {
      hotspots[i].risks = hotspots[i].risks.filter((risk) =>
        risk.level == level);
    }
    return hotspots;
  } else if (level == 'LOW') {
    return hotspots;
  } else {
    level = level == 'HIGH' ? 'MEDIUM' : 'LOW';
    return filterHotspots(entities, level);
  }
}
