import Navigo from 'navigo';
import {store,uiState} from '../store/store';
import {create} from '../store/api';


let router;
/**
 * Set up url paths for the app.
 * Paths and params will be written to uiState.
 */
export function initRouter() {
  const root = 'localhost:8080';
  const useHash = true;
  const hash = '#!';
  router = new Navigo(root, useHash, hash);

  router.on({
    'product/:productId/:tab': function(params) {
      uiState.set('params', params, true);
      uiState.set('path', 'product');
    },
    'product/create': function() {
      create('products', {
        'name': '',
        'description': '',
      }, (response) => {
        const productID = response.data.id;
        onNavItemClick('/product/' + productID + '/basic');
        store.invalidateList('products');
      });
    },
    'admin/:tab': function(params) {
      uiState.set('params', params, true);
      uiState.set('path', 'admin');
    },
    'report/:productId': function(params) {
      uiState.set('params', params, true);
      uiState.set('path', 'report');
    },
    '*': function(params) {
      uiState.set('params', params, true);
      uiState.set('path', 'home');
    },
  }).resolve();
}

// to enable following links
export const onNavItemClick = (path) => {
  router.navigate(path);
};
