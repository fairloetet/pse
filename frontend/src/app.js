import './styles/styles.scss';

import 'bulma-divider/dist/css/bulma-divider.min.css';
import './js/router/router';
import {render} from 'lit-html';
import {uiState} from './js/store/store';
import {component as homePage} from './js/ui/homePage/homePage';
import {component as productPage} from './js/ui/productPage/productPage';
import {component as adminPage} from './js/ui/adminPage/adminPage';
import {component as reportPage} from './js/ui/reportPage/reportPage';
import {initRouter} from './js/router/router';
import {Component} from './js/util/component';

const pages = {
};
/**
 * Register new pages to be used by router.
 * Pages are the root components and initialized here
 * (passing init() on to all their registered children)
 * @param {String} key
 * @param {Component} page
 */
function registerPage(key, page) {
  pages[key] = page;
  page.init();
}
registerPage('product', productPage);
registerPage('admin', adminPage);
registerPage('report', reportPage);
registerPage('home', homePage);

initRouter();
/**
 * To be called whenever a new render of components is necessary.
 */
export function updateUI() {
  const page = pages[uiState.get('path')];
  render(page.render(), document.body);
  page.onRendered();
}
