# Fairtronics

This project is composed out of the [frontend](frontend/README.md) and
[backend](backend/README.md) subprojects. See the corresponding README files for
details on each project.

## Running

The components of the Fairtronics app are provided as Docker images. Follow
these steps to start up the app using `docker-compose`.

1. **Prerequisites**

    Install `docker` and `docker-compose`. On Ubuntu 18.04,
    you can use this command:

    ```bash
    # apt install docker.io
    ```

    It is recommended to add your user to the `docker` group so you can run
    commands without being root:

    ```bash
    # usermod -G docker -a <your user>
    ```

2. **Running**

    This command builds and starts the Fairtronics app.

    ```bash
    $ ./up.sh
    ```

    **NOTE:** The database content is currently persisted in a Docker volume
    called `pse_pgdata`.

3. **Using**

    When the app is running, you can:

        - Access the frontend att `http://localhost:8080`
        - Access the backend documentation at `http://localhost:8000/docs`.

## Development Setup

See the [frontend](frontend/README.md) and [backend](backend/README.md) README
files for information on how to start developing the respective components.

### Git Hooks

For convenience, this repository includes a `pre-push` git hook which you can
use to ensure you never forget to run tests before pushing. To set up the git
hook in your local git repository, use the provided install script:

```bash
$ ./install-git-hooks.sh
```

**NOTE:** Make sure you have completed the *Development Setup* for the backend
first!

The pre-push hook will run the `quicktest.sh` script, which executes some quick
checks and tests.


## License

Fairtronics is free software: you can redistribute it and/or modify it under the
terms of the GNU Affero General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

Fairtronics is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

For licensing terms, see the file `LICENSE.md`  or
[https://www.gnu.org/licenses/agpl-3.0.html](https://www.gnu.org/licenses/agpl-3.0.html).
