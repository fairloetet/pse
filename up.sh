#!/usr/bin/env bash
#
# Starts the Fairtronics app locally in development mode.
#
# Rebuilds everything, from data to Docker images.

set -e

# Preprocess data
cd data
make prepare
make
cd ..

cp -r data/output/* backend/data/

# Start the backend
docker-compose up --build
