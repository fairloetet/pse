#!/usr/bin/env bash
# Creates a database on a postgresql server

set -e

HOST=$1

DB_NAME=pse
DB_PASSWORD=pse-dev-passwd

# Create the pse database if it doesn't exist
# Magic SQL from https://stackoverflow.com/a/18389184
echo "SELECT 'CREATE DATABASE ${DB_NAME}' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = '${DB_NAME}')\gexec" \
    | PGPASSWORD=${DB_PASSWORD} psql -h ${HOST} -p 5432 -U postgres

# Migrate the database if necessary
poetry run alembic upgrade head
