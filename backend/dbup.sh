#!/usr/bin/env bash
# Starts a local database server for development.

set -e

DB_NAME=pse
DB_PASSWORD=pse-dev-passwd
VOLUME_NAME=pse-dev-db

# Create the volume if it doesn't exist
docker volume create pse-dev-db

docker run --rm --name psedb -p 5432:5432 \
    --mount type=volume,src=${VOLUME_NAME},target=/var/lib/postgresql/data/pgdata \
    -e POSTGRES_PASSWORD=${DB_PASSWORD} \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -d postgres

sleep 3

./createdb.sh localhost
