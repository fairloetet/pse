import csv
import sys
from contextlib import closing
from typing import TextIO, Dict

from pse_backend import PseException
from pse_backend.db import get_database, Database
from pse_backend.db.crud_ops import UpdateT, CrudOps
from pse_backend.db.entity_ops import EntityOps
from pse_backend.db.exceptions import CreateError, UpdateError, NotUniqueError


def import_entities_from_csv(entity_name: str, csv_file: TextIO, update: bool = True):
    """
    Import entity instances from a CSV file.

    Each row in the CSV file is created as a new entity. Columns must be labeled with the entity's field names.
    If an entity cannot be created, it is skipped.

    :param entity_name: name of the entity to import, e.g. "country"
    :param csv_file: CSV file object to read from
    :param update: whether to overwrite (update) existing entities sharing the same slug
    :raises: EntityImportError if the CSV file is incorrectly formatted
    """
    with closing(get_database()) as db:
        import_entities_from_csv_into_db(db, entity_name, csv_file, update)


def import_entities_from_csv_into_db(db: Database, entity_name: str, csv_file: TextIO, update: bool = True) -> Dict[str, int]:
    """
    Import entity instances from a CSV file.

    Each row in the CSV file is created as a new entity. Columns must be labeled with the entity's field names.
    If an entity cannot be created, it is skipped.

    :param db: database to import into
    :param entity_name: name of the entity to import, e.g. "country"
    :param csv_file: CSV file object to read from
    :param update: whether to overwrite (update) existing entities sharing the same slug
    :raises: EntityImportError if the CSV file is incorrectly formatted
    :returns: a dictionary of 'success', 'updated', 'created' counts
    """

    entity_ops: EntityOps = db.get_entity(entity_name)
    # The spec defines the data structures for new entities and entity updates.
    # Alias them for easy access.
    new_type = entity_ops.new_type
    update_type = entity_ops.update_type

    error_counter = 0
    updated_counter = 0
    created_counter = 0
    csv_reader = csv.DictReader(csv_file)

    if 'slug' not in csv_reader.fieldnames:
        raise EntityImportError('The mandatory field "slug" is missing.')

    for row in csv_reader:
        try:
            # Should we try to perform an update?
            if update:
                # Create representation of update from the CSV row
                update_rep = update_type(**row)
                # Try to update
                updated = update_entity_instance(entity_ops, update_rep)

                if updated:
                    updated_counter += 1
                    continue

            # Try to create the entity

            # Create representation of new entity from the CSV row
            entity_rep = new_type(**row)
            # Create
            entity_ops.create(entity_rep)
            created_counter += 1
        except CreateError as e:
            print(f'Unable to create {entity_name} {row["slug"]}. Maybe the entity already exists?\n'
                  f'Details: {e.message}', file=sys.stderr)
            error_counter += 1
            continue
        except NotUniqueError as e:
            print(f'Unable to create/update {entity_name} {row["slug"]} because of a uniqueness constraint.\n'
                  f'Details: {e.message}', file=sys.stderr)
            error_counter += 1
            continue
        except UpdateError as e:
            print(f'Unable to update {entity_name} {row["slug"]}.\n'
                  f'Details: {e.message}', file=sys.stderr)
            error_counter += 1
            continue

    print(f'Imported {updated_counter+created_counter} entities '
          f'({created_counter} new, {updated_counter} updates)', file=sys.stderr)

    return {
        'error': error_counter,
        'created': created_counter,
        'updated': updated_counter
    }


def update_entity_instance(entity_ops: CrudOps, data: UpdateT) -> bool:
    """
    Try to update an existing entity.

    :param entity_ops: CrudOps object for the entity
    :param data: data representation of the entity

    :raise: NotUniqueError if there is more than one entity to be updated
    :raise: UpdateError if the update failed
    :return: True if an existing entity was updated. False if no entity existed.
    """
    # Get existing version of entity
    existing_entities = entity_ops.get_where({'slug': data.slug})

    # If entity does not exist, do not update
    if len(existing_entities) != 1:
        return False

    entity_ops.update(entity_id=existing_entities[0].id, update=data)
    return True


class EntityImportError(PseException):
    def __init__(self, message: str):
        PseException.__init__(self, message)
