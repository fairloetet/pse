import csv
from contextlib import closing
from typing import Sequence, List, TextIO, Mapping, TypeVar, Type, Dict, Optional

from pse_backend import PseException
from pse_backend.db import Database, get_database
from pse_backend.db.assignment_ops import AssignmentOps
from pse_backend.db.crud_ops import CrudOps
from pse_backend.db.exceptions import NotUniqueError

T = TypeVar('T')


class FieldNameError(PseException):
    def __init__(self, available_names: Sequence[str], candidates: List[str]):
        self.available_names = available_names
        self.candidates = candidates
        PseException.__init__(self,
                              f'Need one of the following fields to exist: {candidates}. '
                              f'Fields found: {available_names}')


def choose_field_name(available_names: Sequence[str], candidates: List[str]) -> str:
    """
    Choose the first available name from a list of candidates.

    :param available_names: list of field names that can be chosen
    :param candidates: list of candidates
    :return: one of candidates
    :raises: FieldNameError if there is no overlap between the two lists
    """
    for candidate in candidates:
        if candidate in available_names:
            return candidate
    raise FieldNameError(available_names, candidates)


def _entity_field_values(fields: List[str], row: Mapping[str, str]):
    """
    Extract type-prefixed entity fields from a CSV row.

    :param fields: List of prefixed field names, e.g. country.code
    :param row: row from CSVReader, must contain all fields
    :return: Dictionary of field name–value assignments with stripped type prefix.
    """
    return {field_name.split('.', 1)[-1]: row[field_name] for field_name in fields}


def _get_first_unique(items: List[T], query: Dict[str, str], entity_type: Type) -> T:
    if len(items) != 1:
        raise NotUniqueError(query, entity_type)
    return items[0]


def _extract_value_field(relationship: AssignmentOps, row: Dict[str, str]) -> Optional[str]:
    """
    Extracts the assignment value from a CSV row.

    :param relationship: relationship representing the assignment
    :param row: row in the CSV file as a dictionary
    :return: the value, or None if there is no assignment value
    """
    value_field = choose_field_name(list(row.keys()),
                                    ['value', relationship.value_column])
    return row[value_field]


def import_assignment(parent_entity_name: str,
                      relationship_name: str,
                      csv_file: TextIO,
                      parent_id: Optional[int] = None) -> None:
    """
    Load assignments from a CSV file.

    :param parent_entity_name: the parent of the assignment relationship
    :param relationship_name: the name of the relationship property
    :param csv_file: opened CSV file to load
    :param parent_id: optional ID of the parent entity – if given, it does not need to be specified in the CSV file
    :raises: FieldNameError if a required field is missing;
             EntityNotFoundError if any of the assignment targets does not exist
    """
    with closing(get_database()) as db:
        import_assignment_into_db(db, parent_entity_name, relationship_name, csv_file, parent_id)


def import_assignment_into_db(db: Database,
                              parent_entity_name: str,
                              relationship_name: str,
                              csv_file: TextIO,
                              parent_id: Optional[int] = None,
                              clear: bool = True) -> None:
    """
    Load assignments from a CSV file into a given database.

    :param db: database connection to use
    :param parent_entity_name: the parent of the assignment relationship
    :param relationship_name: the name of the relationship property
    :param csv_file: opened CSV file to load
    :param parent_id: optional ID of the parent entity – if given, it does not need to be specified in the CSV file
    :param clear: whether to clear all existing assignments for every parent entity in the imported data set
    :raises: FieldNameError if a required field is missing;
             EntityNotFoundError if any of the assignment targets does not exist
    """
    parent_entity_name = parent_entity_name.lower()
    relationship_name = relationship_name.lower()

    parent_ops: CrudOps = db.get_entity(parent_entity_name)
    relationship = db.get_relationship(parent_entity_name, relationship_name)
    child_entity_name = relationship.child_entity_name().lower()
    child_ops: CrudOps = db.get_entity(child_entity_name)

    csv_reader = csv.DictReader(csv_file)

    # Detect field names
    def is_parent_field(field_name: str) -> bool:
        return (field_name.lower().startswith('parent') or
                field_name.lower().startswith(parent_entity_name))

    def is_value_field(field_name: str) -> bool:
        return field_name.lower() in ['value', getattr(relationship, 'value_column', None)]

    def is_child_field(field_name: str) -> bool:
        return (field_name.lower().startswith('child') or
                field_name.lower().startswith(child_entity_name) or
                # Any column can be interpreted as a child column if the parent is pre-specified
                (parent_id is not None and not is_value_field(field_name)))

    parent_fields = [field_name for field_name in csv_reader.fieldnames if is_parent_field(field_name)]
    child_fields = [field_name for field_name in csv_reader.fieldnames if is_child_field(field_name)]

    # Clear existing assignments
    if clear:
        # TODO implement atomic behaviour: should only delete existing assignments if assignment import is successful
        # Assemble all parents mentioned in the file
        if parent_id is not None:
            parents = [parent_ops.get(parent_id)]
        else:
            parents = [parent_ops.get_where(_entity_field_values(parent_fields, row), unique=True)[0]
                       for row in csv_reader]
            csv_file.seek(0)
            csv_reader = csv.DictReader(csv_file)  # reset the CSV reader

        for parent in parents:
            relationship.clear(parent.id)

    for row in csv_reader:
        if parent_id is None:
            parent = parent_ops.get_where(_entity_field_values(parent_fields, row), unique=True)[0]
        else:
            parent = parent_ops.get(parent_id)

        child = child_ops.get_where(_entity_field_values(child_fields, row), unique=True)[0]

        value = _extract_value_field(relationship, row)
        relationship.add(parent.id, child.id, value)
