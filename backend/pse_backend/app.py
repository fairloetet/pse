"""
Defines the app object which serves as the entry point for uvicorn, along with all available routes.

To run the backend, call uvicorn on the app object, e.g.

    uvicorn pse_backend.app:app
"""

from pse_backend.config import get_config
from pse_backend.context import Context

context = Context.from_config(get_config())
app = context.app
