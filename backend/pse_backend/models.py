"""
Contains Pydantic models for elementary building blocks of the JSON API.

As suggested in the FastAPI tutorial, we define groups of data types for every entity type. For any entity *SomeEntity*,
there may be the following classes:

    - The model `SomeEntity` contains all fields of the entity and is the data structure always returned by the API.
    - The model `SomeEntityNew` contains all fields except the ID and may be used to create the entity.
    - The model `SomeEntityUpdate` has all fields except the ID set to Optional and may be used to update the entity.

Every API endpoint has its own request and response models, which incorporate the basic entity models defined here
as submodels.
"""
from typing import List, Optional

from pydantic import BaseModel, Extra, validator  # type: ignore


class ErrorMessage(BaseModel):
    detail: str


class Entity(BaseModel):
    """
    Base class for all entities.

    Defines configuration all entities inherit.
    """
    class Config:
        extra = Extra.forbid  # Disable ignoring of extra attributes
        orm_mode = True


class EntityFull(BaseModel):
    id: int


# ---------------------------------------------------------------------
# ComponentAssignment
# ---------------------------------------------------------------------

class ComponentAssignmentNew(Entity):
    component_id: int
    amount: float


class ComponentAssignment(ComponentAssignmentNew):
    id: int


class ComponentAssignmentUpdate(Entity):
    component_id: Optional[int]
    amount: Optional[float]


# ---------------------------------------------------------------------
# Product
# ---------------------------------------------------------------------

class ProductNew(Entity):
    name: str
    slug: Optional[str]
    description: Optional[str]


class Product(ProductNew, EntityFull):
    components: List[ComponentAssignment] = []


class ProductUpdate(Entity):
    slug: Optional[str]
    name: Optional[str]
    description: Optional[str]


# ---------------------------------------------------------------------
# MaterialAssignment
# ---------------------------------------------------------------------

class MaterialAssignment(Entity):
    material_id: int
    amount: float


# ---------------------------------------------------------------------
# Component
# ---------------------------------------------------------------------

class ComponentNew(Entity):
    name: str
    slug: Optional[str]
    weight: float


class Component(ComponentNew, EntityFull):
    materials: List['MaterialAssignment']

    class Config:
        orm_mode = True


class ComponentUpdate(Entity):
    name: Optional[str]
    slug: Optional[str]
    weight: Optional[float]


# ---------------------------------------------------------------------
# CountryAssignment
# ---------------------------------------------------------------------


class CountryAssignment(Entity):
    country_id: int
    amount: float


# ---------------------------------------------------------------------
# Material
# ---------------------------------------------------------------------

class MaterialNew(Entity):
    name: str
    slug: Optional[str]


class Material(MaterialNew, EntityFull):
    countries: Optional[List[CountryAssignment]]

    class Config:
        orm_mode = True


class MaterialUpdate(Entity):
    name: Optional[str]
    slug: Optional[str]


# ---------------------------------------------------------------------
# IndicatorAssignment
# ---------------------------------------------------------------------


class IndicatorAssignment(Entity):
    indicator_id: int
    amount: float


# ---------------------------------------------------------------------
# Country
# ---------------------------------------------------------------------

class CountryNew(Entity):
    name: str
    code: str
    slug: Optional[str]


class Country(CountryNew, EntityFull):
    code: str
    materials: Optional[List[MaterialAssignment]]
    indicators: Optional[List[IndicatorAssignment]]

    class Config:
        orm_mode = True


class CountryUpdate(Entity):
    name: Optional[str]
    code: Optional[str]
    slug: Optional[str]


# ---------------------------------------------------------------------
# Indicator
# ---------------------------------------------------------------------

class IndicatorNew(Entity):
    name: str
    slug: Optional[str]
    direction: str = 'ascending'  # ascending means: higher value -> higher risk

    @validator('direction')
    def valid_directions(cls, v):
        if v not in ['ascending', 'descending']:
            raise ValueError("Must be one of 'ascending', 'descending'")
        return v


class Indicator(IndicatorNew, EntityFull):
    countries: Optional[List[CountryAssignment]]

    class Config:
        orm_mode = True


class IndicatorUpdate(Entity):
    name: Optional[str]
    slug: Optional[str]
    direction: Optional[str]

    @validator('direction')
    def valid_directions(cls, v):
        if v not in ['ascending', 'descending']:
            raise ValueError("Must be one of 'ascending', 'descending'")
        return v


class RiskReport(BaseModel):
    name: str
    level: str


class Level1Report(BaseModel):
    id: int
    name: str
    activity: float
    riskLevel: str
    risks: List[RiskReport]


class Report(BaseModel):
    components: List[Level1Report]
    materials: List[Level1Report]
    countries: List[Level1Report]

    weight: float
