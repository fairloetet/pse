from typing import Any, Dict, Optional, Type, TypeVar, Generic, cast
from pydantic import create_model, BaseModel, Schema  # type: ignore


DataT = TypeVar('DataT')


class RequestModel(BaseModel, Generic[DataT]):
    """
    Generic model for requests.

    Requests always have a `data` parameter.
    """
    data: DataT


class ResponseModel(BaseModel, Generic[DataT]):
    """
    Generic model for responses.

    Requests always have a `data` parameter and possibly a dictionary of `included` entities.
    """
    data: DataT
    included: Optional[Dict[str, Any]]


def mk_response_model(model_name: str, data_type: Type, included_types: Dict[str, Type] = None) -> Type[ResponseModel]:
    """
    Creates a Pydantic model for an API response.

    Responses can have the following top-level keys:

        - `data` contains the main result of the request
        - `included` (optional) contains a dictionary of additional objects related to the main result objects.

    :param model_name: class name for the generated model; should be unique
    :param data_type: type of the data attribute in the response
    :param included_types: dictionary of types specifying the included objects
    :return: a Pydantic model
    """
    # Build the arguments to create_model as a dictionary
    model_dict = {"data": (data_type, ...)}
    if included_types:
        included_dict = {k: (v, ...) for k, v in included_types.items()} if included_types else {}
        included_model = create_model(model_name + "Included", **included_dict)
        model_dict["included"] = (included_model, ...)

    model = create_model(model_name, **model_dict)
    return cast(Type[ResponseModel], model)


def mk_request_model(model_name: str, data_type: Type, example_data: Any = None) -> Type[RequestModel]:
    """
    Creates a Pydantic model for a request body.

    :param model_name: class name for the generated model; should be unique
    :param data_type: type of the data attribute in the request
    :param example_data: example value for the data argument
    :return: a Pydantic model
    """
    model = create_model(model_name, data=(data_type, Schema(..., example=example_data)))
    return cast(Type[RequestModel], model)
