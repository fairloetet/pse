"""
Contains functions that handle common API requests in a generic way.

These functions are the backing implementations for concrete API endpoints.
"""
from io import StringIO

from fastapi import File, HTTPException
from starlette.status import HTTP_400_BAD_REQUEST

from pse_backend import PseException
from pse_backend.data_import.assignment import import_assignment_into_db
from pse_backend.data_import.entity import import_entities_from_csv_into_db
from pse_backend.db import Database
from pse_backend.db.assignment_ops import AssignmentOps


def upload_assignment(db: Database, parent: str, assignment: str, parent_id: int, file: File):
    """
    Upload a CSV file defining an assignment for a given parent entity.

    All existing assignments are removed from the parent entity. I.e., the assignment is replaced by the uploaded list.

    :param db: database to work on
    :param parent: name of the parent entity, e.g. 'product'
    :param assignment: name of the assignment relation, e.g. 'components'
    :param parent_id: ID of the entity to upload assignments for
    :param file: uploaded file
    """

    try:
        csv_data = file.file.read().decode('utf-8', errors='strict')
        text_io = StringIO(csv_data)
        import_assignment_into_db(db=db,
                                  parent_entity_name=parent,
                                  relationship_name=assignment,
                                  csv_file=text_io,
                                  parent_id=parent_id)
    except UnicodeDecodeError:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail='Please upload a UTF-8 encoded CSV file.')
    except PseException as e:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail=e.message)


def upload_entities(db: Database, entity: str, file: File):
    """
    Upload a CSV file defining entities of a common type.

    :param db: database to work on
    :param entity: type of entity to upload, e.g. 'component'
    :param file: uploaded file
    """
    try:
        csv_data = file.file.read().decode('utf-8', errors='strict')
        text_io = StringIO(csv_data)
        import_entities_from_csv_into_db(db, entity, text_io, update=True)
    except UnicodeDecodeError:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail='Please upload a UTF-8 encoded CSV file.')
    except PseException as e:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail=e.message)
