"""
Defines the endpoints for the PSE API.
"""

from fastapi import FastAPI, HTTPException, Depends, UploadFile, File
from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request
from starlette.responses import Response
from starlette.status import HTTP_201_CREATED, HTTP_404_NOT_FOUND, HTTP_500_INTERNAL_SERVER_ERROR, HTTP_204_NO_CONTENT, \
    HTTP_400_BAD_REQUEST

from pse_backend.analysis.report import get_report
from pse_backend.api import handlers
from pse_backend.api.models import *
from pse_backend.db import EntityNotFoundError, Database


# ---------------------------------------------------------------------
# Setup
# ---------------------------------------------------------------------


def build_app(db: Database) -> FastAPI:
    """
    Creates a FastAPI webapp object which can be served with uvicorn.

    :param db: the database used by the application
    :return: a FastAPI application
    """

    # FastAPI setup

    app = FastAPI(title="PSE Backend")

    # CORS setup

    origins = [
        "http://localhost",
        "http://localhost:8080",
        "http://localhost:8081",
        "http://app.fairtronics.org",
        "http://app.fairtronics.org:8001",
        "http://app.fairtronics.org:8080"
    ]

    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=['*'],
        allow_headers=['*']
    )

    # Set up Starlette to manage database connections

    @app.middleware("http")
    async def db_session_middleware(request: Request, call_next):
        response = Response("Internal server error", status_code=500)
        request.state.db = db
        try:
            response = await call_next(request)
        finally:
            request.state.db.close()
        return response

    def get_db(request: Request):
        return request.state.db

    # ---------------------------------------------------------------------
    # Accessing Products
    # ---------------------------------------------------------------------

    @app.get("/")
    def get_root():
        return {'Hello': 'World'}

    @app.get("/products", response_model=GetProductsResponse)
    def get_products(db: Database = Depends(get_db)):
        """
        Retrieve a list of all products.
        """
        product_list = db.product.get_all()
        return GetProductsResponse(data=product_list)

    @app.get("/products/{product_id}", response_model=GetProductResponse, responses={
        HTTP_404_NOT_FOUND: {"model": ErrorMessage, "description": "Item not found"},
        HTTP_500_INTERNAL_SERVER_ERROR: {"model": ErrorMessage, "description": "Product depends on missing component"}
    })
    def get_product(product_id: int, db: Database = Depends(get_db)):
        """
        Retrieve an individual product by its ID.
        """
        try:
            product: Product = db.product.get(product_id)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

        try:
            included_components = [db.component.get(component_id) for component_id in db.product.components.get_children(product.id)]
        except EntityNotFoundError as e:
            raise HTTPException(status_code=HTTP_500_INTERNAL_SERVER_ERROR,
                                detail=f"Product depends on missing component. {e.message}")

        return {
            "data": product,
            "included": {
                "components": included_components
            }
        }

    @app.post("/products", status_code=HTTP_201_CREATED, response_model=CreateProductResponse)
    def create_product(data: CreateProductRequest, db: Database = Depends(get_db)):
        """
        Creates a new product.
        """
        db_item = db.product.create(data.data)
        return CreateProductResponse(data=db_item)

    @app.put("/products", status_code=HTTP_204_NO_CONTENT)
    def upload_products(file: UploadFile = File(...), db: Database = Depends(get_db)):
        """
        Upload a CSV file containing product definitions.

        Warning: This is an overwriting operation. Existing products are overwritten if they have the same slug as
        an uploaded product!
        """
        handlers.upload_entities(db, 'product', file)

    @app.patch("/products/{product_id}", response_model=UpdateProductResponse)
    def update_product(product_id: int, data: UpdateProductRequest, db: Database = Depends(get_db)):
        """
        Update a product's attributes.

        All fields are optional. The provided fields will be used to update the stored entity.
        """
        entity = db.product.update(product_id, data.data)
        return UpdateProductResponse(data=entity)

    @app.delete("/products/{product_id}", status_code=HTTP_204_NO_CONTENT)
    def delete_product(product_id: int, db: Database = Depends(get_db)):
        """
        Delete a product by its ID.
        """
        try:
            db.product.delete(product_id)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    # ---------------------------------------------------------------------
    # Product-Component Assignment
    # ---------------------------------------------------------------------

    @app.post("/products/{product_id}/components", status_code=HTTP_201_CREATED, response_model=AddProductComponentsResponse)
    def add_product_components(
            product_id: int,
            request: AddProductComponentsRequest,
            db: Database = Depends(get_db)):
        """
        Add components to a product. Data can either be a single component ID or a list of component IDs.
        """
        # Handle single IDs as well as lists of IDs
        if isinstance(request.data, list):
            assignments = request.data
        else:
            assignments = [request.data]

        try:
            created_assignments = [db.product.components.append(product_id, assignment.component_id, assignment.amount)
                                   for assignment in assignments]
            return AddProductComponentsResponse(data=created_assignments)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail=e.message)

    @app.patch("/products/{product_id}/components/{assignment_id}", response_model=UpdateProductComponentResponse)
    def update_product_component(component_id: int, assignment_id: int, data: UpdateProductComponentRequest, db: Database = Depends(get_db)):
        """
        Update a product-component assignment.

        All fields are optional. The provided fields will be used to update the stored assignment.
        """
        assignment = db.product.components.update(assignment_id, data.data)
        return UpdateProductComponentResponse(data=assignment)

    @app.delete("/products/{product_id}/components/{assignment_id}",
              status_code=HTTP_204_NO_CONTENT,
              responses={
                  HTTP_404_NOT_FOUND: {"model": ErrorMessage, "description": "Item not found"}
              })
    def remove_product_component(product_id: int,
                                 assignment_id: int,
                                 db: Database = Depends(get_db)):
        """
        Remove component assignments from a product. Specify the ID of the assignment.
        """
        db.product.components.remove_assignment(product_id, assignment_id)

    @app.put("/products/{product_id}/components",
             status_code=HTTP_204_NO_CONTENT,
             responses={
                 HTTP_400_BAD_REQUEST: {"model": ErrorMessage, "description": "Bad Encoding or Formatting"},
                 HTTP_404_NOT_FOUND: {"model": ErrorMessage, "description": "Item not found"}
             })
    def upload_product_components(product_id: int, file: UploadFile = File(...), db: Database = Depends(get_db)):
        """
        Upload a CSV file defining the components of a product.

        All existing components are removed from the product. I.e., the component list is replaced by the uploaded list.
        """
        handlers.upload_assignment(db, 'product', 'components', product_id, file)

    # ---------------------------------------------------------------------
    # Access product report
    # ---------------------------------------------------------------------

    @app.get("/products/{product_id}/report")
    def get_product_report(product_id: int, db: Database = Depends(get_db)):
        try:
            report = get_report(product_id, db=db)
            return {
                "data": report
            }
        except EntityNotFoundError as e:
            raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail=e.message)

    # ---------------------------------------------------------------------
    # Accessing Components
    # ---------------------------------------------------------------------

    @app.get("/components", response_model=GetComponentsResponse)
    def get_components(db: Database = Depends(get_db)):
        """
        Retrieve a list of all components.
        """
        component_list = db.component.get_all()
        return GetComponentsResponse(data=component_list)

    @app.post("/components", status_code=HTTP_201_CREATED, response_model=CreateComponentResponse)
    def create_component(data: CreateComponentRequest, db: Database = Depends(get_db)):
        """
        Creates a new component.
        """
        db_item = db.component.create(data.data)
        return CreateComponentResponse(data=db_item)

    @app.put("/components", status_code=HTTP_204_NO_CONTENT)
    def upload_components(file: UploadFile = File(...), db: Database = Depends(get_db)):
        """
        Upload a CSV file containing component definitions.

        Warning: This is an overwriting operation. Existing components are overwritten if they have the same slug as
        an uploaded component!
        """
        handlers.upload_entities(db, 'component', file)

    @app.get("/components/{component_id}", response_model=GetComponentResponse, responses={
        HTTP_404_NOT_FOUND: {"model": ErrorMessage, "description": "Item not found"}
    })
    def get_component(component_id: int, db: Database = Depends(get_db)):
        """
        Retrieve an individual component by its ID.
        """
        try:
            db_item = db.component.get(component_id)
            return GetComponentResponse(data=db_item)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    @app.patch("/components/{component_id}", response_model=UpdateComponentResponse)
    def update_component(component_id: int, data: UpdateComponentRequest, db: Database = Depends(get_db)):
        """
        Update a component's attributes.

        All fields are optional. The provided fields will be used to update the stored entity.
        """
        entity = db.component.update(component_id, data.data)
        return UpdateComponentResponse(data=entity)

    @app.delete("/components/{component_id}", status_code=HTTP_204_NO_CONTENT)
    def delete_component(component_id: int, db: Database = Depends(get_db)):
        """
        Delete a component by its ID.
        """
        try:
            db.component.delete(component_id)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    # ---------------------------------------------------------------------
    # Component-Material Assignment
    # ---------------------------------------------------------------------

    @app.post("/components/{component_id}/materials/set", status_code=HTTP_204_NO_CONTENT)
    def set_component_material(component_id: int, request: SetComponentMaterialRequest, db: Database = Depends(get_db)):
        """
        Assign a material to a component.
        """
        try:
            material_assignments = request.data
            if not isinstance(material_assignments, list):
                material_assignments = [material_assignments]
            for material_assignment in material_assignments:
                db.component.materials.add(component_id, material_assignment.material_id, material_assignment.amount)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    @app.post("/components/{component_id}/materials/remove", status_code=HTTP_204_NO_CONTENT)
    def remove_component_material(component_id: int, request: RemoveComponentMaterialRequest, db: Database = Depends(get_db)):
        """
        Remove a material assignment from a component.
        """
        try:
            material_ids = request.data
            if not isinstance(material_ids, list):
                material_ids = [material_ids]
            for material_id in material_ids:
                db.component.materials.remove(component_id, material_id)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    @app.put("/components/{component_id}/materials",
             status_code=HTTP_204_NO_CONTENT,
             responses={
                 HTTP_400_BAD_REQUEST: {"model": ErrorMessage, "description": "Bad Encoding or Formatting"},
                 HTTP_404_NOT_FOUND: {"model": ErrorMessage, "description": "Item not found"}
             })
    def upload_component_materials(component_id: int, file: UploadFile = File(...), db: Database = Depends(get_db)):
        """
        Upload a CSV file defining the material composition of a component.

        All existing material assignments are removed from the component.
        I.e., the material composition is replaced by the uploaded list.
        """
        handlers.upload_assignment(db, 'component', 'materials', component_id, file)

    # ---------------------------------------------------------------------
    # Accessing Materials
    # ---------------------------------------------------------------------

    @app.get("/materials", response_model=GetMaterialsResponse)
    def get_materials(db: Database = Depends(get_db)):
        """
        Retrieve a list of all materials.
        """
        material_list = db.material.get_all()
        return GetMaterialsResponse(data=material_list)

    @app.post("/materials", status_code=HTTP_201_CREATED, response_model=CreateMaterialResponse)
    def create_material(data: CreateMaterialRequest, db: Database = Depends(get_db)):
        """
        Creates a new material.
        """
        db_item = db.material.create(data.data)
        return CreateMaterialResponse(data=db_item)

    @app.put("/materials", status_code=HTTP_204_NO_CONTENT)
    def upload_materials(file: UploadFile = File(...), db: Database = Depends(get_db)):
        """
        Upload a CSV file containing material definitions.

        Warning: This is an overwriting operation. Existing materials are overwritten if they have the same slug as
        an uploaded material!
        """
        handlers.upload_entities(db, 'material', file)

    @app.get("/materials/{material_id}", response_model=GetMaterialResponse, responses={
        HTTP_404_NOT_FOUND: {"model": ErrorMessage, "description": "Item not found"}
    })
    def get_material(material_id: int, db: Database = Depends(get_db)):
        """
        Retrieve an individual material by its ID.
        """
        try:
            db_item = db.material.get(material_id)
            return GetMaterialResponse(data=db_item)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    @app.patch("/materials/{material_id}", response_model=UpdateMaterialResponse)
    def update_material(material_id: int, data: UpdateMaterialRequest, db: Database = Depends(get_db)):
        """
        Update a material's attributes.

        All fields are optional. The provided fields will be used to update the stored entity.
        """
        entity = db.material.update(material_id, data.data)
        return UpdateMaterialResponse(data=entity)

    @app.delete("/materials/{material_id}", status_code=HTTP_204_NO_CONTENT)
    def delete_material(material_id: int, db: Database = Depends(get_db)):
        """
        Delete a material by its ID.
        """
        try:
            db.material.delete(material_id)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    # ---------------------------------------------------------------------
    # Material-Country Assignment
    # ---------------------------------------------------------------------

    @app.post("/materials/{material_id}/countries/set", status_code=HTTP_204_NO_CONTENT)
    def set_material_country(material_id: int, request: SetMaterialCountryRequest, db: Database = Depends(get_db)):
        """
        Assign a country to a material.
        """
        try:
            country_assignments = request.data
            if not isinstance(country_assignments, list):
                country_assignments = [country_assignments]
            for country_assignment in country_assignments:
                db.material.countries.add(material_id, country_assignment.country_id, country_assignment.amount)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    @app.post("/materials/{material_id}/countries/remove", status_code=HTTP_204_NO_CONTENT)
    def remove_material_country(material_id: int, request: RemoveMaterialCountryRequest, db: Database = Depends(get_db)):
        """
        Remove a country assignment from a material.
        """
        try:
            country_ids = request.data
            if not isinstance(country_ids, list):
                country_ids = [country_ids]
            for country_id in country_ids:
                db.material.countries.remove(material_id, country_id)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    @app.put("/materials/{material_id}/countries",
             status_code=HTTP_204_NO_CONTENT,
             responses={
                 HTTP_400_BAD_REQUEST: {"model": ErrorMessage, "description": "Bad Encoding or Formatting"},
                 HTTP_404_NOT_FOUND: {"model": ErrorMessage, "description": "Item not found"}
             })
    def upload_material_countries(material_id: int, file: UploadFile = File(...), db: Database = Depends(get_db)):
        """
        Upload a CSV file defining the countries of origin of a material.

        All existing country assignments are removed from the material.
        I.e., the list of countries is replaced by the uploaded list.
        """
        handlers.upload_assignment(db, 'material', 'countries', material_id, file)

    # ---------------------------------------------------------------------
    # Accessing Countries
    # ---------------------------------------------------------------------

    @app.get("/countries", response_model=GetCountriesResponse)
    def get_countries(db: Database = Depends(get_db)):
        """
        Retrieve a list of all countries.
        """
        country_list = db.country.get_all()
        return GetCountriesResponse(data=country_list)

    @app.post("/countries", status_code=HTTP_201_CREATED, response_model=CreateCountryResponse)
    def create_country(data: CreateCountryRequest, db: Database = Depends(get_db)):
        """
        Creates a new country.
        """
        db_item = db.country.create(data.data)
        return CreateCountryResponse(data=db_item)

    @app.put("/countries", status_code=HTTP_204_NO_CONTENT)
    def upload_countries(file: UploadFile = File(...), db: Database = Depends(get_db)):
        """
        Upload a CSV file containing country definitions.

        Warning: This is an overwriting operation. Existing countries are overwritten if they have the same slug as
        an uploaded country!
        """
        handlers.upload_entities(db, 'country', file)

    @app.get("/countries/{country_id}", response_model=GetCountryResponse, responses={
        HTTP_404_NOT_FOUND: {"model": ErrorMessage, "description": "Item not found"}
    })
    def get_country(country_id: int, db: Database = Depends(get_db)):
        """
        Retrieve an individual country by its ID.
        """
        try:
            db_item = db.country.get(country_id)
            return GetCountryResponse(data=db_item)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    @app.patch("/countries/{country_id}", response_model=UpdateCountryResponse)
    def update_country(country_id: int, data: UpdateCountryRequest, db: Database = Depends(get_db)):
        """
        Update a country's attributes.

        All fields are optional. The provided fields will be used to update the stored entity.
        """
        entity = db.country.update(country_id, data.data)
        return UpdateCountryResponse(data=entity)

    @app.delete("/countries/{country_id}", status_code=HTTP_204_NO_CONTENT)
    def delete_country(country_id: int, db: Database = Depends(get_db)):
        """
        Delete a country by its ID.
        """
        try:
            db.country.delete(country_id)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    # ---------------------------------------------------------------------
    # Country-Indicator Assignment
    # ---------------------------------------------------------------------

    @app.post("/countries/{country_id}/indicators/set", status_code=HTTP_204_NO_CONTENT)
    def set_country_indicator(country_id: int, request: SetCountryIndicatorRequest, db: Database = Depends(get_db)):
        """
        Assign a country to an indicator.
        """
        try:
            indicator_assignments = request.data
            if not isinstance(indicator_assignments, list):
                indicator_assignments = [indicator_assignments]
            for indicator_assignment in indicator_assignments:
                db.country.indicators.add(country_id, indicator_assignment.indicator_id, indicator_assignment.amount)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    @app.post("/countries/{country_id}/indicators/remove", status_code=HTTP_204_NO_CONTENT)
    def remove_material_country(country_id: int, request: RemoveCountryIndicatorRequest, db: Database = Depends(get_db)):
        """
        Remove a country assignment from an indicator.
        """
        try:
            indicator_ids = request.data
            if not isinstance(indicator_ids, list):
                indicator_ids = [indicator_ids]
            for indicator_id in indicator_ids:
                db.country.indicators.remove(country_id, indicator_id)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    # ---------------------------------------------------------------------
    # Accessing Indicators
    # ---------------------------------------------------------------------

    @app.get("/indicators", response_model=GetIndicatorsResponse)
    def get_indicators(db: Database = Depends(get_db)):
        """
        Retrieve a list of all indicators.
        """
        indicator_list = db.indicator.get_all()
        return GetIndicatorsResponse(data=indicator_list)

    @app.post("/indicators", status_code=HTTP_201_CREATED, response_model=CreateIndicatorResponse)
    def create_indicator(data: CreateIndicatorRequest, db: Database = Depends(get_db)):
        """
        Creates a new indicator.
        """
        db_item = db.indicator.create(data.data)
        return CreateIndicatorResponse(data=db_item)

    @app.put("/indicators", status_code=HTTP_204_NO_CONTENT)
    def upload_indicators(file: UploadFile = File(...), db: Database = Depends(get_db)):
        """
        Upload a CSV file containing indicator definitions.

        Warning: This is an overwriting operation. Existing indicators are overwritten if they have the same slug as
        an uploaded indicator!
        """
        handlers.upload_entities(db, 'indicator', file)

    @app.get("/indicators/{indicator_id}", response_model=GetIndicatorResponse, responses={
        HTTP_404_NOT_FOUND: {"model": ErrorMessage, "description": "Item not found"}
    })
    def get_indicator(indicator_id: int, db: Database = Depends(get_db)):
        """
        Retrieve an individual indicator by its ID.
        """
        try:
            db_item = db.indicator.get(indicator_id)
            return GetIndicatorResponse(data=db_item)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    @app.patch("/indicators/{indicator_id}", response_model=UpdateIndicatorResponse)
    def update_indicator(indicator_id: int, data: UpdateIndicatorRequest, db: Database = Depends(get_db)):
        """
        Update a indicator's attributes.

        All fields are optional. The provided fields will be used to update the stored entity.
        """
        entity = db.indicator.update(indicator_id, data.data)
        return UpdateIndicatorResponse(data=entity)

    @app.delete("/indicators/{indicator_id}", status_code=HTTP_204_NO_CONTENT)
    def delete_indicator(indicator_id: int, db: Database = Depends(get_db)):
        """
        Delete a indicator by its ID.
        """
        try:
            db.indicator.delete(indicator_id)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    # ---------------------------------------------------------------------
    # Indicator-Country Assignment
    # ---------------------------------------------------------------------

    @app.post("/indicators/{indicator_id}/countries/set", status_code=HTTP_204_NO_CONTENT)
    def set_indicator_country(indicator_id: int, request: SetIndicatorCountryRequest, db: Database = Depends(get_db)):
        """
        Assign a country to an indicator.
        """
        try:
            country_assignments = request.data
            if not isinstance(country_assignments, list):
                country_assignments = [country_assignments]
            for country_assignment in country_assignments:
                db.indicator.countries.add(indicator_id, country_assignment.country_id, country_assignment.amount)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    @app.post("/indicators/{indicator_id}/countries/remove", status_code=HTTP_204_NO_CONTENT)
    def remove_material_country(indicator_id: int, request: RemoveIndicatorCountryRequest, db: Database = Depends(get_db)):
        """
        Remove a country assignment from an indicator.
        """
        try:
            country_ids = request.data
            if not isinstance(country_ids, list):
                country_ids = [country_ids]
            for country_id in country_ids:
                db.indicator.countries.remove(indicator_id, country_id)
        except EntityNotFoundError as e:
            raise HTTPException(status_code=404, detail=e.message)

    @app.put("/indicators/{indicator_id}/countries",
             status_code=HTTP_204_NO_CONTENT,
             responses={
                 HTTP_400_BAD_REQUEST: {"model": ErrorMessage, "description": "Bad Encoding or Formatting"},
                 HTTP_404_NOT_FOUND: {"model": ErrorMessage, "description": "Item not found"}
             })
    def upload_indicator_countries(indicator_id: int, file: UploadFile = File(...), db: Database = Depends(get_db)):
        """
        Upload a CSV file defining an indicator's values for a list of countries.

        All existing country assignments are removed from the indicator.
        I.e., the list of countries is replaced by the uploaded list.
        """
        handlers.upload_assignment(db, 'indicator', 'countries', indicator_id, file)

    return app
