from typing import Union

from pse_backend.models import *
from pse_backend.api.response import mk_response_model, mk_request_model

GetProductsResponse = mk_response_model('GetProductsResponse', List[Product])
GetProductResponse = mk_response_model('GetProductResponse', Product, included_types={'components': List[Component]})
CreateProductRequest = mk_request_model('CreateProductRequest', ProductNew)
CreateProductResponse = mk_response_model('CreateProductResponse', Product)
UpdateProductRequest = mk_request_model('UpdateProductRequest', ProductUpdate)
UpdateProductResponse = mk_response_model('UpdateProductResponse', Product)

AddProductComponentsRequest = mk_request_model(
    "AddProductComponentsRequest",
    data_type=Union[ComponentAssignmentNew, List[ComponentAssignmentNew]],
    example_data=ComponentAssignmentNew(component_id=12345, amount=1.0))
AddProductComponentsResponse = mk_response_model('AddProductComponentsResponse', List[ComponentAssignment])
UpdateProductComponentRequest = mk_request_model(
    "UpdateProductComponentRequest",
    data_type=Union[ComponentAssignmentUpdate],
    example_data=ComponentAssignmentUpdate(amount=23.0))
UpdateProductComponentResponse = mk_response_model('UpdateProductComponentResponse', ComponentAssignment)

GetComponentsResponse = mk_response_model('GetComponentsResponse', List[Component])
CreateComponentRequest = mk_request_model('CreateComponentRequest', ComponentNew)
CreateComponentResponse = mk_response_model('CreateComponentResponse', Component)
GetComponentResponse = mk_response_model('GetComponentResponse', Component)
UpdateComponentRequest = mk_request_model('UpdateComponentRequest', ComponentUpdate)
UpdateComponentResponse = mk_response_model('UpdateComponentResponse', Component)

SetComponentMaterialRequest = mk_request_model(
    "SetComponentMaterialRequest",
    data_type=Union[MaterialAssignment, List[MaterialAssignment]],
    example_data=MaterialAssignment(material_id=12345, amount=0.23))
RemoveComponentMaterialRequest = mk_request_model(
    "RemoveComponentMaterialRequest",
    data_type=Union[int, List[int]],
    example_data=12345)

GetMaterialsResponse = mk_response_model('GetMaterialsResponse', List[Material])
CreateMaterialRequest = mk_request_model('CreateMaterialRequest', MaterialNew)
CreateMaterialResponse = mk_response_model('CreateMaterialResponse', Material)
GetMaterialResponse = mk_response_model('GetMaterialResponse', Material)
UpdateMaterialRequest = mk_request_model('UpdateMaterialRequest', MaterialUpdate)
UpdateMaterialResponse = mk_response_model('UpdateMaterialResponse', Material)

SetMaterialCountryRequest = mk_request_model(
    "SetMaterialCountryRequest",
    data_type=Union[CountryAssignment, List[CountryAssignment]],
    example_data=CountryAssignment(country_id=12345, amount=0.23))
RemoveMaterialCountryRequest = mk_request_model(
    "RemoveMaterialCountryRequest",
    data_type=Union[int, List[int]],
    example_data=12345)

GetCountriesResponse = mk_response_model('GetCountriesResponse', List[Country])
CreateCountryRequest = mk_request_model('CreateCountryRequest', CountryNew)
CreateCountryResponse = mk_response_model('CreateCountryResponse', Country)
GetCountryResponse = mk_response_model('GetCountryResponse', Country)
UpdateCountryRequest = mk_request_model('UpdateCountryRequest', CountryUpdate)
UpdateCountryResponse = mk_response_model('UpdateCountryResponse', Country)

SetCountryIndicatorRequest = mk_request_model(
    "SetCountryIndicatorRequest",
    data_type=Union[IndicatorAssignment, List[IndicatorAssignment]],
    example_data=IndicatorAssignment(indicator_id=12345, amount=0.23))
RemoveCountryIndicatorRequest = mk_request_model(
    "RemoveCountryIndicatorRequest",
    data_type=Union[int, List[int]],
    example_data=12345)

GetIndicatorsResponse = mk_response_model('GetIndicatorsResponse', List[Indicator])
CreateIndicatorRequest = mk_request_model('CreateIndicatorRequest', IndicatorNew)
CreateIndicatorResponse = mk_response_model('CreateIndicatorResponse', Indicator)
GetIndicatorResponse = mk_response_model('GetIndicatorResponse', Indicator)
UpdateIndicatorRequest = mk_request_model('UpdateIndicatorRequest', IndicatorUpdate)
UpdateIndicatorResponse = mk_response_model('UpdateIndicatorResponse', Indicator)

SetIndicatorCountryRequest = mk_request_model(
    "SetIndicatorCountryRequest",
    data_type=Union[CountryAssignment, List[CountryAssignment]],
    example_data=CountryAssignment(country_id=12345, amount=0.23))
RemoveIndicatorCountryRequest = mk_request_model(
    "RemoveIndicatorCountryRequest",
    data_type=Union[int, List[int]],
    example_data=12345)
