class PseException(Exception):
    def __init__(self, message: str, underlying: Exception = None):
        self.message = message
        self.underlying = underlying
        if underlying:
            Exception.__init__(self, message, underlying)
        else:
            Exception.__init__(self, message)
