import numpy as np


def normalize(vector: np.array) -> np.array:
    """
    Normalize a numpy array so that it sums to one.

    Taken from https://stackoverflow.com/a/40360416

    :param vector: a numpy array
    :return: a numpy array with the same dimensions
    """
    norm = np.linalg.norm(vector, ord=1)
    if norm == 0:
        norm = np.finfo(vector.dtype).eps
    return vector / norm
