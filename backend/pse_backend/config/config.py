import logging
import os
import re
from importlib import resources
from typing import Optional, TextIO, Mapping, Dict

import toml
from deepmerge import always_merger
from pydantic import BaseModel, Extra, ValidationError  # type: ignore

from pse_backend.pse_exception import PseException


class ConfigError(PseException):
    """
    Raised when there is a problem loading or interpreting a configuration file.
    """
    def __init__(self, file_path: str, message: str, underlying: Exception = None):
        self.file_path = file_path
        PseException.__init__(self, f"Problem with config file {file_path}: {message}", underlying)


class ConfigBase(BaseModel):
    """
    Base class for configuration objects.
    """
    class Config:
        extra = Extra.forbid  # Disable ignoring of extra attributes


class DatabaseConfig(ConfigBase):
    """
    Defines configuration parameters for the database connection.
    """
    host: str
    port: int
    user: str
    password: str
    dbname: str


class Config(ConfigBase):
    """
    Stores configuration information for the PSE backend.
    """
    database: DatabaseConfig


the_config: Optional[Config] = None


def get_config() -> Config:
    """
    Access configuration data.

    You may specify the path of the config file in the `PSE_CONFIG` environment variable. If the variable is set,
    this config file is loaded. Otherwise, try to load `pse.toml` in the current working directory.
    If it does not exist, defaults are used for all settings.

    For the available config options and default values, see the file
    `pse_backend.config.pse-defaults.toml`.
    You may selectively override settings in your config file and defaults will be used for all other options.

    Configuration is loaded lazily on the first access.

    :return: a Config object
    """
    global the_config
    if not the_config:
        the_config = load_config()
    return the_config


def load_config() -> Config:
    """
    Load the configuration from the default location.

    First loads the defaults from the packaged file `pse_backend/config/pse-defaults.toml`.
    Then looks for the file specified in the PSE_CONFIG environment variable, falling back to
    `pse.toml` in the working directory. The loaded config file is used to override default settings.

    An optional dictionary of overridden values may be specified. The keys of the override dictionary are dot-separated
    configuration keys, e.g. 'database.name'. The corresponding values will take precedence over those loaded from
    a configuration file.

    :return: a Config object.
    :raises: ConfigError if the config cannot be loaded.
    """

    # Load default config
    try:
        with resources.open_text('pse_backend.config', 'pse-defaults.toml') as default_config_file:
            config = Config(**load_config_file(default_config_file, 'pse-defaults.toml'))
    except FileNotFoundError as e:
        raise ConfigError('pse-defaults.toml', 'File not found', e)
    except ValidationError as e:
        raise ConfigError('pse-defaults.toml', str(e), e)

    # Load user-specified config file
    config_file_name = 'pse.toml'
    if 'PSE_CONFIG' in os.environ:
        config_file_name = os.environ['PSE_CONFIG']
    try:
        with open(config_file_name) as config_file:
            local_config = load_config_file(config_file, config_file_name)
        config_dict = always_merger.merge(config.dict(), local_config)
        config = Config(**config_dict)
    except FileNotFoundError:
        logging.info(f"{config_file_name} not found, using default configuration")

    # Override config values
    if 'PSE_OPTIONS' in os.environ:
        override = parse_config_options(os.environ['PSE_OPTIONS'])
        config = override_config_values(config, override)

    return config


def load_config_file(file: TextIO, file_name: str) -> Mapping:
    """
    Loads TOML configuration data from a file stream.

    :param file: a TextIO object
    :param file_name: name of the file to load (for error messages)
    :return: a dictionary of configuration keys
    """
    try:
        return toml.load(file)
    except toml.TomlDecodeError as e:
        raise ConfigError(file_name, str(e), e)


config_option_re = re.compile(r'([\w.]+)=([^";]*|"([^\\"]|\\\\|\\")*?")(;|$)')


def parse_config_options(option_str: str) -> Dict[str, str]:
    """
    Parses a string of configuration options into a dictionary which can be used for configuration overriding purposes.

    The string may contain several key-value pairs, where keys are dot-separated paths and values may be quoted with
    double quotes. Entries are separated by semicolon. E.g.:

        database.port=1234;database.dbname="mydbname"

    :param option_str: a string of options
    :return: a dictionary of configuration key-value pairs
    """
    options = {}

    rest = option_str
    match = config_option_re.search(rest)
    while match:
        key = match.group(1)
        value = match.group(2)

        # Strip double quotes and unescape
        if value.startswith('"') and value.endswith('"'):
            value = value[1:-1]
            value = value.replace('\\"', '"')
            value = value.replace('\\\\', '\\')

        options[key] = value
        rest = rest[match.end():]
        match = config_option_re.search(rest)

    return options


def override_config_values(config: Config, override: Dict[str, str]) -> Config:
    """
    Replace specific configuration values by those specified in an override dictionary.

    The keys of the override dictionary are dot-separated configuration keys, e.g. 'database.name'. The corresponding
    configured values are overwritten with the values contained in the override dictionary.

    :param config: a config
    :param override: a dictionary with overriding configuration values
    :return: the overridden config
    """
    for k, v in override.items():
        segments = k.split('.')

        # Select the appropriate sub-object of the config
        selected = config
        for segment in segments[:-1]:
            selected = selected.__getattribute__(segment)

        # Set the overridden configuration value
        selected.__setattr__(segments[-1], v)

    return config
