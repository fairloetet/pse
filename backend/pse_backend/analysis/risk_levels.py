from dataclasses import dataclass
from typing import List, Iterable

import numpy as np

from pse_backend.models import Indicator


risk_level_map = {
    0: 'UNKNOWN',
    1: 'LOW',
    2: 'MEDIUM',
    3: 'HIGH'
}


risk_level_inverse_map = {v: k for k, v in risk_level_map.items()}


def risk_level_to_numeric(risk_level: str) -> int:
    """
    Convert a risk level to a number. The higher the risk, the higher the number.

    :param risk_level: a risk level in string form
    :return: numeric risk level
    """
    return risk_level_inverse_map.get(risk_level, 0)


def max_risk_level(risk_levels: List[str]) -> str:
    """
    Returns the highest of a collection of risk levels.

    :param risk_levels: a collection of risk levels
    :return: the highest risk level present in the collection, or unknown if it is empty
    """
    if not risk_levels:
        return 'UNKNOWN'
    return max(risk_levels, key=risk_level_to_numeric)


@dataclass
class ThresholdEntry:
    indicator: Indicator
    lower: float
    upper: float


class IndicatorThresholds:
    """
    Provides risk level computation using indicator thresholds.
    """
    def __init__(self, entries: Iterable[ThresholdEntry]):
        """
        Create an indicator threshold store from a collection of entries.

        Usually, you should not use the constructor but the `compute` method to obtain an indicator threshold store.
        Directly create one only if you want to override the computation.

        :param entries: iterable of threshold entries, one for each indicator
        """
        self.entries = {entry.indicator.id: entry for entry in entries}

    def risk_level(self, indicator_id: int, indicator_value: float) -> str:
        """
        Returns the risk level of a given indicator value.

        :param indicator_id: ID of the indicator
        :param indicator_value: indicator value to rate
        :return: a risk level, 'HIGH', 'MEDIUM', 'LOW', or 'UNKNOWN'
        """
        entry = self.entries.get(indicator_id)
        if not entry:
            return 'UNKNOWN'
        if np.isnan(entry.lower) or np.isnan(entry.upper):
            return 'UNKNOWN'
        if np.isnan(indicator_value):
            return 'UNKNOWN'

        is_descending = entry.indicator.direction == 'descending'
        if is_descending and indicator_value >= entry.upper or not is_descending and indicator_value <= entry.lower:
            return 'LOW'
        if is_descending and indicator_value <= entry.lower or not is_descending and indicator_value >= entry.upper:
            return 'HIGH'
        return 'MEDIUM'

    @staticmethod
    def compute(indicators: List[Indicator]) -> 'IndicatorThresholds':
        """
        Compute indicator thresholds based on upper and lower quartiles.

        :param indicators: list of indicators to compute thresholds for
        :return: an indicator threshold store
        """
        def indicator_entry(indicator: Indicator) -> ThresholdEntry:
            countries = indicator.countries if indicator.countries is not None else []
            values = [assoc.amount for assoc in countries]
            if len(values) >= 1:
                lower = np.quantile(values, 0.25, interpolation='nearest')
                upper = np.quantile(values, 0.75, interpolation='nearest')
            else:
                lower = upper = np.nan
            return ThresholdEntry(indicator, lower, upper)

        entries = [indicator_entry(indicator) for indicator in indicators]
        return IndicatorThresholds(entries)
