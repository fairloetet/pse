from dataclasses import dataclass
from typing import List, Optional

import numpy as np

from pse_backend.analysis.risk_levels import IndicatorThresholds, max_risk_level
from pse_backend.db import Database
from pse_backend.models import Product, Report, Component, Indicator, Country, Level1Report, \
    EntityFull, Material, RiskReport
from pse_backend.util.math import normalize


@dataclass
class ReportContext:
    """
    Assembles all entities needed for the computation of a product report.
    """
    product: Product
    components: List[Component]
    materials: List[Material]
    countries: List[Country]
    indicators: List[Indicator]


def get_report(product_id: int, db: Database) -> Report:
    """
    Produces a social risk report for a product.

    :param product_id: ID of the Product instance to compute the report for
    :param db: database object
    :return: a Report instance
    """

    # Assemble the entities we need to work with
    entities = load_entities(product_id, db)
    # Compute the report
    return compute_report(
        entities.product, entities.components, entities.materials, entities.countries, entities.indicators)


def load_entities(product_id: int, db: Database) -> ReportContext:
    """
    Loads all entities needed for report computation of a given product.

    :param product_id: ID of the product to load entities for
    :param db: the database
    :return: loaded entities assembled into a ReportContext
    """
    product = db.product.get(product_id)
    # Retrieve only the components, materials and countries that are associated with this product
    components = [db.component.get(assoc.component_id) for assoc in product.components]
    # First deduplicate the material IDs, then retrieve them
    material_ids = {assoc.material_id for component in components for assoc in component.materials}
    materials = [db.material.get(material_id) for material_id in material_ids]
    # Same with the countries
    country_ids = {assoc.country_id for material in materials for assoc in material.countries}
    countries = [db.country.get(country_id) for country_id in country_ids]

    # Always work on all indicators
    indicators = db.indicator.get_all()
    return ReportContext(product, components, materials, countries, indicators)


def compute_report(product: Product,
                   components: List[Component],
                   materials: List[Material],
                   countries: List[Country],
                   indicators: List[Indicator]) -> Report:
    """
    Computes risks and activities for a given product and a given set of loaded entities.

    :param product: the product to analyse
    :param components: components used by the product
    :param materials: materials used (transitively) by the product
    :param countries: countries used (transitively) by the product
    :param indicators: indicators to analyse
    :return: a Report object
    """
    # Compute the required risk tables
    # First, retrieve the risks by country (this is in our database verbatim)
    country_risks = country_risk_matrix(countries, indicators)
    # Transfer the country-wise risks to the material level
    material_countries = material_country_matrix(materials, countries)
    material_risks = transfer_risk(material_countries, country_risks)
    # Transfer the material-wise risks to the component level
    component_materials = component_material_matrix(components, materials)
    component_risks = transfer_risk(component_materials, material_risks)

    # Compute activity vectors
    # First, retrieve the component activities (determined by amount and weight of components)
    component_activities = component_activity_vector(product, components)
    # Transfer them to the material level using the component-material matrix
    component_materials = component_material_matrix(components, materials)
    material_activities = transfer_activity(component_activities, component_materials)
    # Transfer them to the country level using the material-country matrix
    material_countries = material_country_matrix(materials, countries)
    country_activities = transfer_activity(material_activities, material_countries)

    # Compute thresholds
    thresholds = IndicatorThresholds.compute(indicators)

    # Assemble the report object
    return Report(
        components=mk_entity_reports(components, indicators, thresholds, component_activities, component_risks),
        materials=mk_entity_reports(materials, indicators, thresholds, material_activities, material_risks),
        countries=mk_entity_reports(countries, indicators, thresholds, country_activities, country_risks),
        weight=product_weight(product, components)
    )


def mk_entity_reports(entities: List[EntityFull],
                      indicators: List[Indicator],
                      thresholds: IndicatorThresholds,
                      activity_vector: np.array,
                      risk_table: np.ma.array) -> List[Level1Report]:
    """
    Assemble a list of Level1Report objects from an entity risk table.

    :param entities: list of entities whose risks are reported in the table
    :param indicators: list of indicators represented in the table
    :param thresholds: thresholds to apply for risk level assignment
    :param activity_vector: vector specifying the activity for each entity
    :param risk_table: entity risk table
    :return: a list of Level1Report objects
    """
    risks = risk_table.filled(np.nan)

    def entity_report(entity_idx: int, entity: EntityFull) -> Optional[Level1Report]:
        risk_reports = [RiskReport(name=indicator.name, level=thresholds.risk_level(indicator.id, risk_value))
                        for indicator, risk_value in zip(indicators, risks[entity_idx])]

        # Skip entities that have no risk data
        if all(report.level == 'UNKNOWN' for report in risk_reports):
            return None

        return Level1Report(
            id=entity.id,
            name=entity.name,
            activity=activity_vector[entity_idx],
            riskLevel=max_risk_level([report.level for report in risk_reports]),
            risks=risk_reports
        )

    def gen_entity_reports():
        for idx, entity in enumerate(entities):
            report = entity_report(idx, entity)
            if report is not None:
                yield report

    return list(gen_entity_reports())


def transfer_risk(assignment_matrix: np.array, risk_matrix: np.array) -> np.array:
    """
    Transfers risk assignments to another entity.

    Given an assignment from entity A to entity B, and a matrix of risks for entity B, computes a matrix of risks
    for entity A.

    :param assignment_matrix: an assignment matrix, m*n
    :param risk_matrix: a risk matrix, n*o
    :return: a risk matrix, m*o
    """
    return np.ma.dot(assignment_matrix, risk_matrix)


def mk_assignment_matrix(
        parents: List[EntityFull],
        children: List[EntityFull],
        assignment_name: str,
        assignment_id_field: str) -> np.array:
    """
    Assembles an assignment between two entities in a matrix.

    There must be an assignment of children to parents. The valid pairs of entity types are (component, material),
    (material, country), and (country, indicator).

    Indices in the matrix returned by this function are assigned according to the position of each entity in the
    `parents` or `children` list.

    :param parents: list of the parent entities to assemble values for
    :param children: list of the child entities to assemble values for
    :param assignment_name: name of the property in the parent entities that defines the assignment, e.g. 'indicators'
    :param assignment_id_field: name of the property of an assignment object which specifies the child ID,
                                e.g. 'indicator_id'
    :return: a n*m matrix, where n is the number of parents and m is the number of children
    """
    # Map from child entity IDs to matrix indices
    child_indices = {child.id: idx for idx, child in enumerate(children)}

    matrix = np.ma.masked_all((len(parents), len(children)))
    for parent_idx, parent in enumerate(parents):
        for assoc in getattr(parent, assignment_name):
            child_idx = child_indices[getattr(assoc, assignment_id_field)]
            matrix[parent_idx, child_idx] = assoc.amount
    return matrix


def country_risk_matrix(countries: List[Country], indicators: List[Indicator]) -> np.array:
    """
    Assembles indicator values for countries in a matrix.

    :param countries: list of the countries to assemble values for
    :param indicators: list of the indicators to assemble values for
    :return: a m*n matrix, where m is the number of countries and n is the number of indicators
    """
    return mk_assignment_matrix(countries, indicators, 'indicators', 'indicator_id')


def material_country_matrix(materials: List[Material], countries: List[Country]) -> np.array:
    """
    Assembles country shares for materials in a matrix.

    :param materials: list of the materials to assemble values for
    :param countries: list of the countries to assemble values for
    :return: a m*n matrix, where m is the number of materials and n is the number of countries
    """
    return mk_assignment_matrix(materials, countries, 'countries', 'country_id')


def component_material_matrix(components: List[Component], materials: List[Material]) -> np.array:
    """
    Assembles materials shares for components in a matrix.

    :param components: list of the components to assemble values for
    :param materials: list of the materials to assemble values for
    :return: a m*n matrix, where m is the number of components and n is the number of materials
    """
    return mk_assignment_matrix(components, materials, 'materials', 'material_id')


def transfer_activity(activity_vector: np.array, assignment_matrix: np.array) -> np.array:
    """
    Transfers activity values to a different entity.

    Given an activity vector for entity A and an assignment matrix from entity A to entity B, produces an activity
    vector for entity B.

    :param activity_vector: an activity vector of dimension m
    :param assignment_matrix: an assignment matrix of dimension m*n
    :return: an activity vector of dimension n
    """
    # Convert into a row vector
    transposed = activity_vector[:, np.newaxis].transpose()
    transferred = np.ma.dot(transposed, assignment_matrix)
    # Convert back into a column vector
    return transferred[0, :]


def component_activity_vector(product: Product, components: List[Component]) -> np.array:
    """
    Computes the normalized activity for each component.

    :param product: product whose components to examine
    :param components: list of components in the product
    :return: row vector specifying component activities; sums to one
    """
    # Make the association objects available by component ID
    assoc_by_id = {assoc.component_id: assoc for assoc in product.components}
    # Build vector of component weights
    component_weights = np.array([c.weight for c in components])
    # Build vector of component amounts, making sure the order is the same as in components list
    component_amounts = np.array([assoc_by_id[component.id].amount for component in components])
    # Activity is weight times amount normalized
    activities = normalize(component_weights * component_amounts)
    return activities


def product_weight(product: Product, components: List[Component]) -> float:
    """
    Computes the total weight of a product's components.

    :param product: product whose components to examine
    :param components: list of components in the product
    :return: weight of the product
    """
    # Make the association objects available by component ID
    assoc_by_id = {assoc.component_id: assoc for assoc in product.components}
    # Build vector of component weights
    component_weights = np.array([c.weight for c in components])
    # Build vector of component amounts, making sure the order is the same as in components list
    component_amounts = np.array([assoc_by_id[component.id].amount for component in components])
    # Activity is weight times amount normalized
    activities = component_weights * component_amounts
    return sum(activities)
