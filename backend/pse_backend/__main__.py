#!/usr/bin/env python3
"""
Command-line entry point for the backend. Allows running a development server and provides an administrative CLI.
"""

import click
import uvicorn

from pse_backend import data_import


@click.group()
def cli():
    pass


@click.command()
@click.option('--port', type=int, default=8000, help='The port to listen on')
@click.option('--dev', type=bool, default=False, help='Whether to restart the server on code changes')
def run(port, dev):
    """
    Run the backend in a local development server.
    """
    uvicorn.run('pse_backend.app:app', host="0.0.0.0", port=port, reload=dev)


@click.command(name='import-entity')
@click.argument("entity", type=str)
@click.argument("csv_file", type=click.File('r'))
def import_entity(entity, csv_file):
    """
    Import entities from a CSV file.

    Reads every line from the the CSV file as a new entity. Columns must be labeled with the entity's field names.

    If an entity cannot be created (e.g., if database constraints are violated), the entity is skipped and a warning
    message is written to STDERR.
    """
    data_import.import_entities_from_csv(entity, csv_file, update=True)


@click.command(name='import-assignment')
@click.option("--parent", required=True, help="Parent entity")
@click.option("--attribute", required=True, help="Relationship attribute name")
@click.argument("csv_file", type=click.File('r'))
def import_assignment(parent, attribute, csv_file):
    """
    Import an assignment from a CSV file.

    Reads the CSV file INPUT and reads every line as an assignment record.
    """
    data_import.import_assignment(parent, attribute, csv_file)


cli.add_command(import_entity)
cli.add_command(import_assignment)


cli.add_command(run)

# When called as a script, run the uvicorn server. This helps with debugging from within an IDE.
if __name__ == "__main__":
    cli()
