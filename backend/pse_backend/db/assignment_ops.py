import abc
from typing import Any, TypeVar, Generic, Type, Iterable

from pse_backend.db.base import Base
from pse_backend.db.entity_ops import EntityOps

ValueT = TypeVar('ValueT')


class AssignmentOps(abc.ABC, Generic[ValueT]):
    """
    Defines the common interface for assignments.
    """

    dbo_type: Type[Base]
    parent_spec: Type[EntityOps]
    child_spec: Type[EntityOps]

    parent_id_column: str
    child_id_column: str
    value_column: str

    @classmethod
    def parent_dbo_type(cls) -> Type[Base]:
        return cls.parent_spec.dbo_type

    @classmethod
    def parent_entity_name(cls) -> str:
        return cls.parent_spec.out_type.__name__

    @classmethod
    def child_dbo_type(cls) -> Type[Base]:
        return cls.child_spec.dbo_type

    @classmethod
    def child_entity_name(cls) -> str:
        return cls.child_spec.out_type.__name__

    @staticmethod
    @abc.abstractmethod
    def relation(dbo_instance: Base):
        pass

    def __init__(self):
        pass

    @abc.abstractmethod
    def get_assignments(self, parent_id: int) -> Iterable[Any]:
        """
        Returns all assignment objects associated with a given parent entity.

        :param parent_id: the parent entity to look up
        :return: an iterable of all assignment objects that are linked to this parent
        """

    @abc.abstractmethod
    def get_children(self, parent_id: int) -> Iterable[int]:
        """
        Returns all children of a given parent entity.

        :param parent_id: the parent entity to look up
        :return: an iterable of all child entity IDs that are linked to this parent
        """

    @abc.abstractmethod
    def add(self, parent_id: int, child_id: int, value: ValueT):
        """
        Adds an association. If the association exists, what happens depends on the concrete type of the assignment.

        :param parent_id: ID of the parent entity
        :param child_id: ID of the entity to add
        :param value: label value
        :return: an ORM object representing the product-component association
        """

    @abc.abstractmethod
    def clear(self, parent_id: int) -> None:
        """
        Removes all assignments from a given parent entity.

        :param parent_id: the parent entity to clear
        """
