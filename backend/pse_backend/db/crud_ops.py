from typing import List, TypeVar

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Session

from pse_backend.db.entity_ops import EntityOps
from .exceptions import EntityNotFoundError, NotUniqueError, CreateError, UpdateError
from .. import models

SpecT = TypeVar('SpecT', bound=EntityOps)
ModelT = TypeVar('ModelT', bound=models.EntityFull)
NewT = TypeVar('NewT', bound=models.Entity)
UpdateT = TypeVar('UpdateT', bound=models.Entity)


class CrudOps(EntityOps):
    """
    Defines operations for CRUD functionality.
    """
    def __init__(self, session: Session):
        self.session = session

    def get_all(self, skip: int = 0, limit: int = None) -> List[ModelT]:
        """
        Retrieve a paginated list of all entities.

        :param skip: list offset to start at
        :param limit: number of entitys to return
        :return: a list of entity DBOs
        """
        query = self.session.query(self.dbo_type).offset(skip)
        if limit is not None:
            query = query.limit(limit)
        dbos = query.all()
        return [self.from_orm(dbo) for dbo in dbos]

    def get(self, entity_id: int) -> ModelT:
        """
        Retrieve an individual entity.

        :param entity_id: ID of the entity
        :return: entity DBO

        :raises: EntityNotFoundError if no entity with the given ID exists
        """
        db_item = self.session.query(self.dbo_type).get(entity_id)
        if not db_item:
            raise EntityNotFoundError(self.dbo_type, entity_id)
        return self.from_orm(db_item)

    def get_where(self, query, unique: bool = False) -> List[ModelT]:
        """
        Retrieve all entities with a certain configuration of field values.

        :param query: a dictionary of field-value assignments
        :param unique: whether the uniqueness of the result should be asserted
        :return: all entities matching the query
        """
        db_items = self.session.query(self.dbo_type).filter_by(**query).all()

        if unique and len(db_items) != 1:
            raise NotUniqueError(query, self.dbo_type, count=len(db_items))

        return [self.from_orm(dbo) for dbo in db_items]

    def create(self, item: NewT) -> ModelT:
        """
        Create an entity.

        Returns the full entity object including its ID.

        :param item: entity data
        :return: entity DBO
        :raises: CreateError if the entity could not be created
        """
        try:
            # noinspection PyArgumentList
            db_item = self.create_orm(item, self.session)  # Create the DBO, calling its constructor
            self.session.add(db_item)
            self.session.commit()
            self.session.refresh(db_item)
            return self.from_orm(db_item)
        except SQLAlchemyError as e:
            self.session.rollback()
            raise CreateError(item, e)

    def update(self, entity_id: int, update: UpdateT) -> ModelT:
        """
        Update attribute(s) of an entity.

        :param entity_id: ID of the entity to update
        :param update: model containing attributes to be updated
        :return: the updated entity
        :raises: EntityNotFoundError if there is no entity with the given ID
        :raises: UpdateError if the update cannot be performed for any other reason
        """
        try:
            db_item = self.session.query(self.dbo_type).get(entity_id)
            if not db_item:
                raise EntityNotFoundError(self.__class__, entity_id)
            for field, value in update.dict().items():
                if value is not None:
                    db_item.__setattr__(field, value)
            self.session.commit()
            return self.from_orm(db_item)
        except SQLAlchemyError as e:
            self.session.rollback()
            raise UpdateError(update, e)

    def delete(self, entity_id: int) -> None:
        """
        Delete a entity given its ID.

        :param entity_id: ID of the entity to delete
        :raises: EntityNotFoundError if there is no entity with the given ID
        """
        db_item = self.session.query(self.dbo_type).get(entity_id)
        if not db_item:
            raise EntityNotFoundError(self.__class__, entity_id)

        self.session.delete(db_item)
        self.session.commit()
