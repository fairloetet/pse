from typing import Type

from slugify import slugify
from sqlalchemy.orm import Session

from pse_backend.api import models
from pse_backend.db.base import Base


class EntityOps:
    """
    Base class for specifications of entities.
    """
    dbo_type: Type[Base]

    out_type: Type[models.EntityFull]
    new_type: Type[models.Entity]
    update_type: Type[models.Entity]

    @classmethod
    def from_orm(cls, dbo_instance) -> models.EntityFull:
        return cls.out_type.from_orm(dbo_instance)

    @classmethod
    def create_orm(cls, data: models.Entity, session: Session) -> Base:
        # Somehow, pytype can't prove that the dbo_type attribute exists for mixin instances
        return cls.dbo_type(**data.dict())  # type: ignore


def suffix_slug(slug: str, entity: Type[Base], session: Session) -> str:
    suffix_no = 1
    suffixed_slug = slug  # the first slug is not suffixed
    while session.query(entity).filter_by(slug=suffixed_slug).count():
        suffix_no += 1
        suffixed_slug = f'{slug}-{suffix_no}'
    return suffixed_slug


class Sluggy(EntityOps):
    """
    Mixin class that auto-fills the slug field.
    """
    @classmethod
    def create_orm(cls, data: models.Entity, session: Session) -> Base:
        slug = data.slug
        if not slug:
            slug = slugify(data.name)

        slug = suffix_slug(slug, cls.dbo_type, session)
        return super(Sluggy, cls).create_orm(data.copy(update={'slug': slug}), session)
