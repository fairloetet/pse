from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from pse_backend.config import get_config

db_config = get_config().database
DATABASE_URL = f'postgresql://{db_config.user}:{db_config.password}@' \
               f'{db_config.host}:{db_config.port}/{db_config.dbname}'
engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
