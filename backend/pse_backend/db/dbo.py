"""
Definitions of database objects and associated operations.

This module defines SQLAlchemy classes that make up the database schema of the Fairtronics application. These classes
all inherit from `Base`, SQLAlchemy’s declarative base class, and are specified using SQLAlchemy’s *Declarative*
ORM mapping language. For more information, see
[the SQLAlchemy documentation](https://docs.sqlalchemy.org/en/13/orm/tutorial.html).

There are two types of DBO classes: Entities and assignments. Entities correspond to objects such as products, while
assignments represent many-to-many relationships between entities, such as the assignment of an indicator value to a
country.

DBO classes tend to have companions which define the operations available for the entity or assignment. These are called
Ops classes and loosely implement a typeclass pattern. Ops classes have the following responsibilities:

- Converting between API data structures (the data structures that are exposed via the API) and DBOs (which are stored
  in the database)
- Providing an easy-to-use interface to the management of database-stored objects in terms of API data structures
  (i.e., DBO classes should not leak outside of the `db` module)

Ops classes for entities inherit from `EntityOps` (or one of its subclasses), while ops classes for assignments inherit
from `AssignmentOps` (or one of its subclasses).

The relationship between DBO classes and Ops classes does need to be one-to-one. E.g. in the case of bidirectional
assignments, there are two views of the assignments with interchanged parent-child rows. I.e., it is possible to access
indicator values through a country, or access country-specific values through an indicator. These two views are
represented by two separate Ops classes.

The Ops classes are assembled in the `Facade`, and are those part of the interface of the `db` module.
"""

from typing import List, Dict, Any

from sqlalchemy import Column, Integer, String, ForeignKey, Float, CheckConstraint
from sqlalchemy.ext.orderinglist import ordering_list
from sqlalchemy.orm import relationship

from pse_backend import models
from pse_backend.db.base import Base
from pse_backend.db.crud_ops import CrudOps
from pse_backend.db.entity_ops import Sluggy
from pse_backend.db.labelled_assignment_ops import LabelledAssignmentOps
from pse_backend.db.ordered_list_ops import OrderedListOps


# ---------------------------------------------------------------------
# Entities
# ---------------------------------------------------------------------

class Product(Base):
    __tablename__ = "products"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False, index=True)
    slug = Column(String, index=True, unique=True)
    description = Column(String)

    component_relation = relationship("ProductComponents",
                                      order_by="ProductComponents.position",
                                      collection_class=ordering_list('position'))

    def __repr__(self):
        return f"<Product id={self.id} name='{self.name}' slug='{self.slug}'>"

    @property
    def components(self) -> List[Dict[str, Any]]:
        """
        The IDs of components that are associated with this product.

        An ID may occur several times if the component is in the product several times.
        """
        return [{"id": assoc.id, "component_id": assoc.component_id, "amount": assoc.amount} for assoc in self.component_relation]


class ProductOps(Sluggy, CrudOps):
    dbo_type = Product

    new_type = models.ProductNew
    update_type = models.ProductUpdate
    out_type = models.Product

    def __init__(self, session):
        CrudOps.__init__(self, session)

        self.components = ProductComponentsOps(session)


class Component(Base):
    __tablename__ = "components"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False, index=True)
    slug = Column(String, index=True, unique=True)
    weight = Column(Float, nullable=False)

    material_relation = relationship("ComponentMaterialAssignment")

    def __repr__(self):
        return f"<Component id={self.id} name='{self.name}'>"

    @property
    def materials(self) -> List[Dict[str, Any]]:
        """
        The list of materials that are associated with this component.

        Each entry in the list is a dictionary containing the keys:

            - 'material_id': the ID of the associated material
            - 'amount': the share of the component weight assigned to the material
        """
        return [{'material_id': assoc.material_id,
                 'amount': assoc.amount} for assoc in self.material_relation]


class ComponentOps(Sluggy, CrudOps):
    dbo_type = Component

    new_type = models.ComponentNew
    update_type = models.ComponentUpdate
    out_type = models.Component

    def __init__(self, session):
        CrudOps.__init__(self, session)

        self.materials = ComponentMaterialAssignmentOps(session)


class Material(Base):
    __tablename__ = "materials"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False, index=True)
    slug = Column(String, index=True, unique=True)

    country_relation = relationship("MaterialCountryAssignment",
                                    back_populates="material",
                                    cascade="all, delete-orphan")

    def __repr__(self):
        return f"<Material id={self.id} name='{self.name}'>"

    @property
    def countries(self) -> List[Dict[str, Any]]:
        """
        The list of countries of origin that are associated with this material.

        Each entry in the list is a dictionary containing the keys:

            - 'country_id': the ID of the associated country
            - 'amount': the market share of the country for the material
        """
        return [{'country_id': assoc.country_id,
                 'amount': assoc.amount} for assoc in self.country_relation]


class MaterialOps(Sluggy, CrudOps):
    dbo_type = Material

    new_type = models.MaterialNew
    update_type = models.MaterialUpdate
    out_type = models.Material

    def __init__(self, session):
        CrudOps.__init__(self, session)

        self.countries = MaterialCountryAssignmentOps(session)


class Country(Base):
    __tablename__ = "countries"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False, index=True)
    code = Column(String, nullable=False, index=True, unique=True)
    slug = Column(String, index=True, unique=True)

    material_relation = relationship("MaterialCountryAssignment",
                                     back_populates="country",
                                     cascade="all, delete-orphan")

    indicator_relation = relationship("IndicatorCountryAssignment",
                                      back_populates="country",
                                      cascade="all, delete-orphan")

    def __repr__(self):
        return f"<Country id={self.id} name='{self.name}' code='{self.code}'>"

    @property
    def materials(self) -> List[Dict[str, Any]]:
        """
        The list of materials that are associated with this country.

        Each entry in the list is a dictionary containing the keys:

            - 'material_id': the ID of the associated material
            - 'amount': the market share of the country for the material
        """
        return [{'material_id': assoc.material_id,
                 'amount': assoc.amount} for assoc in self.material_relation]

    @property
    def indicators(self) -> List[Dict[str, Any]]:
        """
        The list of indicators that are associated with this material.

        Each entry in the list is a dictionary containing the keys:

            - 'indicator_id': the ID of the associated country
            - 'amount': the indicator value for this country
        """
        return [{'indicator_id': assoc.indicator_id,
                 'amount': assoc.amount} for assoc in self.indicator_relation]


class CountryOps(Sluggy, CrudOps):
    dbo_type = Country

    new_type = models.CountryNew
    update_type = models.CountryUpdate
    out_type = models.Country

    def __init__(self, session):
        CrudOps.__init__(self, session)

        self.indicators = CountryIndicatorAssignmentOps(session)


class Indicator(Base):
    __tablename__ = "indicators"
    __table_args__ = (
        CheckConstraint("direction='ascending' OR direction='descending'", name='check_direction'),
    )

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False, index=True)
    slug = Column(String, index=True, unique=True)
    direction = Column(String, nullable=False)

    country_relation = relationship("IndicatorCountryAssignment",
                                    back_populates="indicator",
                                    cascade="all, delete-orphan")

    def __repr__(self):
        return f"<Indicator id={self.id} name='{self.name}'>"

    @property
    def countries(self) -> List[Dict[str, Any]]:
        """
        The list of countries that are associated with this indicator.

        Each entry in the list is a dictionary containing the keys:

            - 'country_id': the ID of the associated country
            - 'amount': the indicator value for the country
        """
        return [{'country_id': assoc.country_id,
                 'amount': assoc.amount} for assoc in self.country_relation]


class IndicatorOps(Sluggy, CrudOps):
    dbo_type = Indicator

    new_type = models.IndicatorNew
    update_type = models.IndicatorUpdate
    out_type = models.Indicator

    def __init__(self, session):
        CrudOps.__init__(self, session)

        self.countries = IndicatorCountryAssignmentOps(session)


# ---------------------------------------------------------------------
# Assignments
# ---------------------------------------------------------------------

class ProductComponents(Base):
    """
    Models the association between products and components.

    Products own an ordered list of components, with duplicates permitted. This association is modeled here as
    an instance of the *association object* pattern. Each association is annotated with the position of the
    associated component in the component list. The position index is managed using the `ordering_list`
    SQLAlchemy extension.
    """
    __tablename__ = "product_components"

    id = Column(Integer, primary_key=True, index=True)
    product_id = Column(Integer, ForeignKey("products.id", ondelete='CASCADE'), index=True)
    position = Column(Integer, index=True)
    component_id = Column(Integer, ForeignKey("components.id", ondelete='CASCADE'))
    amount = Column(Float, nullable=False, default=1)


class ProductComponentsOps(OrderedListOps[float]):
    dbo_type = ProductComponents
    parent_spec = ProductOps
    parent_id_column = 'product_id'
    child_spec = ComponentOps
    child_id_column = 'component_id'
    value_column = 'amount'

    out_type = models.ComponentAssignment
    new_type = models.ComponentAssignmentNew
    update_type = models.ComponentAssignmentUpdate

    @staticmethod
    def relation(dbo_instance: Product):
        return dbo_instance.component_relation

    @staticmethod
    def mk_assoc(parent_id: int, child_id: int, amount: float):
        return ProductComponents(product_id=parent_id, component_id=child_id, amount=amount)


class ComponentMaterialAssignment(Base):
    __tablename__ = "component_material_assignment"

    component_id = Column(Integer, ForeignKey('components.id', ondelete='CASCADE'), primary_key=True)
    material_id = Column(Integer, ForeignKey('materials.id', ondelete='CASCADE'), primary_key=True)

    amount = Column(Float, nullable=False)

    def __repr__(self):
        return f"<ComponentMaterialAssignment " \
               f"component_id={self.component_id} material_id={self.material_id} amount={self.amount}>"


class ComponentMaterialAssignmentOps(LabelledAssignmentOps[float]):
    dbo_type = ComponentMaterialAssignment
    parent_spec = ComponentOps
    parent_id_column = 'component_id'
    child_spec = MaterialOps
    child_id_column = 'material_id'
    value_column = 'amount'

    @staticmethod
    def relation(dbo_instance: Component):
        return dbo_instance.material_relation

    @staticmethod
    def mk_labelled_assoc(parent_id: int, child_id: int, value: float):
        return ComponentMaterialAssignment(component_id=parent_id, material_id=child_id, amount=value)


class MaterialCountryAssignment(Base):
    __tablename__ = "material_country_assignment"

    material_id = Column(Integer, ForeignKey('materials.id', ondelete='CASCADE'), primary_key=True)
    material = relationship("Material", back_populates="country_relation")
    country_id = Column(Integer, ForeignKey('countries.id', ondelete='CASCADE'), primary_key=True)
    country = relationship("Country", back_populates="material_relation")

    amount = Column(Float, nullable=False)

    def __repr__(self):
        return f"<MaterialCountryAssignment " \
               f"component_id={self.component_id} material_id={self.material_id} amount={self.amount}>"


class MaterialCountryAssignmentOps(LabelledAssignmentOps[float]):
    dbo_type = MaterialCountryAssignment
    parent_spec = MaterialOps
    parent_id_column = 'material_id'
    child_spec = CountryOps
    child_id_column = 'country_id'
    value_column = 'amount'

    @staticmethod
    def relation(dbo_instance: Material):
        return dbo_instance.country_relation

    @staticmethod
    def mk_labelled_assoc(parent_id: int, child_id: int, value: float) -> MaterialCountryAssignment:
        return MaterialCountryAssignment(material_id=parent_id, country_id=child_id, amount=value)


class IndicatorCountryAssignment(Base):
    __tablename__ = "indicator_country_assignment"

    indicator_id = Column(Integer, ForeignKey('indicators.id', ondelete='CASCADE'), primary_key=True)
    indicator = relationship("Indicator", back_populates="country_relation")
    country_id = Column(Integer, ForeignKey('countries.id', ondelete='CASCADE'), primary_key=True)
    country = relationship("Country", back_populates="indicator_relation")

    amount = Column(Float, nullable=False)

    def __repr__(self):
        return f"<IndicatorCountryAssignment " \
               f"indicator_id={self.indicator_id} material_id={self.material_id} amount={self.amount}>"


class IndicatorCountryAssignmentOps(LabelledAssignmentOps[float]):
    dbo_type = IndicatorCountryAssignment
    parent_spec = IndicatorOps
    parent_id_column = 'indicator_id'
    child_spec = CountryOps
    child_id_column = 'country_id'
    value_column = 'amount'

    @staticmethod
    def relation(dbo_instance: Country):
        return dbo_instance.country_relation

    @staticmethod
    def mk_labelled_assoc(parent_id: int, child_id: int, value: float) -> IndicatorCountryAssignment:
        return IndicatorCountryAssignment(indicator_id=parent_id, country_id=child_id, amount=value)


class CountryIndicatorAssignmentOps(LabelledAssignmentOps[float]):
    dbo_type = IndicatorCountryAssignment
    parent_spec = CountryOps
    parent_id_column = 'country_id'
    child_spec = IndicatorOps
    child_id_column = 'indicator_id'
    value_column = 'amount'

    @staticmethod
    def relation(dbo_instance: Indicator):
        return dbo_instance.indicator_relation

    @staticmethod
    def mk_labelled_assoc(parent_id: int, child_id: int, value: float) -> IndicatorCountryAssignment:
        return IndicatorCountryAssignment(country_id=parent_id, indicator_id=child_id, amount=value)
