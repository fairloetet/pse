import abc
import logging
from typing import Any, Iterable, Type, TypeVar, Generic

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Session

from pse_backend import models
from pse_backend.db.assignment_ops import AssignmentOps
from .exceptions import EntityNotFoundError, UpdateError


ValueT = TypeVar('ValueT')
ModelT = TypeVar('ModelT')
NewT = TypeVar('NewT')
UpdateT = TypeVar('UpdateT')

class OrderedListOps(Generic[ValueT], AssignmentOps[ValueT], abc.ABC):
    """
    Defines operations for ordered list relationships.
    """

    out_type: Type[models.EntityFull]
    new_type: Type[models.Entity]
    update_type: Type[models.Entity]

    def __init__(self, session: Session):
        AssignmentOps.__init__(self)
        self.session = session

    @classmethod
    def from_orm(cls, dbo_instance) -> ModelT:
        return cls.out_type.from_orm(dbo_instance)


    def get_assignments(self, parent_id: int) -> Iterable[ModelT]:
        """
        Returns all assignment objects associated with a given parent entity.

        :param parent_id: the parent entity to look up
        :return: an iterable of all assignment objects that are linked to this parent
        """
        associations = self.session.query(self.dbo_type).filter_by(**{self.parent_id_column: parent_id})
        return associations

    def get_children(self, parent_id: int) -> Iterable[int]:
        """
        Returns all children of a given parent entity.

        Children occur several times if they have multiple associations.

        :param parent_id: the parent entity to look up
        :return: an iterable of all child entity IDs that are linked to this parent
        """
        for association in self.get_assignments(parent_id):
            yield getattr(association, self.child_id_column)

    def append(self, parent_id: int, child_id: int, value: ValueT):
        """
        Adds a component to the end of the list.

        :param parent_id: ID of the parent entity
        :param child_id: ID of the entity to add
        :param value: label of the association, e.g. number of components
        :return: a ProductComponents object representing the product-component association
        """
        assoc = self.dbo_type(**{self.parent_id_column: parent_id,
                                 self.child_id_column: child_id,
                                 self.value_column: value})
        parent_object = self.session.query(self.parent_dbo_type()).get(parent_id)
        self.relation(parent_object).append(assoc)
        self.session.commit()
        return assoc

    def add(self, parent_id: int, child_id: int, value: ValueT):
        """
        Adds an entity to the end of the list.

        :param parent_id: ID of the parent entity
        :param child_id: ID of the entity to add
        :param value: label of the association, e.g. number of components
        :return: a ProductComponents object representing the product-component association
        """
        self.append(parent_id, child_id, value)

    def update(self, entity_id: int, update: UpdateT) -> ModelT:
        """
        Update attribute(s) of an entity.

        :param entity_id: ID of the entity to update
        :param update: model containing attributes to be updated
        :return: the updated entity
        :raises: EntityNotFoundError if there is no entity with the given ID
        :raises: UpdateError if the update cannot be performed for any other reason
        """
        try:
            db_item = self.session.query(self.dbo_type).get(entity_id)
            if not db_item:
                raise EntityNotFoundError(self.__class__, entity_id)
            for field, value in update.dict().items():
                if value is not None:
                    db_item.__setattr__(field, value)
            self.session.commit()
            return self.from_orm(db_item)
        except SQLAlchemyError as e:
            self.session.rollback()
            raise UpdateError(update, e)

    def remove_assignment(self, parent_id: int, assignment_id: int):
        """
        Removes a component assignment from a product's component list.

        :param parent_id: ID of the assignment object
        """
        assignment_object = self.session.query(self.dbo_type).get(assignment_id)
        parent_object = self.session.query(self.parent_dbo_type()).get(parent_id)
        self.relation(parent_object).remove(assignment_object)
        self.session.delete(assignment_object)
        self.session.commit()

    def clear(self, parent_id: int) -> None:
        """
        Removes all assignments from a given parent entity.

        :param parent_id: the parent entity to clear
        """
        associations = self.session.query(self.dbo_type).filter_by(**{self.parent_id_column: parent_id})
        for association in associations:
            self.session.delete(association)
        self.session.commit()
