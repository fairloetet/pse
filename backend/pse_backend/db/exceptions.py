from typing import Type, Dict, Optional

from pse_backend import PseException
from pse_backend.models import Entity


class EntityNotFoundError(PseException):
    """
    Raised if a requested entity does not exist.
    """

    def __init__(self, entity_type: Type, entity_id: int):
        """
        :param entity_type: DBO type of the entity that was not found
        :param entity_id: ID of the entity that was not found
        """
        self.entity_type = entity_type
        self.entity_id = entity_id
        message = f"Entity not found: {entity_type.__name__} with ID {entity_id}"
        PseException.__init__(self, message)

    def __repr__(self):
        return f"<EntityNotFoundError entity_type={self.entity_type} entity_id={self.entity_id}>"


class NotUniqueError(PseException):
    """
    Raised if a query was supposed to return one result but returned more.
    """

    def __init__(self, query: Dict[str, str], entity_type: Type, count: Optional[int] = None):
        """
        :param entity_type: DBO type of the entity that was queried
        """
        self.entity_type = entity_type
        self.query = query
        message = f"Query {query} for entity {entity_type.__name__} not unique"
        if count is not None:
            message += f": {count} instances found"
        PseException.__init__(self, message)

    def __repr__(self):
        return f"<NotUniqueError query={self.query} entity_type={self.entity_type}>"


class CreateError(PseException):
    """
    Raised if an entity cannot be created.
    """

    def __init__(self, item: Entity, underlying: Exception):
        self.item = item
        self.underlying = underlying
        message = f"Unable to create {type(item).__name__} with data {item}. Reason: {underlying}"
        PseException.__init__(self, message)

    def __repr__(self):
        return f"<CreateError data={self.item} underlying={self.underlying}>"



class UpdateError(PseException):
    """
    Raised if an entity cannot be updated.
    """

    def __init__(self, item: Entity, underlying: Exception):
        self.item = item
        self.underlying = underlying
        message = f"Unable to update {type(item).__name__} with data {item}. Reason: {underlying}"
        PseException.__init__(self, message)

    def __repr__(self):
        return f"<CreateError data={self.item} underlying={self.underlying}>"
