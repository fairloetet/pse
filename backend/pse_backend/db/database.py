from sqlalchemy.orm import Session

from pse_backend.db.assignment_ops import AssignmentOps
from pse_backend.db.crud_ops import CrudOps
from pse_backend.db.session import SessionLocal
from pse_backend.db.dbo import ComponentOps, ProductOps, CountryOps, MaterialOps, IndicatorOps


def get_database() -> 'Database':
    """
    Obtain a Facade object which can be used to access the database.

    :return: a Facade
    """
    session = SessionLocal()
    return Database(session)


class Database:
    """
    Interface to the database subsystem.
    """
    def __init__(self, session: Session):
        self.session = session

        self.product = ProductOps(session)
        self.component = ComponentOps(session)

        self.country = CountryOps(session)
        self.material = MaterialOps(session)
        self.indicator = IndicatorOps(session)

    def close(self):
        self.session.close()

    def get_entity(self, entity_name: str) -> CrudOps:
        return self.__getattribute__(entity_name.lower())

    def get_relationship(self, parent_entity_name: str, relationship_name: str) -> AssignmentOps:
        parent = self.get_entity(parent_entity_name)
        return parent.__getattribute__(relationship_name)
