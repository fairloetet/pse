import abc
import logging
from typing import Any, TypeVar, Iterable

from sqlalchemy.orm import Session

from pse_backend.db.assignment_ops import AssignmentOps

ValueT = TypeVar('ValueT')


class LabelledAssignmentOps(AssignmentOps[ValueT], abc.ABC):
    """
    Defines operations for labelled assignments, i.e. when the assignment is annotated with an additional value.
    """

    def __init__(self, session: Session):
        AssignmentOps.__init__(self)
        self.session = session

    def get_assignments(self, parent_id: int) -> Iterable[Any]:
        """
        Returns all assignment objects associated with a given parent entity.

        :param parent_id: the parent entity to look up
        :return: an iterable of all assignment objects that are linked to this parent
        """
        associations = self.session.query(self.dbo_type).filter_by(**{self.parent_id_column: parent_id})
        return associations

    def get_children(self, parent_id: int) -> Iterable[int]:
        """
        Returns all children of a given parent entity.

        Children occur several times if they have multiple associations.

        :param parent_id: the parent entity to look up
        :return: an iterable of all child entity IDs that are linked to this parent
        """
        for association in self.get_assignments(parent_id):
            yield getattr(association, self.child_id_column)

    def add(self, parent_id: int, child_id: int, value: ValueT):
        """
        Adds an association. If the association exists, it is overwritten.

        :param parent_id: ID of the parent entity
        :param child_id: ID of the entity to add
        :param value: label value
        :return: an ORM object representing the product-component association
        """
        # Overwrite any possibly existing association
        assoc = self.session.query(self.dbo_type).get({self.parent_id_column: parent_id,
                                                       self.child_id_column: child_id})
        if not assoc:
            assoc = self.dbo_type(**{self.parent_id_column: parent_id,
                                     self.child_id_column: child_id})

        setattr(assoc, self.value_column, value)

        self.session.add(assoc)
        self.session.commit()
        return assoc

    def remove(self, parent_id: int, child_id: int):
        """
        Removes an association. Emits a warning if the association does not exist.

        :param parent_id: ID of the parent entity
        :param child_id: ID of the entity to add
        """
        assoc = self.session.query(self.dbo_type).get({self.parent_id_column: parent_id,
                                                       self.child_id_column: child_id})
        if assoc:
            self.session.delete(assoc)
            self.session.commit()
        else:
            logging.warning(f"Attempted to remove nonexisting association "
                            f"between {self.parent_dbo_type.__name__} {parent_id} "
                            f"and {self.child_dbo_type.__name__} {child_id}")

    def clear(self, parent_id: int) -> None:
        """
        Removes all assignments from a given parent entity.

        :param parent_id: the parent entity to clear
        """
        associations = self.session.query(self.dbo_type).filter_by(**{self.parent_id_column: parent_id})
        for association in associations:
            self.session.delete(association)
        self.session.commit()
