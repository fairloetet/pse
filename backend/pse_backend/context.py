from fastapi import FastAPI
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from pse_backend.api.endpoints import build_app
from pse_backend.config.config import Config
from pse_backend.db import Database


class Context:
    """
    Collects the global components of the PSE backend.

    Construction recipes are expressed as class methods.
    """

    def __init__(self,
                 db: Database,
                 app: FastAPI):
        """
        Assemble a bunch of existing components into a Context.

        :param db: the database instance
        :param app: the webapp
        """
        self.db = db
        self.app = app

    @staticmethod
    def from_config(config: Config) -> 'Context':
        """
        Create a context from a configuration object.

        :param config: a config object
        :return: a context
        """
        db_config = config.database
        DATABASE_URL = f'postgresql://{db_config.user}:{db_config.password}@' \
                       f'{db_config.host}:{db_config.port}/{db_config.dbname}'
        engine = create_engine(DATABASE_URL)
        SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        session = SessionLocal()
        db = Database(session)

        app = build_app(db)

        return Context(db, app)
