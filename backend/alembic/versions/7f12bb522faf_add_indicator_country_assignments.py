"""Add indicator-country assignments

Revision ID: 7f12bb522faf
Revises: 0bc988f2e1a9
Create Date: 2019-10-09 13:00:35.216723

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7f12bb522faf'
down_revision = '0bc988f2e1a9'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('indicator_country_assignment',
    sa.Column('indicator_id', sa.Integer(), nullable=False),
    sa.Column('country_id', sa.Integer(), nullable=False),
    sa.Column('amount', sa.Float(), nullable=False),
    sa.ForeignKeyConstraint(['country_id'], ['countries.id'], ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['indicator_id'], ['indicators.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('indicator_id', 'country_id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('indicator_country_assignment')
    # ### end Alembic commands ###
