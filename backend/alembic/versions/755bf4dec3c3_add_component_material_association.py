"""Add Component-Material Association

Revision ID: 755bf4dec3c3
Revises: eabd4c1fb14e
Create Date: 2019-10-04 15:16:00.802077

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '755bf4dec3c3'
down_revision = 'eabd4c1fb14e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('component_material_assignment',
    sa.Column('component_id', sa.Integer(), nullable=False),
    sa.Column('material_id', sa.Integer(), nullable=False),
    sa.Column('amount', sa.Float(), nullable=False),
    sa.ForeignKeyConstraint(['component_id'], ['components.id'], ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['material_id'], ['materials.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('component_id', 'material_id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('component_material_assignment')
    # ### end Alembic commands ###
