"""Merge multiple heads

Revision ID: 0e56f1fe99b7
Revises: 755bf4dec3c3, 818fb278b1c4
Create Date: 2019-10-07 15:51:52.581484

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0e56f1fe99b7'
down_revision = ('755bf4dec3c3', '818fb278b1c4')
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
