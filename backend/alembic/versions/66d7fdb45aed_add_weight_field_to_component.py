"""Add weight field to Component

Revision ID: 66d7fdb45aed
Revises: 44ef9af173a9
Create Date: 2020-01-30 09:56:24.674397

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '66d7fdb45aed'
down_revision = '44ef9af173a9'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('components', sa.Column('weight', sa.Float(), nullable=True))
    op.execute('UPDATE components SET weight = 0.0')
    op.alter_column('components', 'weight', nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('components', 'weight')
    # ### end Alembic commands ###
