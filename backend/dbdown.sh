#!/usr/bin/env bash
# Stops the development database server started with dbup.sh.

set -e

VOLUME_NAME=pse-dev-db

docker stop psedb
docker volume rm ${VOLUME_NAME}
