#!/usr/bin/env bash
# Initialises and migrates the database if necessary and then starts up the
# backend.
#
# Assumes that all dependencies are installed and accessible.
#
# Uses environment variables for configuration:
#
# DB_HOST: hostname under which postgres is running
# DB_PORT: port the database is using
# DB_NAME: name of the database to use
# DB_USER: username to use for database connection
# DB_PASSWORD: passwort to use for database connection

set -e

# Create the pse database if it doesn't exist
# Magic SQL from https://stackoverflow.com/a/18389184
echo "SELECT 'CREATE DATABASE ${DB_NAME}' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = '${DB_NAME}')\gexec" \
    | PGPASSWORD=${DB_PASSWORD} psql -h ${DB_HOST} -p ${DB_PORT} -U ${DB_USER}

# Migrate the database if necessary
alembic upgrade head

# Import base data if available
if [ -f data/import_all.sh ]; then
    data/import_all.sh
fi

python -m pse_backend run
