import pytest
from sqlalchemy import event, create_engine
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy_utils import create_database, drop_database

from pse_backend.config import get_config
from pse_backend.db import dbo, Database
from pse_backend.db.session import engine


@pytest.fixture(scope="function")
def database():
    """
    Fixture for using the database from within a test.

    The fixture gives access to a db.Facade instance. After the test, it rolls back all changes.

    :return: a database Facade object
    """

    # Recipe for tests with rollback from
    # https://docs.sqlalchemy.org/en/13/orm/session_transaction.html#joining-a-session-into-an-external-transaction-such-as-for-test-suites

    # Set up
    # ------

    # connect to the database
    connection = engine.connect()

    # begin a non-ORM transaction
    trans = connection.begin()

    # bind an individual Session to the connection
    session = Session(bind=connection)

    # start the session in a SAVEPOINT...
    session.begin_nested()

    # then each time that SAVEPOINT ends, reopen it
    @event.listens_for(session, "after_transaction_end")
    def restart_savepoint(session, transaction):
        if transaction.nested and not transaction._parent.nested:

            # ensure that state is expired the way
            # session.commit() at the top level normally does
            # (optional step)
            session.expire_all()

            session.begin_nested()

    # Create and use facade
    # ---------------------
    db = Database(session)
    yield db

    # Tear down
    # ---------
    session.close()

    # rollback - everything that happened with the
    # Session above (including calls to commit())
    # is rolled back.
    trans.rollback()

    # return connection to the Engine
    connection.close()


@pytest.fixture
def test_database():
    """
    Creates a new, empty database to run tests on. Deletes the database after use.

    :return: a database Facade object
    """
    db_name = 'pse_test_database'
    db_config = get_config().database
    url = f'postgresql://{db_config.user}:{db_config.password}@' \
          f'{db_config.host}:{db_config.port}/{db_name}'

    create_database(url)

    engine = create_engine(url)
    dbo.Base.metadata.create_all(engine)

    test_db_session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    session = test_db_session()

    yield Database(session)

    drop_database(url)
