from typing import Type

import pytest

from pse_backend import models
from pse_backend.models import ComponentNew, ProductNew, MaterialNew, CountryNew, IndicatorNew, \
    MaterialAssignment, CountryAssignment, IndicatorAssignment, ProductUpdate, Entity, ComponentUpdate, \
    MaterialUpdate, CountryUpdate, IndicatorUpdate
from pse_backend.db import EntityNotFoundError
from pse_backend.db.crud_ops import CrudOps
from pse_backend.db.exceptions import CreateError


def check_update(ops: CrudOps, entity_id: int, update_model: Type[Entity], attribute: str):
    """
    Helper function to test the update operation for an entity.

    :param ops: Sub-object of the database to work on
    :param entity_id: ID of the entity to test
    :param update_model: Model for update requests of the entity
    :param attribute: attribute name to set
    """
    # Should be able to update the name
    updated = ops.update(entity_id, update_model(**{attribute: "MyUpdatedTestEntity"}))  # type: ignore
    # Should return the updated entity
    assert updated.id == entity_id
    assert updated.__getattribute__(attribute) == "MyUpdatedTestEntity"  # type: ignore
    # Should get the same data on subsequent request
    updated2 = ops.get(entity_id)  # type: ignore
    assert updated2 == updated


def test_product_lifecycle(database):
    # Should be able to create a new product
    product_new = ProductNew(name='MyTestProduct')
    product = database.product.create(product_new)
    assert product

    # Get by ID should return the same product
    product2 = database.product.get(product.id)
    assert product2.id == product.id
    assert product2.name == product.name

    # Created product should be in the list of all products
    all_products = database.product.get_all()
    assert any(c.id == product.id for c in all_products)

    # Should be able to update fields
    check_update(database.product, product.id, ProductUpdate, "name")
    check_update(database.product, product.id, ProductUpdate, "description")

    # Delete the product
    database.product.delete(product.id)

    # After deletion, the entity is not found
    with pytest.raises(EntityNotFoundError):
        database.product.get(product.id)


def test_component_lifecycle(database):
    # Should be able to create a new component
    component_new = ComponentNew(name='MyTestComponent', weight=1.0)
    component = database.component.create(component_new)
    assert component

    # Get by ID should return the same component
    component2 = database.component.get(component.id)
    assert component2.id == component.id
    assert component2.name == component.name

    # Created component should be in the list of all components
    all_components = database.component.get_all()
    assert any(c.id == component.id for c in all_components)

    # Should be able to update the name
    check_update(database.component, component.id, ComponentUpdate, "name")

    # Delete the component
    database.component.delete(component.id)

    # After deletion, the entity is not found
    with pytest.raises(EntityNotFoundError):
        database.component.get(component.id)


def test_product_components(database):
    # Create example data
    product = database.product.create(ProductNew(name='MyTestProduct'))
    component1 = database.component.create(ComponentNew(name='TestComponent1', weight=1.0))
    component2 = database.component.create(ComponentNew(name='TestComponent2', weight=1.0))

    # Add three components
    assignment1 = database.product.components.append(product.id, component1.id, 1.0)
    assert len(database.product.get(product.id).components) == 1

    assignment2 = database.product.components.append(product.id, component2.id, 1.0)
    assert len(database.product.get(product.id).components) == 2

    assignment3 = database.product.components.append(product.id, component1.id, 1.0)
    assert len(database.product.get(product.id).components) == 3

    # component 2 should be removed, both instances of component 1 should remain
    database.product.components.remove_assignment(product.id, assignment2.id)
    assert len(database.product.get(product.id).components) == 2
    assert all(cid == component1.id for cid in database.product.components.get_children(product.id))

    # first instance of component 1 should be updated
    updated_assignment = database.product.components.update(assignment1.id, models.ComponentAssignmentUpdate(amount=42.0))
    assert updated_assignment.amount == 42.0
    updated_product = database.product.get(product.id)
    assert updated_product.components[0].amount == 42.0

    # when removing component 1 once, one instance should remain
    database.product.components.remove_assignment(product.id, assignment3.id)
    assert len(database.product.get(product.id).components) == 1
    assert all(cid == component1.id for cid in database.product.components.get_children(product.id))

    # when removing the component itself, the association should disappear
    database.component.delete(component1.id)
    assert len(database.product.get(product.id).components) == 0

    # Clean up the product
    database.component.delete(component2.id)
    database.product.delete(product.id)


def test_material_lifecycle(database):
    # Should be able to create a new material
    material_new = MaterialNew(name='MyTestMaterial')
    material = database.material.create(material_new)
    assert material

    # Get by ID should return the same material
    material2 = database.material.get(material.id)
    assert material2.id == material.id
    assert material2.name == material.name

    # Should be able to get by name
    material_where = database.material.get_where({'name': 'MyTestMaterial'})
    assert len(material_where) == 1
    material3 = material_where[0]
    assert material3.id == material.id

    # Created material should be in the list of all materials
    all_materials = database.material.get_all()
    assert any(c.id == material.id for c in all_materials)

    # Should be able to update the name
    check_update(database.material, material.id, MaterialUpdate, "name")

    # Delete the material
    database.material.delete(material.id)

    # After deletion, the entity is not found
    with pytest.raises(EntityNotFoundError):
        database.material.get(material.id)


def test_component_material_assignment(database):
    # Create example data
    component = database.component.create(ComponentNew(name='MyTestComponent', weight=1.0))
    mat1 = database.material.create(MaterialNew(name='Test Material 1'))
    mat2 = database.material.create(MaterialNew(name='Test Material 2'))

    # Add material shares
    database.component.materials.add(component.id, mat1.id, 0.7)
    component = database.component.get(component.id)
    assert len(component.materials) == 1
    assert MaterialAssignment(material_id=mat1.id, amount=0.7) in component.materials

    database.component.materials.add(component.id, mat2.id, 0.3)
    component = database.component.get(component.id)
    assert len(component.materials) == 2
    assert MaterialAssignment(material_id=mat1.id, amount=0.7) in component.materials
    assert MaterialAssignment(material_id=mat2.id, amount=0.3) in component.materials

    # Should overwrite on repeated assignment
    database.component.materials.add(component.id, mat2.id, 0.5)
    component = database.component.get(component.id)
    assert len(component.materials) == 2
    assert MaterialAssignment(material_id=mat1.id, amount=0.7) in component.materials
    assert MaterialAssignment(material_id=mat2.id, amount=0.5) in component.materials

    # Remove one of the materials
    database.component.materials.remove(component.id, mat2.id)
    component = database.component.get(component.id)
    assert len(component.materials) == 1
    assert MaterialAssignment(material_id=mat1.id, amount=0.7) in component.materials

    # Clean up
    database.material.delete(mat2.id)
    database.material.delete(mat1.id)
    database.component.delete(component.id)


def test_country_lifecycle(database):
    # Should be able to create a new country
    country_new = CountryNew(name='MyTestCountry', code='MyTestCountryCode')
    country = database.country.create(country_new)
    assert country

    # Get by ID should return the same country
    country2 = database.country.get(country.id)
    assert country2.id == country.id
    assert country2.name == country.name
    assert country2.code == country.code

    # Should be able to get by country code
    country_where = database.country.get_where({'code': 'MyTestCountryCode'})
    assert len(country_where) == 1
    country3 = country_where[0]
    assert country3.id == country.id

    # Should be able to get by name
    country_where2 = database.country.get_where({'name': 'MyTestCountry'})
    assert len(country_where2) == 1
    country4 = country_where2[0]
    assert country4.id == country.id

    # Created country should be in the list of all countries
    all_countries = database.country.get_all()
    assert any(c.id == country.id for c in all_countries)

    # Should be able to update the name
    check_update(database.country, country.id, CountryUpdate, "name")
    # Should be able to update the code
    check_update(database.country, country.id, CountryUpdate, "code")

    # Delete the country
    database.country.delete(country.id)

    # After deletion, the entity is not found
    with pytest.raises(EntityNotFoundError):
        database.country.get(country.id)


def test_country_constraints(database):
    # Creating a second country with the same country code should fail
    c1 = database.country.create(CountryNew(name='Test Country 1', code='T1'))
    with pytest.raises(CreateError):
        c2 = database.country.create(CountryNew(name='Test Country 2', code='T1'))
    database.country.delete(c1.id)


def test_material_country_assignment(database):
    # Create example data
    material = database.material.create(MaterialNew(name='MyTestMaterial'))
    c1 = database.country.create(CountryNew(name='Test Country 1', code='T1'))
    c2 = database.country.create(CountryNew(name='Test Country 2', code='T2'))

    # Add material shares
    database.material.countries.add(material.id, c1.id, 0.7)
    material = database.material.get(material.id)
    assert len(material.countries) == 1
    assert CountryAssignment(country_id=c1.id, amount=0.7) in material.countries

    database.material.countries.add(material.id, c2.id, 0.3)
    material = database.material.get(material.id)
    assert len(material.countries) == 2
    assert CountryAssignment(country_id=c1.id, amount=0.7) in material.countries
    assert CountryAssignment(country_id=c2.id, amount=0.3) in material.countries

    # Should be accessible through the country
    c1 = database.country.get(c1.id)
    assert len(c1.materials) == 1
    assert MaterialAssignment(material_id=material.id, amount=0.7) in c1.materials
    c2 = database.country.get(c2.id)
    assert len(c2.materials) == 1
    assert MaterialAssignment(material_id=material.id, amount=0.3) in c2.materials

    # Should overwrite on repeated assignment
    database.material.countries.add(material.id, c2.id, 0.5)
    material = database.material.get(material.id)
    assert len(material.countries) == 2
    assert CountryAssignment(country_id=c1.id, amount=0.7) in material.countries
    assert CountryAssignment(country_id=c2.id, amount=0.5) in material.countries

    # Remove one of the materials
    database.material.countries.remove(material.id, c2.id)
    material = database.material.get(material.id)
    assert len(material.countries) == 1
    assert CountryAssignment(country_id=c1.id, amount=0.7) in material.countries

    # Clean up
    database.country.delete(c2.id)
    database.country.delete(c1.id)
    database.material.delete(material.id)


def test_indicator_lifecycle(database):
    # Should be able to create a new indicator
    indicator_new = IndicatorNew(name='MyTestIndicator')
    indicator = database.indicator.create(indicator_new)
    assert indicator

    # Get by ID should return the same indicator
    indicator2 = database.indicator.get(indicator.id)
    assert indicator2.id == indicator.id
    assert indicator2.name == indicator.name
    assert indicator2.direction == 'ascending'  # Direction should be set by default

    # Should be able to get by name
    indicator_where = database.indicator.get_where({'name': 'MyTestIndicator'})
    assert len(indicator_where) == 1
    indicator3 = indicator_where[0]
    assert indicator3.id == indicator.id

    # Created indicator should be in the list of all indicators
    all_indicators = database.indicator.get_all()
    assert any(c.id == indicator.id for c in all_indicators)

    # Should be able to update the name
    check_update(database.indicator, indicator.id, IndicatorUpdate, "name")

    # Delete the indicator
    database.indicator.delete(indicator.id)

    # After deletion, the entity is not found
    with pytest.raises(EntityNotFoundError):
        database.indicator.get(indicator.id)


def test_indicator_direction(database):
    # ascending and descending should work as values
    indicator_new1 = IndicatorNew(name='MyTestIndicator1', direction='ascending')
    database.indicator.create(indicator_new1)
    indicator_new2 = IndicatorNew(name='MyTestIndicator2', direction='descending')
    database.indicator.create(indicator_new2)

    # a default value should be assigned if direction is omitted
    indicator_new3 = IndicatorNew(name='MyTestIndicator3')
    indicator3 = database.indicator.create(indicator_new3)
    assert indicator3.direction == 'ascending'

    # other strings should not be permitted
    with pytest.raises(ValueError):
        indicator_new4 = IndicatorNew(name='MyTestIndicator1', direction='invalid')
        database.indicator.create(indicator_new4)


def test_indicator_country_assignment(database):
    # Create example data
    indicator = database.indicator.create(IndicatorNew(name='MyTestIndicator'))
    c1 = database.country.create(CountryNew(name='Test Country 1', code='T1'))
    c2 = database.country.create(CountryNew(name='Test Country 2', code='T2'))

    # Add indicator assignments
    database.indicator.countries.add(indicator.id, c1.id, 0.7)
    indicator = database.indicator.get(indicator.id)
    assert len(indicator.countries) == 1
    assert CountryAssignment(country_id=c1.id, amount=0.7) in indicator.countries

    database.indicator.countries.add(indicator.id, c2.id, 0.3)
    indicator = database.indicator.get(indicator.id)
    assert len(indicator.countries) == 2
    assert CountryAssignment(country_id=c1.id, amount=0.7) in indicator.countries
    assert CountryAssignment(country_id=c2.id, amount=0.3) in indicator.countries

    # Should be accessible through the country
    c1 = database.country.get(c1.id)
    assert len(c1.indicators) == 1
    assert IndicatorAssignment(indicator_id=indicator.id, amount=0.7) in c1.indicators
    c2 = database.country.get(c2.id)
    assert len(c2.indicators) == 1
    assert IndicatorAssignment(indicator_id=indicator.id, amount=0.3) in c2.indicators

    # Should overwrite on repeated assignment
    database.indicator.countries.add(indicator.id, c2.id, 0.5)
    indicator = database.indicator.get(indicator.id)
    assert len(indicator.countries) == 2
    assert CountryAssignment(country_id=c1.id, amount=0.7) in indicator.countries
    assert CountryAssignment(country_id=c2.id, amount=0.5) in indicator.countries

    # Remove one of the materials
    database.indicator.countries.remove(indicator.id, c2.id)
    indicator = database.indicator.get(indicator.id)
    assert len(indicator.countries) == 1
    assert CountryAssignment(country_id=c1.id, amount=0.7) in indicator.countries

    # Clean up
    database.country.delete(c2.id)
    database.country.delete(c1.id)
    database.indicator.delete(indicator.id)


@pytest.mark.parametrize("entity,data,slug", [
    ("product", ProductNew(name='My Test Product Name'), 'my-test-product-name')
])
def test_slug(database, entity, data, slug):
    # Slug should be auto-filled.
    ops = database.get_entity('product')
    instance = ops.create(data)
    assert instance.slug == slug

    # Slug should be suffixed if conflicting
    instance2 = ops.create(data)
    assert instance2.slug == slug + '-2'
    instance3 = ops.create(data)
    assert instance3.slug == slug + '-3'
