import csv
from contextlib import closing

import pytest
from starlette.testclient import TestClient

from pse_backend.api.models import *
from pse_backend.api.endpoints import build_app
from pse_backend.db import get_database


@pytest.fixture(scope="function")
def client(database):
    app = build_app(database)
    client = TestClient(app)
    return client


def check_patch(client: TestClient, entity_type: str, entity_id: int, attribute_name: str):
    """
    Helper function to test attribute-updating PATCH requests.

    :param client: the test client to access
    :param entity_type: name of the entity (in the form of a URL component, e.g. "products")
    :param entity_id: ID of the entity to test
    :param attribute_name: attribute name to set
    """
    # Should be able to update the attribute via PATCH
    entity_url = f"/{entity_type}/{entity_id}"
    update_request = {"data": {attribute_name: "Updated Test Name"}}
    update_response = client.patch(entity_url, json=update_request)
    assert update_response.status_code == 200
    # Request should return the updated entity
    assert update_response.json()["data"][attribute_name] == update_request["data"][attribute_name]
    # New get request should also return the updated entity
    get_response = client.get(entity_url)
    assert get_response.json()["data"][attribute_name] == update_request["data"][attribute_name]


def impl_test_upload_entities(client: TestClient, entity: str, csv_path: str):
    # Assemble data for the entities to import
    with open(csv_path) as f:
        csv_reader = csv.DictReader(f)
        csv_rows = [row for row in csv_reader]
    slugs_to_import = [row["slug"] for row in csv_rows]

    # Upload CSV
    with open(csv_path, mode='rb') as f:
        upload_response = client.put(f"/{entity}", files={"file": f})
    assert upload_response.status_code == 204

    data = client.get(f"/{entity}").json()["data"]
    # All entities should have been imported
    slugs = {x["slug"] for x in data}
    for slug in slugs_to_import:
        assert slug in slugs

    # Upload CSV again
    with open(csv_path, mode='rb') as f:
        upload_response = client.put(f"/{entity}", files={"file": f})
    assert upload_response.status_code == 204


def impl_test_upload_assignment(client: TestClient, parent: str, assignment: str, parent_id: int, assignment_csv: str):
    # Upload CSV
    with open(assignment_csv, mode='rb') as f:
        upload_response = client.put(f"/{parent}/{parent_id}/{assignment}", files={"file": f})
    assert upload_response.status_code == 204

    p_data_1 = client.get(f"/{parent}/{parent_id}").json()["data"]
    # Should have some children
    assert len(p_data_1[assignment]) > 0

    # Upload CSV again
    with open(assignment_csv, mode='rb') as f:
        upload_response = client.put(f"/{parent}/{parent_id}/{assignment}", files={"file": f})
    assert upload_response.status_code == 204

    p_data_2 = client.get(f"/{parent}/{parent_id}").json()["data"]
    # Should still have only the original children (should have replaced them)
    assert len(p_data_1[assignment]) == len(p_data_2[assignment])


def test_product_lifecycle(client):
    # When creating a product with valid data, should return the same product with an ID
    request = {
        "data": {
            "name": "le Prodükt"
        }
    }
    create_response = client.post("/products", json=request)
    assert create_response.status_code == 201
    create_response_body = CreateProductResponse(**create_response.json())
    created_product = create_response_body.data
    assert created_product.name == request["data"]["name"]
    assert created_product.slug == "le-produkt"  # slug should be auto-created

    # When accessing an existing product, should return the requested product
    get_response = client.get(f"/products/{created_product.id}")
    assert get_response.status_code == 200
    get_response_body = GetProductResponse(**get_response.json())
    gotten_product = get_response_body.data
    assert gotten_product.id == created_product.id

    # The created product should be in the list of all products
    get_all_response = client.get("/products")
    assert get_all_response.status_code == 200
    get_all_response_body = GetProductsResponse(**get_all_response.json())
    assert any(c.id == created_product.id for c in get_all_response_body.data)

    # Should be able to update fields via PATCH
    check_patch(client, "products", created_product.id, "name")
    check_patch(client, "products", created_product.id, "description")

    # Should be able to remove the product
    delete_response = client.delete(f"/products/{created_product.id}")
    assert delete_response.status_code == 204

    # When accessing a nonexisting product, should return a 404
    get_nonex_response = client.get(f"/products/{created_product.id}")
    assert get_nonex_response.status_code == 404
    json = get_nonex_response.json()
    assert "detail" in json


def test_create_product_invalid(client):
    # When creating a component with invalid data, should return a 422

    # Not wrapped in data attribute
    request = {
        "name": "xyz"
    }
    response = client.post("/components", json=request)
    assert response.status_code == 422

    # Nonexistent attribute
    request = {
        "data": {
            "name": "xyz",
            "uiaeuia": "vlcvlc"
        }
    }
    response = client.post("/components", json=request)
    assert response.status_code == 422

    # Required attribute missing
    request = {
        "data": {
        }
    }
    response = client.post("/components", json=request)
    assert response.status_code == 422


def test_upload_products(client):
    impl_test_upload_entities(client, 'products', 'tests/data_import/products.csv')


def test_product_components(client):
    # Create a product and components to work with
    product_request = {
        "data": {
            "name": "TestProduct"
        }
    }
    product_response = client.post("/products", json=product_request)
    assert product_response.status_code == 201
    product = CreateProductResponse(**product_response.json()).data
    product_id = product.id
    # New product should start out with empty components
    assert not product.components

    component1_request = {
        "data": {
            "name": "TestComponent1",
            "weight": 1.0
        }
    }
    component1_response = client.post("/components", json=component1_request)
    component1 = CreateComponentResponse(**component1_response.json()).data
    assert component1_response.status_code == 201

    component2_request = {
        "data": {
            "name": "TestComponent2",
            "weight": 1.0
        }
    }
    component2_response = client.post("/components", json=component2_request)
    component2 = CreateComponentResponse(**component2_response.json()).data
    assert component2_response.status_code == 201

    # New product should start out with empty components
    product_response = client.get(f"/products/{product_id}")
    product = GetProductResponse(**product_response.json()).data
    assert not product.components

    # Should accept a single component
    request = {
        "data": {"component_id": component1.id, "amount": 1.0}
    }
    response = client.post(f"/products/{product_id}/components", json=request)
    assert response.status_code == 201

    # Should accept a list of components
    request = {
        "data": [
            {"component_id": component1.id, "amount": 1.0},
            {"component_id": component2.id, "amount": 1.0}]
    }
    response = client.post(f"/products/{product_id}/components", json=request)
    assert response.status_code == 201

    # Product should contain the added components
    product_with_three_components_response = client.get(f"/products/{product_id}")
    product_with_three_components = CreateProductResponse(**product_with_three_components_response.json()).data
    assert [assignment.component_id for assignment in product_with_three_components.components]\
        == [component1.id, component1.id, component2.id]

    # Should remove one of two occurrences
    assignment_id = product_with_three_components.components[0].id
    response = client.delete(f"/products/{product_id}/components/{assignment_id}")
    assert response.status_code == 204

    product_with_two_components_response = client.get(f"/products/{product_id}")
    product_with_two_components = CreateProductResponse(**product_with_two_components_response.json()).data
    assert [assignment.component_id for assignment in product_with_two_components.components]\
        == [component1.id, component2.id]

    # Should remove the remaining occurrences
    response = client.delete(f"/products/{product_id}/components/{product_with_two_components.components[0].id}")
    response = client.delete(f"/products/{product_id}/components/{product_with_two_components.components[1].id}")
    assert response.status_code == 204

    product_without_components_response = client.get(f"/products/{product_id}")
    product_without_components = CreateProductResponse(**product_without_components_response.json()).data
    assert product_without_components.components == []

    # Cleanup
    client.delete(f"/products/{product_id}")
    client.delete(f"/components/{component1.id}")
    client.delete(f"/components/{component2.id}")


def test_upload_product_components(client):
    # Create two components
    c1_data = {"data": {"name": "Test Component 1", "slug": "test-component-1", "weight": 1.0}}
    client.post("/components", json=c1_data)

    c2_data = {"data": {"name": "Test Component 2", "slug": "test-component-2", "weight": 2.0}}
    client.post("/components", json=c2_data)

    # Create a product
    p_data = {"data": {"name": "Test Product"}}
    p_create_response = client.post("/products", json=p_data)
    p = Product(**p_create_response.json()["data"])

    impl_test_upload_assignment(client, 'products', 'components', p.id, 'tests/data_import/product_components.csv')


def test_product_report(client):
    # Test that product reports can be accessed
    with closing(get_database()) as database:
        # Create a test product
        product = database.product.create(ProductNew(name='TestProduct'))

        # Empty report should be returned
        response1 = client.get(f'/products/{product.id}/report')
        report_json = response1.json()['data']
        assert response1.status_code == 200
        assert not report_json['components']
        assert 'weight' in report_json

        # Delete the product to see how reports of non-existing products are handled
        database.product.delete(product.id)

        # Should return a 404
        response_nonexistent = client.get(f'/products/{product.id}/report')
        assert response_nonexistent.status_code == 404


def test_get_components(client):
    # When accessing the /components endpoint, should return component data
    response = client.get("/components")
    assert response.status_code == 200
    json = response.json()
    assert "data" in json


def test_component_lifecycle(client):
    # When creating a component with valid data, should return the same component with an ID
    request = {
        "data": {
            "name": "le Kömpönent",
            "weight": 1.0
        }
    }
    create_response = client.post("/components", json=request)
    assert create_response.status_code == 201
    create_response_body = CreateComponentResponse(**create_response.json())
    created_component = create_response_body.data
    assert created_component.name == request["data"]["name"]
    assert created_component.slug == 'le-komponent'

    # When accessing an existing component, should return the requested component
    get_response = client.get(f"/components/{created_component.id}")
    assert get_response.status_code == 200
    get_response_body = GetComponentResponse(**get_response.json())
    gotten_component = get_response_body.data
    assert gotten_component.id == created_component.id

    # The created component should be in the list of all components
    get_all_response = client.get("/components")
    assert get_all_response.status_code == 200
    get_all_response_body = GetComponentsResponse(**get_all_response.json())
    assert any(c.id == created_component.id for c in get_all_response_body.data)

    # Should be able to update the name via PATCH
    check_patch(client, "components", created_component.id, "name")

    # Should be able to remove the component
    delete_response = client.delete(f"/components/{created_component.id}")
    assert delete_response.status_code == 204

    # When accessing a nonexisting component, should return a 404
    get_nonex_response = client.get(f"/components/{created_component.id}")
    assert get_nonex_response.status_code == 404
    json = get_nonex_response.json()
    assert "detail" in json


def test_create_component_invalid(client):
    # When creating a component with invalid data, should return a 422

    # Not wrapped in data attribute
    request = {
        "name": "xyz",
        "weight": 1.0
    }
    response = client.post("/components", json=request)
    assert response.status_code == 422

    # Nonexistent attribute
    request = {
        "data": {
            "name": "xyz",
            "weight": 1.0,
            "uiaeuia": "vlcvlc"
        }
    }
    response = client.post("/components", json=request)
    assert response.status_code == 422

    # Required attribute missing
    request = {
        "data": {
        }
    }
    response = client.post("/components", json=request)
    assert response.status_code == 422


def test_upload_components(client):
    impl_test_upload_entities(client, 'components', 'tests/data_import/components.csv')


def test_component_material_assignment(client):
    # Create a component and two materials
    c1_data = {"data": {"name": "Test Component 1", "weight": 1.0}}
    c1_create_response = client.post("/components", json=c1_data)
    c1 = Component(**c1_create_response.json()["data"])

    m1_data = {"data": {"name": "Test Material 1"}}
    m1_create_response = client.post("/materials", json=m1_data)
    m1 = Material(**m1_create_response.json()["data"])

    m2_data = {"data": {"name": "Test Material 2"}}
    m2_create_response = client.post("/materials", json=m2_data)
    m2 = Material(**m2_create_response.json()["data"])

    # Should start out empty
    assert len(c1.materials) == 0

    c1_get1 = Component(**client.get(f"/components/{c1.id}").json()["data"])
    assert len(c1_get1.materials) == 0

    # Add a material assignment
    m1_post1_data = {"data": [{"material_id": m1.id, "amount": 0.7}]}
    m1_post1_response = client.post(f"/components/{c1.id}/materials/set", json=m1_post1_data)
    assert m1_post1_response.status_code == 204

    c1_get2 = Component(**client.get(f"/components/{c1.id}").json()["data"])
    assert len(c1_get2.materials) == 1
    assert MaterialAssignment(material_id=m1.id, amount=0.7) in c1_get2.materials

    # Add another material assignment
    m2_post1_data = {"data": [{"material_id": m2.id, "amount": 0.3}]}
    m2_post1_response = client.post(f"/components/{c1.id}/materials/set", json=m2_post1_data)
    assert m2_post1_response.status_code == 204

    c1_get3 = Component(**client.get(f"/components/{c1.id}").json()["data"])
    assert len(c1_get3.materials) == 2
    assert MaterialAssignment(material_id=m1.id, amount=0.7) in c1_get3.materials
    assert MaterialAssignment(material_id=m2.id, amount=0.3) in c1_get3.materials

    # Overwrite the second material assignment
    m2_post2_data = {"data": [{"material_id": m2.id, "amount": 0.5}]}
    m2_post2_response = client.post(f"/components/{c1.id}/materials/set", json=m2_post2_data)
    assert m2_post2_response.status_code == 204

    c1_get3 = Component(**client.get(f"/components/{c1.id}").json()["data"])
    assert len(c1_get3.materials) == 2
    assert MaterialAssignment(material_id=m1.id, amount=0.7) in c1_get3.materials
    assert MaterialAssignment(material_id=m2.id, amount=0.5) in c1_get3.materials

    # Remove the first material assignment
    m1_remove_data = {"data": m1.id}
    m1_remove_response = client.post(f"/components/{c1.id}/materials/remove", json=m1_remove_data)
    assert m1_remove_response.status_code == 204

    c1_get4 = Component(**client.get(f"/components/{c1.id}").json()["data"])
    assert len(c1_get4.materials) == 1
    assert MaterialAssignment(material_id=m2.id, amount=0.5) in c1_get4.materials

    # Bulk update both assignments
    m1m2_post_data = {"data": [{"material_id": m1.id, "amount": 0.1},
                               {"material_id": m2.id, "amount": 0.2}]}
    m1m2_post_response = client.post(f"/components/{c1.id}/materials/set", json=m1m2_post_data)
    assert m1m2_post_response.status_code == 204

    c1_get5 = Component(**client.get(f"/components/{c1.id}").json()["data"])
    assert len(c1_get5.materials) == 2
    assert MaterialAssignment(material_id=m1.id, amount=0.1) in c1_get5.materials
    assert MaterialAssignment(material_id=m2.id, amount=0.2) in c1_get5.materials

    # Bulk remove both assignments
    m1m2_remove_data = {"data": [m1.id, m2.id]}
    m1m2_remove_response = client.post(f"/components/{c1.id}/materials/remove", json=m1m2_remove_data)
    assert m1m2_remove_response.status_code == 204

    c1_get6 = Component(**client.get(f"/components/{c1.id}").json()["data"])
    assert len(c1_get6.materials) == 0

    # Clean up
    client.delete(f"/components/{c1.id}")
    client.delete(f"/materials/{m1.id}")
    client.delete(f"/materials/{m2.id}")


def test_upload_component_materials(client):
    # Create two materials
    m1_data = {"data": {"name": "Test Material 1", "slug": "test-material-1"}}
    client.post("/materials", json=m1_data)

    m2_data = {"data": {"name": "Test Material 2", "slug": "test-material-2"}}
    client.post("/materials", json=m2_data)

    # Create a component
    c_data = {"data": {"name": "Test Component", "weight": 1.0}}
    c_create_response = client.post("/components", json=c_data)
    c = Component(**c_create_response.json()["data"])

    impl_test_upload_assignment(client, 'components', 'materials', c.id, 'tests/data_import/component_materials.csv')


def test_get_materials(client):
    # When accessing the /materials endpoint, should return material data
    response = client.get("/materials")
    assert response.status_code == 200
    json = response.json()
    assert "data" in json


def test_material_lifecycle(client):
    # When creating a material with valid data, should return the same material with an ID
    request = {
        "data": {
            "name": "ich heiße material"
        }
    }
    create_response = client.post("/materials", json=request)
    assert create_response.status_code == 201
    create_response_body = CreateMaterialResponse(**create_response.json())
    created_material = create_response_body.data
    assert created_material.name == request["data"]["name"]
    assert created_material.slug == 'ich-heisse-material'

    # When accessing an existing material, should return the requested material
    get_response = client.get(f"/materials/{created_material.id}")
    assert get_response.status_code == 200
    get_response_body = GetMaterialResponse(**get_response.json())
    gotten_material = get_response_body.data
    assert gotten_material.id == created_material.id

    # The created material should be in the list of all materials
    get_all_response = client.get("/materials")
    assert get_all_response.status_code == 200
    get_all_response_body = GetMaterialsResponse(**get_all_response.json())
    assert any(c.id == created_material.id for c in get_all_response_body.data)

    # Should be able to update the name via PATCH
    check_patch(client, "materials", created_material.id, "name")

    # Should be able to remove the material
    delete_response = client.delete(f"/materials/{created_material.id}")
    assert delete_response.status_code == 204

    # When accessing a nonexisting material, should return a 404
    get_nonex_response = client.get(f"/materials/{created_material.id}")
    assert get_nonex_response.status_code == 404
    json = get_nonex_response.json()
    assert "detail" in json


def test_create_material_invalid(client):
    # When creating a material with invalid data, should return a 422

    # Not wrapped in data attribute
    request = {
        "name": "xyz"
    }
    response = client.post("/materials", json=request)
    assert response.status_code == 422

    # Nonexistent attribute
    request = {
        "data": {
            "name": "xyz",
            "uiaeuia": "vlcvlc"
        }
    }
    response = client.post("/materials", json=request)
    assert response.status_code == 422

    # Required attribute missing
    request = {
        "data": {
        }
    }
    response = client.post("/materials", json=request)
    assert response.status_code == 422


def test_upload_materials(client):
    impl_test_upload_entities(client, 'materials', 'tests/data_import/materials.csv')


def test_material_country_assignment(client):
    # Create a material and two countries
    mat1_data = {"data": {"name": "Test Material 1"}}
    mat1_create_response = client.post("/materials", json=mat1_data)
    mat1 = Material(**mat1_create_response.json()["data"])

    country1_data = {"data": {"name": "Test Country 1", "code": "T1"}}
    country1_create_response = client.post("/countries", json=country1_data)
    country1 = Country(**country1_create_response.json()["data"])

    country2_data = {"data": {"name": "Test Country 2", "code": "T2"}}
    country2_create_response = client.post("/countries", json=country2_data)
    country2 = Country(**country2_create_response.json()["data"])

    # Should start out empty
    assert len(mat1.countries) == 0

    mat1_get1 = Material(**client.get(f"/materials/{mat1.id}").json()["data"])
    assert len(mat1_get1.countries) == 0

    # Add a country assignment
    c1_post1_data = {"data": [{"country_id": country1.id, "amount": 0.7}]}
    c1_post1_response = client.post(f"/materials/{mat1.id}/countries/set", json=c1_post1_data)
    assert c1_post1_response.status_code == 204

    mat1_get2 = Material(**client.get(f"/materials/{mat1.id}").json()["data"])
    assert len(mat1_get2.countries) == 1
    assert CountryAssignment(country_id=country1.id, amount=0.7) in mat1_get2.countries

    # Add another country assignment
    c2_post1_data = {"data": [{"country_id": country2.id, "amount": 0.3}]}
    c2_post1_response = client.post(f"/materials/{mat1.id}/countries/set", json=c2_post1_data)
    assert c2_post1_response.status_code == 204

    mat1_get3 = Material(**client.get(f"/materials/{mat1.id}").json()["data"])
    assert len(mat1_get3.countries) == 2
    assert CountryAssignment(country_id=country1.id, amount=0.7) in mat1_get3.countries
    assert CountryAssignment(country_id=country2.id, amount=0.3) in mat1_get3.countries

    # The assignments should be accessible via the country as well
    c1_get1 = Country(**client.get(f"/countries/{country1.id}").json()["data"])
    assert len(c1_get1.materials) == 1
    assert MaterialAssignment(material_id=mat1.id, amount=0.7) in c1_get1.materials
    c2_get1 = Country(**client.get(f"/countries/{country2.id}").json()["data"])
    assert len(c2_get1.materials) == 1
    assert MaterialAssignment(material_id=mat1.id, amount=0.3) in c2_get1.materials

    # Overwrite the second country assignment
    c2_post2_data = {"data": [{"country_id": country2.id, "amount": 0.5}]}
    c2_post2_response = client.post(f"/materials/{mat1.id}/countries/set", json=c2_post2_data)
    assert c2_post2_response.status_code == 204

    mat1_get4 = Material(**client.get(f"/materials/{mat1.id}").json()["data"])
    assert len(mat1_get4.countries) == 2
    assert CountryAssignment(country_id=country1.id, amount=0.7) in mat1_get4.countries
    assert CountryAssignment(country_id=country2.id, amount=0.5) in mat1_get4.countries

    # Remove the first country assignment
    c1_remove_data = {"data": country1.id}
    c1_remove_response = client.post(f"/materials/{mat1.id}/countries/remove", json=c1_remove_data)
    assert c1_remove_response.status_code == 204

    mat1_get5 = Material(**client.get(f"/materials/{mat1.id}").json()["data"])
    assert len(mat1_get5.countries) == 1
    assert CountryAssignment(country_id=country2.id, amount=0.5) in mat1_get5.countries

    # Bulk update both assignments
    c1c2_post_data = {"data": [{"country_id": country1.id, "amount": 0.1},
                               {"country_id": country2.id, "amount": 0.2}]}
    c1c2_post_response = client.post(f"/materials/{mat1.id}/countries/set", json=c1c2_post_data)
    assert c1c2_post_response.status_code == 204

    mat1_get6 = Material(**client.get(f"/materials/{mat1.id}").json()["data"])
    assert len(mat1_get6.countries) == 2
    assert CountryAssignment(country_id=country1.id, amount=0.1) in mat1_get6.countries
    assert CountryAssignment(country_id=country2.id, amount=0.2) in mat1_get6.countries

    # Bulk remove both assignments
    c1c2_remove_data = {"data": [country1.id, country2.id]}
    c1c2_remove_response = client.post(f"/materials/{mat1.id}/countries/remove", json=c1c2_remove_data)
    assert c1c2_remove_response.status_code == 204

    mat1_get5 = Material(**client.get(f"/materials/{mat1.id}").json()["data"])
    assert len(mat1_get5.countries) == 0

    # Clean up
    client.delete(f"/materials/{mat1.id}")
    client.delete(f"/countries/{country1.id}")
    client.delete(f"/countries/{country2.id}")


def test_upload_material_countries(client):
    # Create two countries
    c1_data = {"data": {"name": "Test Country 1", "code": "TC1", "slug": "test-country-1"}}
    client.post("/countries", json=c1_data)

    c2_data = {"data": {"name": "Test Country 2", "code": "TC2", "slug": "test-country-2"}}
    client.post("/countries", json=c2_data)

    # Create a material
    m_data = {"data": {"name": "Test Material"}}
    m_create_response = client.post("/materials", json=m_data)
    m = Material(**m_create_response.json()["data"])

    impl_test_upload_assignment(client, 'materials', 'countries', m.id, 'tests/data_import/material_countries.csv')


def test_get_countries(client):
    # When accessing the /countries endpoint, should return country data
    response = client.get("/countries")
    assert response.status_code == 200
    json = response.json()
    assert "data" in json


def test_country_lifecycle(client):
    # When creating a country with valid data, should return the same country with an ID
    request = {
        "data": {
            "name": "Tést Cõüntrỳ",
            "code": "abc"
        }
    }
    create_response = client.post("/countries", json=request)
    assert create_response.status_code == 201
    create_response_body = CreateCountryResponse(**create_response.json())
    created_country = create_response_body.data
    assert created_country.name == request["data"]["name"]
    assert created_country.code == request["data"]["code"]
    assert created_country.slug == 'test-country'

    # When accessing an existing country, should return the requested country
    get_response = client.get(f"/countries/{created_country.id}")
    assert get_response.status_code == 200
    get_response_body = GetCountryResponse(**get_response.json())
    gotten_country = get_response_body.data
    assert gotten_country.id == created_country.id

    # The created country should be in the list of all countries
    get_all_response = client.get("/countries")
    assert get_all_response.status_code == 200
    get_all_response_body = GetCountriesResponse(**get_all_response.json())
    assert any(c.id == created_country.id for c in get_all_response_body.data)

    # Should be able to update the name via PATCH
    check_patch(client, "countries", created_country.id, "name")
    check_patch(client, "countries", created_country.id, "code")

    # Should be able to remove the country
    delete_response = client.delete(f"/countries/{created_country.id}")
    assert delete_response.status_code == 204

    # When accessing a nonexisting country, should return a 404
    get_nonex_response = client.get(f"/countries/{created_country.id}")
    assert get_nonex_response.status_code == 404
    json = get_nonex_response.json()
    assert "detail" in json


def test_create_country_invalid(client):
    # When creating a country with invalid data, should return a 422

    # Not wrapped in data attribute
    request = {
        "name": "xyz",
        "code": "abc"
    }
    response = client.post("/countries", json=request)
    assert response.status_code == 422

    # Nonexistent attribute
    request = {
        "data": {
            "name": "xyz",
            "code": "abcs",
            "uiaeuia": "vlcvlc"
        }
    }
    response = client.post("/countries", json=request)
    assert response.status_code == 422

    # Required attribute missing
    request = {
        "data": {
        }
    }
    response = client.post("/countries", json=request)
    assert response.status_code == 422


def test_upload_countries(client):
    impl_test_upload_entities(client, 'countries', 'tests/data_import/countries.csv')


def test_get_indicators(client):
    # When accessing the /indicators endpoint, should return indicator data
    response = client.get("/indicators")
    assert response.status_code == 200
    json = response.json()
    assert "data" in json


def test_indicator_lifecycle(client):
    # When creating a indicator with valid data, should return the same indicator with an ID
    request = {
        "data": {
            "name": "Ein Testindikator"
        }
    }
    create_response = client.post("/indicators", json=request)
    assert create_response.status_code == 201
    create_response_body = CreateIndicatorResponse(**create_response.json())
    created_indicator = create_response_body.data
    assert created_indicator.name == request["data"]["name"]
    assert created_indicator.slug == 'ein-testindikator'

    # When accessing an existing indicator, should return the requested indicator
    get_response = client.get(f"/indicators/{created_indicator.id}")
    assert get_response.status_code == 200
    get_response_body = GetIndicatorResponse(**get_response.json())
    gotten_indicator = get_response_body.data
    assert gotten_indicator.id == created_indicator.id

    # The created indicator should be in the list of all indicators
    get_all_response = client.get("/indicators")
    assert get_all_response.status_code == 200
    get_all_response_body = GetIndicatorsResponse(**get_all_response.json())
    assert any(c.id == created_indicator.id for c in get_all_response_body.data)

    # Should be able to update the name via PATCH
    check_patch(client, "indicators", created_indicator.id, "name")

    # Should be able to remove the indicator
    delete_response = client.delete(f"/indicators/{created_indicator.id}")
    assert delete_response.status_code == 204

    # When accessing a nonexisting indicator, should return a 404
    get_nonex_response = client.get(f"/indicators/{created_indicator.id}")
    assert get_nonex_response.status_code == 404
    json = get_nonex_response.json()
    assert "detail" in json


def test_create_indicator_invalid(client):
    # When creating a indicator with invalid data, should return a 422

    # Not wrapped in data attribute
    request = {
        "name": "xyz"
    }
    response = client.post("/indicators", json=request)
    assert response.status_code == 422

    # Nonexistent attribute
    request = {
        "data": {
            "name": "xyz",
            "uiaeuia": "vlcvlc"
        }
    }
    response = client.post("/indicators", json=request)
    assert response.status_code == 422

    # Required attribute missing
    request = {
        "data": {
        }
    }
    response = client.post("/indicators", json=request)
    assert response.status_code == 422


def test_upload_indicators(client):
    impl_test_upload_entities(client, 'indicators', 'tests/data_import/indicators.csv')


def test_indicator_country_assignment(client):
    # Create an indicator and two countries
    ind1_data = {"data": {"name": "Test Material 1"}}
    ind1_create_response = client.post("/indicators", json=ind1_data)
    ind1 = Indicator(**ind1_create_response.json()["data"])

    country1_data = {"data": {"name": "Test Country 1", "code": "T1"}}
    country1_create_response = client.post("/countries", json=country1_data)
    country1 = Country(**country1_create_response.json()["data"])

    country2_data = {"data": {"name": "Test Country 2", "code": "T2"}}
    country2_create_response = client.post("/countries", json=country2_data)
    country2 = Country(**country2_create_response.json()["data"])

    # Should start out empty
    assert len(ind1.countries) == 0

    ind1_get1 = Indicator(**client.get(f"/indicators/{ind1.id}").json()["data"])
    assert len(ind1_get1.countries) == 0

    # Add a country assignment
    c1_post1_data = {"data": [{"country_id": country1.id, "amount": 0.7}]}
    c1_post1_response = client.post(f"/indicators/{ind1.id}/countries/set", json=c1_post1_data)
    assert c1_post1_response.status_code == 204

    ind1_get2 = Indicator(**client.get(f"/indicators/{ind1.id}").json()["data"])
    assert len(ind1_get2.countries) == 1
    assert CountryAssignment(country_id=country1.id, amount=0.7) in ind1_get2.countries

    # Add another country assignment
    c2_post1_data = {"data": [{"country_id": country2.id, "amount": 0.3}]}
    c2_post1_response = client.post(f"/indicators/{ind1.id}/countries/set", json=c2_post1_data)
    assert c2_post1_response.status_code == 204

    ind1_get3 = Indicator(**client.get(f"/indicators/{ind1.id}").json()["data"])
    assert len(ind1_get3.countries) == 2
    assert CountryAssignment(country_id=country1.id, amount=0.7) in ind1_get3.countries
    assert CountryAssignment(country_id=country2.id, amount=0.3) in ind1_get3.countries

    # The assignments should be accessible via the country as well
    c1_get1 = Country(**client.get(f"/countries/{country1.id}").json()["data"])
    assert len(c1_get1.indicators) == 1
    assert IndicatorAssignment(indicator_id=ind1.id, amount=0.7) in c1_get1.indicators
    c2_get1 = Country(**client.get(f"/countries/{country2.id}").json()["data"])
    assert len(c2_get1.indicators) == 1
    assert IndicatorAssignment(indicator_id=ind1.id, amount=0.3) in c2_get1.indicators

    # Overwrite the second country assignment
    c2_post2_data = {"data": [{"country_id": country2.id, "amount": 0.5}]}
    c2_post2_response = client.post(f"/indicators/{ind1.id}/countries/set", json=c2_post2_data)
    assert c2_post2_response.status_code == 204

    ind1_get4 = Indicator(**client.get(f"/indicators/{ind1.id}").json()["data"])
    assert len(ind1_get4.countries) == 2
    assert CountryAssignment(country_id=country1.id, amount=0.7) in ind1_get4.countries
    assert CountryAssignment(country_id=country2.id, amount=0.5) in ind1_get4.countries

    # The assignments should be accessible via the country as well
    c2_get2 = Country(**client.get(f"/countries/{country2.id}").json()["data"])
    assert len(c2_get2.indicators) == 1
    assert IndicatorAssignment(indicator_id=ind1.id, amount=0.5) in c2_get2.indicators

    # Remove the first country assignment
    c1_remove_data = {"data": ind1.id}
    c1_remove_response = client.post(f"/countries/{country1.id}/indicators/remove", json=c1_remove_data)
    assert c1_remove_response.status_code == 204

    ind1_get5 = Indicator(**client.get(f"/indicators/{ind1.id}").json()["data"])
    assert len(ind1_get5.countries) == 1
    assert CountryAssignment(country_id=country2.id, amount=0.5) in ind1_get5.countries

    # Bulk update both assignments
    c1c2_post_data = {"data": [{"country_id": country1.id, "amount": 0.1},
                               {"country_id": country2.id, "amount": 0.2}]}
    c1c2_post_response = client.post(f"/indicators/{ind1.id}/countries/set", json=c1c2_post_data)
    assert c1c2_post_response.status_code == 204

    ind1_get6 = Indicator(**client.get(f"/indicators/{ind1.id}").json()["data"])
    assert len(ind1_get6.countries) == 2
    assert CountryAssignment(country_id=country1.id, amount=0.1) in ind1_get6.countries
    assert CountryAssignment(country_id=country2.id, amount=0.2) in ind1_get6.countries

    # Bulk remove both assignments
    c1c2_remove_data = {"data": [country1.id, country2.id]}
    c1c2_remove_response = client.post(f"/indicators/{ind1.id}/countries/remove", json=c1c2_remove_data)
    assert c1c2_remove_response.status_code == 204

    c1_get6 = Indicator(**client.get(f"/indicators/{ind1.id}").json()["data"])
    assert len(c1_get6.countries) == 0

    # Clean up
    client.delete(f"/indicators/{ind1.id}")
    client.delete(f"/countries/{country1.id}")
    client.delete(f"/countries/{country2.id}")


def test_upload_indicator_countries(client):
    # Create two countries
    c1_data = {"data": {"name": "Test Country 1", "code": "TC1", "slug": "test-country-1"}}
    client.post("/countries", json=c1_data)

    c2_data = {"data": {"name": "Test Country 2", "code": "TC2", "slug": "test-country-2"}}
    client.post("/countries", json=c2_data)

    # Create an indicator
    i_data = {"data": {"name": "Test Indicator"}}
    i_create_response = client.post("/indicators", json=i_data)
    i = Indicator(**i_create_response.json()["data"])

    impl_test_upload_assignment(client, 'indicators', 'countries', i.id, 'tests/data_import/indicator_countries.csv')
