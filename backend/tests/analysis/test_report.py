import numpy as np
import pytest

from pse_backend.analysis.report import get_report, country_risk_matrix, material_country_matrix, \
    component_material_matrix, transfer_risk, \
    mk_entity_reports, component_activity_vector, transfer_activity, compute_report, load_entities, product_weight
from pse_backend.analysis.risk_levels import IndicatorThresholds, ThresholdEntry
from pse_backend.models import CountryNew, ProductNew, ComponentNew, MaterialNew, IndicatorNew, Indicator, Country, \
    IndicatorAssignment, Material, CountryAssignment, Component, MaterialAssignment, Product, ComponentAssignment

values = [0, 25, 40, 50, 60, 75, 80, 100]

indicators = [
    Indicator(id=1, name='Indicator 1', direction='ascending', countries=[
        CountryAssignment(country_id=1, amount=0.2),
        CountryAssignment(country_id=2, amount=0.6)
    ]),
    Indicator(id=2, name='Indicator 2', direction='ascending', countries=[
        CountryAssignment(country_id=1, amount=0.8),
        CountryAssignment(country_id=2, amount=0.4)
    ])
]
countries = [
    Country(id=1, name='Country 1', code='C1', indicators=[
        IndicatorAssignment(indicator_id=1, amount=0.2),
        IndicatorAssignment(indicator_id=2, amount=0.8)
    ]),
    Country(id=2, name='Country 2', code='C2', indicators=[
        IndicatorAssignment(indicator_id=1, amount=0.6),
        IndicatorAssignment(indicator_id=2, amount=0.4)
    ])
]
materials = [
    Material(id=1, name='Material 1', countries=[
        CountryAssignment(country_id=1, amount=0.2),
        CountryAssignment(country_id=2, amount=0.8)
    ]),
    Material(id=2, name='Material 2', countries=[
        CountryAssignment(country_id=1, amount=0.6),
        CountryAssignment(country_id=2, amount=0.4)
    ])
]
components = [
    Component(id=1, name='Component 1', weight=2.0, materials=[
        MaterialAssignment(material_id=1, amount=0.2),
        MaterialAssignment(material_id=2, amount=0.8)
    ]),
    Component(id=2, name='Component 2', weight=1.5, materials=[
        MaterialAssignment(material_id=1, amount=0.6),
        MaterialAssignment(material_id=2, amount=0.4)
    ])
]
product = Product(id=1, name='Product', components=[
    ComponentAssignment(id=1, component_id=1, amount=1.0),
    ComponentAssignment(id=2, component_id=2, amount=2.0)
])


def create_report_data(database):
    countries = []
    # Add some data
    p1 = database.product.create(ProductNew(name='Product1'))
    m1 = database.material.create(MaterialNew(name='Material1'))
    c1 = database.component.create(ComponentNew(name='Component1', weight=1.0))
    database.product.components.append(p1.id, c1.id, 1.0)
    database.component.materials.add(c1.id, m1.id, 100)

    i1 = database.indicator.create(IndicatorNew(name='Indicator1'))
    i2 = database.indicator.create(IndicatorNew(name='Indicator2'))
    for i in range(0, len(values)):
        c = database.country.create(CountryNew(name='Country' + str(i), code='C' + str(i)))
        countries.append(c)
        database.country.indicators.add(c.id, i1.id, values[i])
        database.material.countries.add(m1.id, c.id, 0.5)

    return {
        'countries': countries,
        'indicators': [i1, i2],
        'product': p1,
        'material': m1,
        'component': c1,
    }


def test_get_report(database):
    data = create_report_data(database)
    report = get_report(data['product'].id, database)
    assert len(report.components) == 1
    assert len(report.materials) == 1
    assert len(report.countries) == len(data['countries'])


def test_load_entities(database):
    test_entities = create_report_data(database)
    entities = load_entities(test_entities['product'].id, database)
    assert len(entities.components) == 1
    assert {component.id for component in entities.components} == {test_entities['component'].id}
    assert len(entities.materials) == 1
    assert {material.id for material in entities.materials} == {test_entities['material'].id}
    assert len(entities.countries) == len(test_entities['countries'])
    assert {country.id for country in entities.countries} == {country.id for country in test_entities['countries']}
    # cannot assert indicators, as all indicators are used


def test_compute_report():
    report = compute_report(product, components, materials, countries, indicators)
    # For now, we just check that the data structure is there… in the future, we might add actual test analyses
    assert report.components[0].risks[0].name == indicators[0].name
    assert report.components[0].risks[0].level == 'MEDIUM'
    assert report.components[0].risks[1].name == indicators[1].name
    assert report.components[0].risks[1].level == 'MEDIUM'
    assert report.components[0].activity == 0.4
    assert report.components[0].riskLevel == 'MEDIUM'
    assert report.components[1].risks[0].name == indicators[0].name
    assert report.components[1].risks[0].level == 'MEDIUM'
    assert report.components[1].risks[1].name == indicators[1].name
    assert report.components[1].risks[1].level == 'MEDIUM'
    assert report.components[1].activity == 0.6
    assert report.components[1].riskLevel == 'MEDIUM'
    assert report.materials[0].risks[0].name == indicators[0].name
    assert report.materials[0].risks[1].name == indicators[1].name
    assert report.materials[1].risks[0].name == indicators[0].name
    assert report.materials[1].risks[1].name == indicators[1].name
    assert report.countries[0].risks[0].name == indicators[0].name
    assert report.countries[0].risks[1].name == indicators[1].name
    assert report.countries[1].risks[0].name == indicators[0].name
    assert report.countries[1].risks[1].name == indicators[1].name


def test_country_risk_matrix():
    matrix = country_risk_matrix(countries, indicators)
    assert (matrix == np.array([[0.2, 0.8], [0.6, 0.4]])).all()


def test_material_country_matrix():
    matrix = material_country_matrix(materials, countries)
    assert (matrix == np.array([[0.2, 0.8], [0.6, 0.4]])).all()


def test_component_material_matrix():
    matrix = component_material_matrix(components, materials)
    assert (matrix == np.array([[0.2, 0.8], [0.6, 0.4]])).all()


def test_mk_entity_reports():
    component_risks = np.ma.array([[0.5, 0.2], [0.3, 0.6]])
    thresholds = IndicatorThresholds([
        ThresholdEntry(indicators[0], 0.4, 0.6),
        ThresholdEntry(indicators[1], 0.3, 0.5)
    ])
    activities = [0.2, 0.5]
    component_reports = mk_entity_reports(components[:2], indicators[:2], thresholds, activities, component_risks)
    assert len(component_reports) == 2
    assert component_reports[0].risks[0].name == indicators[0].name
    assert component_reports[0].risks[0].level == 'MEDIUM'
    assert component_reports[0].risks[1].name == indicators[1].name
    assert component_reports[0].risks[1].level == 'LOW'
    assert component_reports[0].activity == 0.2
    assert component_reports[0].riskLevel == 'MEDIUM'
    assert component_reports[1].risks[0].name == indicators[0].name
    assert component_reports[1].risks[0].level == 'LOW'
    assert component_reports[1].risks[1].name == indicators[1].name
    assert component_reports[1].risks[1].level == 'HIGH'
    assert component_reports[1].activity == 0.5
    assert component_reports[1].riskLevel == 'HIGH'


def test_transfer_risk():
    component_materials = component_material_matrix(components, materials)
    material_countries = material_country_matrix(materials, countries)
    country_risks = country_risk_matrix(countries, indicators)
    material_risks = transfer_risk(material_countries, country_risks)
    assert material_risks[0, 0] == pytest.approx(0.2 * 0.2 + 0.8 * 0.6)
    assert material_risks[0, 1] == pytest.approx(0.2 * 0.8 + 0.8 * 0.4)
    assert material_risks[1, 0] == pytest.approx(0.6 * 0.2 + 0.4 * 0.6)
    assert material_risks[1, 1] == pytest.approx(0.6 * 0.8 + 0.4 * 0.4)
    component_risks = transfer_risk(component_materials, material_risks)
    assert component_risks[0, 0] == pytest.approx(0.2 * 0.2 * 0.2 + 0.2 * 0.8 * 0.6 +  # material 1
                                                  0.8 * 0.6 * 0.2 + 0.8 * 0.4 * 0.6)  # material 2
    assert component_risks[0, 1] == pytest.approx(0.2 * 0.2 * 0.8 + 0.2 * 0.8 * 0.4 +  # material 1
                                                  0.8 * 0.6 * 0.8 + 0.8 * 0.4 * 0.4)  # material 2


def test_transfer_activity():
    component_activities = component_activity_vector(product, components)
    component_materials = component_material_matrix(components, materials)
    material_countries = material_country_matrix(materials, countries)
    material_activities = transfer_activity(component_activities, component_materials)
    assert material_activities[0] == pytest.approx(0.4 * 0.2 + 0.6 * 0.6)
    assert material_activities[1] == pytest.approx(0.4 * 0.8 + 0.6 * 0.4)
    country_activities = transfer_activity(material_activities, material_countries)
    assert country_activities[0] == pytest.approx(material_activities[0] * 0.2 + material_activities[1] * 0.6)
    assert country_activities[1] == pytest.approx(material_activities[0] * 0.8 + material_activities[1] * 0.4)


def test_component_activity_vector():
    vector = component_activity_vector(product, components)
    assert vector[0] == pytest.approx(0.4)
    assert vector[1] == pytest.approx(0.6)


def test_product_weight():
    weight = product_weight(product, components)
    assert weight == pytest.approx(5)
