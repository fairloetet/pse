import numpy as np
import pytest

from pse_backend.analysis.risk_levels import IndicatorThresholds, ThresholdEntry, risk_level_to_numeric, max_risk_level
from pse_backend.models import Indicator, CountryAssignment


def test_risk_level_to_numeric():
    assert risk_level_to_numeric('HIGH') > risk_level_to_numeric('MEDIUM')
    assert risk_level_to_numeric('MEDIUM') > risk_level_to_numeric('LOW')


def test_max_risk_level():
    assert max_risk_level(['HIGH', 'LOW', 'MEDIUM']) == 'HIGH'
    assert max_risk_level(['UNKNOWN', 'MEDIUM', 'LOW']) == 'MEDIUM'
    assert max_risk_level([]) == 'UNKNOWN'
    assert max_risk_level(['LOW', 'UNKNOWN', 'LOW']) == 'LOW'
    assert max_risk_level(['UNKNOWN']) == 'UNKNOWN'


def test_risk_level_assignment():
    thresholds = IndicatorThresholds([
        ThresholdEntry(Indicator(id=1, name='Indicator 1', direction='ascending'), 0.1, 0.2),
        ThresholdEntry(Indicator(id=2, name='Indicator 2', direction='descending'), 0.65, 0.85),
        ThresholdEntry(Indicator(id=3, name='Indicator 3', direction='ascending'), np.nan, np.nan)
    ])
    assert thresholds.risk_level(1, 0.0) == 'LOW'
    assert thresholds.risk_level(1, 0.1) == 'LOW'
    assert thresholds.risk_level(1, 0.11) == 'MEDIUM'
    assert thresholds.risk_level(1, 0.19) == 'MEDIUM'
    assert thresholds.risk_level(1, 0.2) == 'HIGH'
    assert thresholds.risk_level(1, 0.6) == 'HIGH'
    assert thresholds.risk_level(2, 0.9) == 'LOW'
    assert thresholds.risk_level(2, 0.85) == 'LOW'
    assert thresholds.risk_level(2, 0.8) == 'MEDIUM'
    assert thresholds.risk_level(2, 0.66) == 'MEDIUM'
    assert thresholds.risk_level(2, 0.65) == 'HIGH'
    assert thresholds.risk_level(2, 0.3) == 'HIGH'
    assert thresholds.risk_level(3, 0.55) == 'UNKNOWN'
    assert thresholds.risk_level(999, 0.55) == 'UNKNOWN'


def test_threshold_computation():
    indicators = [
        Indicator(id=1, name='Indicator 1', direction='ascending', countries=[
            CountryAssignment(country_id=1, amount=0.1),
            CountryAssignment(country_id=2, amount=0.5),
            CountryAssignment(country_id=3, amount=0.9),
            CountryAssignment(country_id=4, amount=1.1),
            CountryAssignment(country_id=5, amount=1.5),
            CountryAssignment(country_id=6, amount=1.7)
        ])
    ]
    thresholds = IndicatorThresholds.compute(indicators)
    assert thresholds.entries[1].lower == pytest.approx(0.5)
    assert thresholds.entries[1].upper == pytest.approx(1.5)
