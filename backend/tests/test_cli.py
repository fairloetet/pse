import os
import subprocess
import sys
from time import sleep

from pse_backend.models import CountryNew, IndicatorNew, IndicatorAssignment


def test_run():
    process = subprocess.Popen('python3 -m pse_backend run --port 8001'.split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    sleep(2)
    process.terminate()
    out, err = process.communicate()
    print('STDOUT\n======', file=sys.stderr)
    print(out, file=sys.stderr)
    print('STDERR\n======', file=sys.stderr)
    print(err, file=sys.stderr)
    assert process.returncode == 0
    assert b"Uvicorn running" in err


def test_import_assignment(test_database):
    database = test_database

    # Add some data
    c1 = database.country.create(CountryNew(name='Country1', code='C1'))
    c2 = database.country.create(CountryNew(name='Country2', code='C2'))
    i1 = database.indicator.create(IndicatorNew(name='Indicator1'))
    i2 = database.indicator.create(IndicatorNew(name='Indicator2'))

    # Run the import command
    cmd = 'python3 -m pse_backend import-assignment --parent country --attribute indicators ' \
          'tests/data_import/country_indicators.csv'
    pse_options = 'database.dbname=pse_test_database'
    pse_options = os.environ['PSE_OPTIONS'] + ';' + pse_options if 'PSE_OPTIONS' in os.environ else pse_options
    env = {**os.environ, 'PSE_OPTIONS': pse_options}
    process = subprocess.Popen(cmd.split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env)
    out, err = process.communicate()
    try:
        assert process.returncode == 0
    except AssertionError as e:
        print(f'\nSTDOUT: {out.decode("utf8")}\nSTDERR: {err.decode("utf8")}')
        raise e

    # Check that assignments were created
    c1 = database.country.get(c1.id)
    c2 = database.country.get(c2.id)
    i1 = database.indicator.get(i1.id)
    i2 = database.indicator.get(i2.id)
    assert len(c1.indicators) == 2
    assert len(c2.indicators) == 2
    assert IndicatorAssignment(indicator_id=i1.id, amount=0.1) in c1.indicators
    assert IndicatorAssignment(indicator_id=i2.id, amount=0.2) in c1.indicators
    assert IndicatorAssignment(indicator_id=i1.id, amount=0.3) in c2.indicators
    assert IndicatorAssignment(indicator_id=i2.id, amount=0.4) in c2.indicators

    # Clean up
    database.country.delete(c1.id)
    database.country.delete(c2.id)
    database.indicator.delete(i1.id)
    database.indicator.delete(i2.id)


def test_import_entity(test_database):
    database = test_database

    # How many countries do we have *before* the test?
    count = len(database.country.get_all())

    # Run the import command
    cmd = 'python3 -m pse_backend import-entity country tests/data_import/countries.csv'
    pse_options = 'database.dbname=pse_test_database'
    pse_options = os.environ['PSE_OPTIONS'] + ';' + pse_options if 'PSE_OPTIONS' in os.environ else pse_options
    env = {**os.environ, 'PSE_OPTIONS': pse_options}
    process = subprocess.Popen(cmd.split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env)
    out, err = process.communicate()
    try:
        assert process.returncode == 0
    except AssertionError as e:
        print(f'\nSTDOUT: {out.decode("utf8")}\nSTDERR: {err.decode("utf8")}')
        raise e

    # Check that countries were created
    countries = database.country.get_all()
    assert len(countries) > count
    c1 = [c for c in countries if c.code == 'C1']
    assert len(c1) == 1
    assert c1[0].name == 'CSV Imported Country 1'
    c2 = [c for c in countries if c.code == 'C2']
    assert len(c2) == 1
    assert c2[0].name == 'CSV Imported Country 2'

    # Clean up
    database.country.delete(c1[0].id)
    database.country.delete(c2[0].id)
