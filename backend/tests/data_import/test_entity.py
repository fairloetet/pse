import textwrap
from io import StringIO

from pse_backend.data_import.entity import import_entities_from_csv_into_db


def import_entity_template(database, entity_name, data_file, attrib_dicts):
    # Importing the CSV should create the entities
    with open(data_file) as csv_file:
        counts1 = import_entities_from_csv_into_db(database, entity_name, csv_file, update=True)

    entity = database.get_entity(entity_name)

    # Check that successes were reported
    assert counts1['error'] == 0
    assert counts1['created'] > 0

    # Check that all attributes are present
    for attrib_dict in attrib_dicts:
        obj = entity.get_where({'slug': attrib_dict['slug']}, unique=True)[0]
        for k, v in attrib_dict.items():
            assert obj.__getattribute__(k) == v

    # Importing the CSV again should update the entities
    with open(data_file) as csv_file:
        counts2 = import_entities_from_csv_into_db(database, entity_name, csv_file, update=True)

    assert counts2['error'] == 0
    assert counts1['created'] == counts2['updated']


def test_import_countries(database):
    import_entity_template(database,
                           entity_name='country',
                           data_file='tests/data_import/countries.csv',
                           attrib_dicts=[{'slug': 'C1', 'name': 'CSV Imported Country 1'},
                                         {'slug': 'C2', 'name': 'CSV Imported Country 2'}])


def test_import_materials(database):
    import_entity_template(database,
                           entity_name='material',
                           data_file='tests/data_import/materials.csv',
                           attrib_dicts=[{'slug': 'imported-mat-1', 'name': 'CSV Imported Material 1'},
                                         {'slug': 'imported-mat-2', 'name': 'CSV Imported Material 2'}])


def test_import_components(database):
    import_entity_template(database,
                           entity_name='component',
                           data_file='tests/data_import/components.csv',
                           attrib_dicts=[{'slug': 'imported-component-1', 'name': 'CSV Imported Component 1'},
                                         {'slug': 'imported-component-2', 'name': 'CSV Imported Component 2'}])


def test_import_indicators(database):
    import_entity_template(database,
                           entity_name='indicator',
                           data_file='tests/data_import/indicators.csv',
                           attrib_dicts=[{'slug': 'imported-ind-1', 'name': 'CSV Imported Indicator 1'},
                                         {'slug': 'imported-ind-2', 'name': 'CSV Imported Indicator 2'}])


def test_skip_invalid(database):
    # Entities should never be overwritten. If an entity cannot be created, it should just be skipped.

    # Import some countries – this should work.
    with open('tests/data_import/countries.csv') as csv_file:
        import_entities_from_csv_into_db(database, 'country', csv_file, database)

    count = len(database.country.get_all())

    # Check that countries were created
    countries = database.country.get_all()
    c1 = [c for c in countries if c.code == 'C1']
    assert len(c1) == 1
    assert c1[0].name == 'CSV Imported Country 1'
    c2 = [c for c in countries if c.code == 'C2']
    assert len(c2) == 1
    assert c2[0].name == 'CSV Imported Country 2'

    # Try to import the same countries again.
    # Nothing should happen, as we don't allow a country code to be duplicated.
    with open('tests/data_import/countries.csv') as csv_file:
        import_entities_from_csv_into_db(database, 'country', csv_file, database)

    # No exception should be thrown!
    count2 = len(database.country.get_all())
    assert count2 == count

    # Clean up
    database.country.delete(c1[0].id)
    database.country.delete(c2[0].id)


def test_quote_handling(database):
    csv = textwrap.dedent('''\
    name,code,slug
    Country Without Quotes,C1,C1
    "Quoted Country",C2,C2
    'Single Quote Country',C3,C3
    "Country Containing ""Quotes""",C4,C4
    ''')
    csv_file = StringIO(csv)
    
    # Importing the CSV should create the countries
    import_entities_from_csv_into_db(database, 'country', csv_file, update=True)

    # Check that all expected countries are present
    countries = database.country.get_all()
    c1 = [c for c in countries if c.code == 'C1'][0]
    assert c1.name == 'Country Without Quotes'
    c2 = [c for c in countries if c.code == 'C2'][0]
    assert c2.name == 'Quoted Country'
    # Single quotes should not be interpreted
    c3 = [c for c in countries if c.code == 'C3'][0]
    assert c3.name == "'Single Quote Country'"
    # Two double quotes should be interpreted as escaping
    c4 = [c for c in countries if c.code == 'C4'][0]
    assert c4.name == 'Country Containing "Quotes"'

    # Clean up
    database.country.delete(c1.id)
    database.country.delete(c2.id)
    database.country.delete(c3.id)
    database.country.delete(c4.id)
