from io import StringIO

from pse_backend.db import Database
from pse_backend.models import CountryNew, IndicatorNew, IndicatorAssignment, ProductNew, ComponentNew, MaterialNew
from pse_backend.data_import.assignment import import_assignment_into_db


def test_import_assignment(database):
    # Add some data
    c1 = database.country.create(CountryNew(name='Country1', code='C1'))
    c2 = database.country.create(CountryNew(name='Country2', code='C2'))
    i1 = database.indicator.create(IndicatorNew(name='Indicator1'))
    i2 = database.indicator.create(IndicatorNew(name='Indicator2'))

    # Importing the CSV should create all the associations
    with open('tests/data_import/country_indicators.csv') as csv_file:
        import_assignment_into_db(database, 'country', 'indicators', csv_file)

    c1 = database.country.get(c1.id)
    c2 = database.country.get(c2.id)

    assert len(c1.indicators) == 2
    assert len(c2.indicators) == 2
    assert IndicatorAssignment(indicator_id=i1.id, amount=0.1) in c1.indicators
    assert IndicatorAssignment(indicator_id=i2.id, amount=0.2) in c1.indicators
    assert IndicatorAssignment(indicator_id=i1.id, amount=0.3) in c2.indicators
    assert IndicatorAssignment(indicator_id=i2.id, amount=0.4) in c2.indicators

    # Clean up
    database.country.delete(c1.id)
    database.country.delete(c2.id)
    database.indicator.delete(i1.id)
    database.indicator.delete(i2.id)


def impl_import_with_parent_id(database: Database,
                               parent_entity_name: str,
                               assignment_name: str,
                               child_id_name: str,
                               parent,
                               children,
                               csv: str):
    csv_file = StringIO(csv)
    import_assignment_into_db(database, parent_entity_name, assignment_name, csv_file, parent_id=parent.id)

    p = database.get_entity(parent_entity_name).get(parent.id)
    assignments = getattr(p, assignment_name)
    assert len(assignments) == len(children)
    for c in children:
        assert any(getattr(assignment, child_id_name) == c.id for assignment in assignments)

    # Length should not change when importing twice: existing assignments should be replaced
    csv_file = StringIO(csv)
    import_assignment_into_db(database, parent_entity_name, assignment_name, csv_file, parent_id=parent.id)

    p = database.get_entity(parent_entity_name).get(parent.id)
    assignments = getattr(p, assignment_name)
    assert len(assignments) == len(children)


def test_import_product_components_with_parent_id(database):
    # Tests importing a CSV list of components into a product.
    # In this case, the parent is specified separately, not as a column in the CSV file.

    # Add a product and a few components
    p = database.product.create(ProductNew(name='Test Product'))
    cs = [database.component.create(
        ComponentNew(name=f'Test Component {i}', weight=1.0)) for i in range(5)]

    # Define CSV data expressing component assignments
    csv = 'slug,amount\n'
    for c in cs:
        csv += f'{c.slug},1.0\n'

    impl_import_with_parent_id(database, 'product', 'components', 'component_id', p, cs, csv)


def test_import_component_materials_with_parent_id(database):
    # Add a component and a few materials
    p = database.component.create(ComponentNew(name='Test Component', weight=1.0))
    cs = [database.material.create(
        MaterialNew(name=f'Test Material {i}')) for i in range(5)]

    # Define CSV data expressing component assignments
    csv = 'slug,amount\n'
    for c in cs:
        csv += f'{c.slug},1.0\n'

    impl_import_with_parent_id(database, 'component', 'materials', 'material_id', p, cs, csv)
