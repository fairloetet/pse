import pytest

from pse_backend.config.config import load_config, parse_config_options


def test_load_config_default(monkeypatch):
    # When instructed to load a certain config, the config should be loaded
    try:
        monkeypatch.delenv('PSE_OPTIONS')
    except KeyError:
        pass
    monkeypatch.setenv("PSE_CONFIG", "tests/config/pse-test.toml")
    config = load_config()
    assert config.database.host == "testhost"
    # Values not set in the config should be unaffected
    assert config.database.port == 5432


def test_override(monkeypatch):
    # When instructed to load a certain config, the config should be loaded
    monkeypatch.setenv("PSE_CONFIG", "tests/config/pse-test.toml")
    monkeypatch.setenv("PSE_OPTIONS", "database.dbname=dbname-override")
    config = load_config()

    # Database name should be overridden
    assert config.database.dbname == 'dbname-override'
    # Other values should be unaffected
    assert config.database.port == 5432


@pytest.mark.parametrize("optstr,expected", [
    ("someopt=12345", {'someopt': '12345'}),  # single top-level option
    ("sub.someopt=12345", {'sub.someopt': '12345'}),  # single second-level option
    ("opt1=1;opt2=2", {'opt1': '1', 'opt2': '2'}),  # two options
    ("opt1=1;opt2=2;opt3=3;opt4=4", {'opt1': '1', 'opt2': '2', 'opt3': '3', 'opt4': '4'}),  # four options
    ('opt1="1";opt2=2', {'opt1': '1', 'opt2': '2'}),  # one option is quoted
    ('opt1="1";opt2="2"', {'opt1': '1', 'opt2': '2'}),  # both options are quoted
    ('opt1="a;b;c";opt2=d', {'opt1': 'a;b;c', 'opt2': 'd'}),  # one option is quoted and contains semicolons
    (r'opt1="a\"b\"c";opt2=d', {'opt1': 'a"b"c', 'opt2': 'd'}),  # one option is quoted and contains quotation marks
])
def test_parse_config_options_valid(optstr, expected):
    parsed = parse_config_options(optstr)
    assert parsed == expected
