import numpy as np

from pse_backend.util.math import normalize


def test_normalize():
    v1 = np.array([4.0, 3.0, 2.0, 1.0, 0.0])
    assert (normalize(v1) == np.array([0.4, 0.3, 0.2, 0.1, 0.0])).all()
