# pse_backend

The backend component of the PSE app.

## Development Setup

To get started with backend development, follow these steps.

1. Python 3.7 is required. Make sure your python version is up to date.

    ```bash
    $ python3 --version
    Python 3.7.4
    ```
    
    If your Python version is less than 3.7, install a more recent 
    version, e.g. using [pyenv](https://github.com/pyenv/pyenv).

2. Some package requirements are needed to set up the Python environment. On Ubuntu, the following command works:
    
   ```bash
   $ sudo apt install build-essential docker.io libpq-dev postgresql-client
   ```
   
   Make sure to give your user permissions to run `docker`, e.g.
   
   ```bash
   $ sudo usermod -G docker -a <youruser>
   ```

3. Project metadata and dependencies are defined using
   [Poetry](https://poetry.eustace.io/).
   If you don't have Poetry, you will need to install it.
   See [Poetry's docs](https://poetry.eustace.io/docs/) for more info
   or simply run the following command:

    ```bash
    $ curl -sSL https://install.python-poetry.org | python3 -
    ```

4. Install the project dependencies. This step is automated with Poetry.
   It will create a virtual environment and download and install all
   the project dependencies.

    ```bash
    $ poetry install
    ```


### Running the app

To run the app, first start the database server. This requires `docker` and `psql` to be available.

```bash
$ ./dbup.sh
```

Then start the app.

```bash
$ poetry run python -m pse_backend run
```

Backend endpoints can be accessed at [http://localhost:8000](http://localhost:8000).

The API documentation can be accessed at [http://localhost:8000/docs](http://localhost:8000/docs) or
[http://localhost:8000/redoc](http://localhost:8000/redoc) (pick the format you like better).

After stopping the app, you may also stop the database server.

```bash
$ ./dbdown.sh
```


### Setting up with Visual Studio Code

* Tested on Linux with VSCode 1.38.0*

1. Open the `backend` directory in Visual Studio Code.

2. Install the *Python* extension. *Extensions* tab.

3. From the palette, select *Open Settings (JSON)* and add the following setting:

    ```
    "python.venvPath": "~/.cache/pypoetry/virtualenvs"
    ```

    Adapt the path if your poetry virtualenv is located elsewhere.

4. From the palette, select *Python: Select Interpreter* and choose the 
   Python interpreter located in a directory starting with `pse-backend`.


### Setting up with PyCharm

*Tested on Linux with Pycharm CE 2019.2*

1. Open the `backend` directory as a new project with PyCharm.

2. Configure the project interpreter. On Linux, this setting is located
   under *File/Settings/Project: backend/Project Interpreter*.
   
   Next to the drop down that selects an interpreter, click the Gears
   icon and choose *Add…*. In the dialog that opens, choose
   *Existing environment*. Navigate to the directory where Poetry
   stored your virtual environment. On Linux, this is at
   `~/.cache/pypoetry/virtualenvs`. Pick the virtualenv that starts
   with `pse-backend` and select the `bin/python` executable.
   
3. Add a run configuration for the `pse_backend` module. Right-click the file
   `pse_backend/__main__.py` in the *Project* view and select *Create
   \_\_main\_\_…*. Into the *Parameters* field, enter `run`. Click *OK*.
   
   You can now run and/or debug the application using the corresponding
   toolbar icons or the commands from the *Run* menu.


## Database Administration

The backend stores persistent data in a SQL database. The schema is defined
using [SQLAlchemy](https://www.sqlalchemy.org/) in the `pse_backend.db.dbo`
module. We use [Alembic](https://alembic.sqlalchemy.org/) to manage database
migrations.

### Initialising and Upgrading

To upgrade an existing database, or initialise a new database, use the following
command:

```bash
poetry run alembic upgrade head
```

(This is done automatically by `dbup.sh`. You might need it if you want to apply
changes to your running database without restarting it.)

### Creating Migrations

Whenever you change the database schema definitions in `pse_backend.db.dbo`, you
must create a new revision and add it to the Git repository. Alembic provides
the `alembic revision` command to achieve this.

Alembic is usually very good at automatically detecting schema changes, so you
should usually use the `--autogenerate` flag.

```bash
poetry run alembic revision --autogenerate -m "Brief description of revision content"
```

However, always check the content of the generated migration script, as there
are changes which Alembic cannot detect. Most notably, renamed elements will not
be detected properly.

Having created the revision, you should also apply it to your local database:

```bash
poetry run alembic upgrade head
```

### Merging Migrations

When two branches are merged and schema changes were made in both branches,
there are now several “head” revisions. The following command creates a new
revision which combines both revisions into one:

```bash
poetry run alembic merge -m "Merge multiple heads"
```

Then upgrade your local database as described above, and commit the generated
migration script.

### How Alembic Works

Alembic generates migration scripts, which reside in the `alembic/versions`
directory.

`alembic/env.py` defines the environment within which the migrations are
executed. Note that the `DATABASE_URL` is imported from the backend and is thus
dynamic and sensitive to configuration. This makes sure that migrations always
use the same database as the operating backend.

For more information, check the [Alembic docs](https://alembic.sqlalchemy.org/).
