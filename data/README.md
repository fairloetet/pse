# Fairtronics Data Repository

Contains source data files and associated processing scripts.

We try to keep source files as close as possible to the original format and
provide scripts to convert them to a format that can be imported into the
Fairtronics database.

## Produce processed data

A `Makefile` is provided to make data processing easy. The following requirements apply:

- Python 3.7+
- [poetry](https://poetry.eustace.io/) 0.12+

Then process the data using these commands:

    $ make prepare
    $ make
    
## Import data

You can import the data into a running backend database, e.g. for development purposes, using the `import_all.sh`
script. Before you do this, process the data as described in the section above.

**Caution:** Data import may overwrite existing data in your database. When importing, entities are match by their
*slug*. If any entity exists with the same slug as an imported entity, it is overwritten! Any existing data assignments
between entities, such as material composition of components, raw material provenance, or indicator data, will also
be overwritten.

1. Move to the `backend` directory.

    ```bash
    $ cd /path/to/pse/backend
    ```

2. Make sure the backend database is running. You can start it manually:

    ```bash
    $ ./dbup.sh
    ```
   This starts up an empty database.

3. Enter a poetry shell so the backend code is available:

    ```bash
    $ poetry shell
    ```
    
    If this doesn't work because of missing dependencies, try running `poetry install` first.

4. Import the data:

    ```bash
    $ ../data/scripts/import_data.sh
    ```

    You should see success messages. The data should now be available in your backend.

## Data Sources

This section documents the sources of the data sets we use.

### Country Data

A map of country codes is located in `input/country_codes.csv`.

| File                | Source  | Accessed |
| ------------------- | ------- | -------- |
| `country_codes.csv` | [GitHub lukes](https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes/blob/master/all/all.csv)       | 10/2019  |

### Material Data

A list of materials is located in `input/materials.csv`. It defines the names
and identifiers (*slugs*) for the known materials and is maintained by the
Fairtronics project.

| File            | Source      | Accessed |
| --------------- | ----------- | -------- |
| `materials.csv` | Fairtronics | n/a      |

### Material Production Shares

Material share files are located in `input/material_shares`.

| File             | Source                                                                                                                     | Accessed |
| ---------------- | -------------------------------------------------------------------------------------------------------------------------- | -------- |
| `bauxite.csv`    | [USGS](https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/s3fs-public/atoms/files/mcs-2019-bauxi.pdf) | 10/2019  |
| `chromium.csv`   | [USGS](https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/s3fs-public/atoms/files/mcs-2019-chrom.pdf) | 10/2019  |
| `copper.csv`     | [USGS](https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/s3fs-public/atoms/files/mcs-2019-coppe.pdf) | 10/2019  |
| `iron_ore.csv`   | [USGS](https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/s3fs-public/atoms/files/mcs-2019-feore.pdf) | 10/2019  |
| `nickel.csv`     | [USGS](https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/atoms/files/mcs-2019-nicke.pdf)             | 10/2019  |
| `silver.csv`     | [USGS](https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/atoms/files/mcs-2019-silve.pdf)             | 10/2019  |
| `tin.csv`        | [USGS](https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/atoms/files/mcs-2019-tin.pdf)               | 10/2019  |

### Indicator Data

Indicator data on a country or region level are located in `input/indicator_data`.

#### World Regions

The ILO provides some data on the level of world regions. The mapping from regions to countries is [defined by the ILO](https://www.ilo.org/global/regions/lang--en/index.htm).

A copy of the region mapping can be found in `region-mapping.csv`. This file is used during conversion of region-level
indicator data.

#### Child Labour

The indicator is "Share of children engaged in economic activity and household chores (in percent), latest year" as provided by ILOSTAT in the infographic in https://ilostat.ilo.org/topics/child-labour/ (accessed 10 2019).

The individual values per country stem from different years, the oldest from 2010. Some values are given for ages 5 to 17, others for ages 5 to 14.

The provided list distinguishes between values for female, male and total. We only used the values for total.

#### Freedom of Association and Collective Bargaining

The indicator is "Share of employees covered by one or more collective agreement (in percent), latest year)" as provided by ILOSTAT in the infographic under https://ilostat.ilo.org/topics/collective-bargaining/ (accessed 10 2019).

The individual values per country stem from different years, the oldest from 2008.

We used the list as is, only removing unneeded columns "MEASURE" and "TIME_PERIOD".

#### Discrimination

The indicator is "Proportion of women in managerial positions (%)" as provided by ILOSTAT under https://ilostat.ilo.org/data/ (5.5.2 Women in management, Last update 30Sep19).

The Excel file provides multiple entries for many countries with varying source type and time. We only used the latest available value per country for "Total management" (dropping "senior and middle management column).

#### Fair Salary

The indicator is "Share of employment by economic class (in percent), ILO modelled estimates for 2018, with lowest economic class based on the World Bank's international poverty line of $1.90 a day (using 2011 PPPs)", as provided by ILOSTAT in the infographic under https://ilostat.ilo.org/topics/working-poor/ (accessed 10 2019).

We only used the values for "Extremely poor".

#### Forced Labour

The indicator is “prevalence of forced labour” as assessed by the ILO. 

Data is taken from the ILO report [Global estimates of modern slavery: forced labour and forced marriage](https://www.ilo.org/wcmsp5/groups/public/@dgreports/@dcomm/documents/publication/wcms_575479.pdf), page 19, table 2.

Data are reported on the level of world regions. We map the data to individual countries.

#### Health

The indicator is "Fatal occupational injuries per 100'000 workers" as provided by ILOSTAT under https://ilostat.ilo.org/data/ (accessed 10 2019).

The Excel file provides multiple entries for Source and  Time per country. We selected the latest available value. We only used the values for "Total" and "Fatal" (dropping the distinction between male/female and non-fatal injuries).

#### Hours of Work

The indicator is "Share of employed persons working 40 or more hours per week, latest year", as provided by ILOSTAT in the infographic under https://ilostat.ilo.org/topics/working-time/ (accessed 10 2019).

The individual values per country stem from different years, the oldest from 2015.

We only used the values for "49 or more hours per week" and dropped "40 to 48 hours per week".

#### Social Protection

The indicator is "Share of population covered by at least one social protection benefit (in percent), latest year" as provided by ILOSTAT in the infographic under https://ilostat.ilo.org/topics/social-protection/ (accessed 10 2019).

The individual values per country stem from different years, the oldest from 2016.
