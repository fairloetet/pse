# Description of material shares

## Bauxite

Note that bauxite is the principle ore of aluminium. Alumina (aluminium oxide) in comparison is an intermediate product of refining bauxite to aluminium.

Data is taken from 2018 estimation of World Mine Production and Reserves from 
https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/s3fs-public/atoms/files/mcs-2019-bauxi.pdf
, linked as 2019 Mineral Commodity Summary by 
https://www.usgs.gov/centers/nmic/bauxite-and-alumina-statistics-and-information
, linked by 
https://www.usgs.gov/centers/nmic/commodity-statistics-and-information 

The zero value for United States of America may not be correct due to closure of data.

## Chromium

Data is taken from 2018 estimation of World Mine Production and Reserves from 
https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/s3fs-public/atoms/files/mcs-2019-coppe.pdf
, linked as 2019 Mineral Commodity Summary by 
https://www.usgs.gov/centers/nmic/chromium-statistics-and-information
, linked by 
https://www.usgs.gov/centers/nmic/commodity-statistics-and-information 

## Copper

Data is taken from 2018 estimation of World Mine Production and Reserves from 
https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/s3fs-public/atoms/files/mcs-2019-coppe.pdf
, linked as 2019 Mineral Commodity Summary by 
https://www.usgs.gov/centers/nmic/copper-statistics-and-information
, linked by 
https://www.usgs.gov/centers/nmic/commodity-statistics-and-information 

## Gold

Data is taken from 2018 estimation of World Mine Production and Reserves from 
https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/s3fs-public/atoms/files/mcs-2019-gold.pdf
, linked as 2019 Mineral Commodity Summary by 
https://www.usgs.gov/centers/nmic/gold-statistics-and-information
, linked by 
https://www.usgs.gov/centers/nmic/commodity-statistics-and-information 

## Iron ore

Note that this represents data on iron ore, not on iron itself which only seems to be used as pig iron (intermediate product within refinement) or row steel (final product incl. refinement).
The usage/crude ore data is used here, not the iron content of it. 

Data is taken from 2018 estimation of World Mine Production and Reserves from 
https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/s3fs-public/atoms/files/mcs-2019-feore.pdf
, linked as 2019 Mineral Commodity Summary by 
https://www.usgs.gov/centers/nmic/iron-ore-statistics-and-information
, linked by 
https://www.usgs.gov/centers/nmic/commodity-statistics-and-information 

## Nickel

Data is taken from 2018 estimation of World Mine Production and Reserves from 
https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/atoms/files/mcs-2019-nicke.pdf
, linked as 2019 Mineral Commodity Summary by 
https://www.usgs.gov/centers/nmic/nickel-statistics-and-information
, linked by 
https://www.usgs.gov/centers/nmic/commodity-statistics-and-information

## Palladium

Data is taken from 2018 estimation of World Mine Production and Reserves from 
https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/atoms/files/mcs-2019-plati.pdf
, linked as 2019 Mineral Commodity Summary by 
https://www.usgs.gov/centers/nmic/platinum-group-metals-statistics-and-information
, linked by 
https://www.usgs.gov/centers/nmic/commodity-statistics-and-information 

## Silver

Data is taken from 2018 estimation of World Mine Production and Reserves from 
https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/atoms/files/mcs-2019-silve.pdf
, linked as 2019 Mineral Commodity Summary by 
https://www.usgs.gov/centers/nmic/silver-statistics-and-information
, linked by 
https://www.usgs.gov/centers/nmic/commodity-statistics-and-information

## Tin

Data is taken from 2018 estimation of World Mine Production and Reserves from 
https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/atoms/files/mcs-2019-tin.pdf
, linked as 2019 Mineral Commodity Summary by 
https://www.usgs.gov/centers/nmic/tin-statistics-and-information
, linked by 
https://www.usgs.gov/centers/nmic/commodity-statistics-and-information








