# Description of indicators

## Child Labour

The indicator is "Share of children engaged in economic activity and household chores (in percent), latest year" as provided by ILOSTAT in the infographic in https://ilostat.ilo.org/topics/child-labour/ (accessed 10 2019).

The individual values per country stem from different years, the oldest from 2010. Some values are given for ages 5 to 17, others for ages 5 to 14.

The provided list distinguishes between values for female, male and total. We only used the values for total.

## New Child Labour

The indicator reflects the share of working children within the workforce of a country. It is compiled of several indicators provided by The World Back in its "World Development Indicators" (https://databank.worldbank.org/source/world-development-indicators), that is:
* Children in employment, total (% of children ages 7-14) [SL.TLF.0714.ZS]
* Average working hours of children, working only, ages 7-14 (hours per week) [SL.TLF.0714.WK.TM]
* Labor force participation rate, total (% of total population ages 15-64) (modeled ILO estimate) [SL.TLF.ACTI.ZS]
* Population ages 0-14, total [SP.POP.0014.TO]
* Population ages 15-64, total [SP.POP.1564.TO]

The individual values per country stem from different years, the oldest from 1999, the most recent from 2016. 

The calculation of the indicator is somewhat complex. The reasoning is that we
wish to represent the prevalence of child labour among the work that goes into a
product. For every hour worked, how likely is it that it was worked by a child?
The calculation of this risk involves multiple steps:

- Compute number of all working children (from total children in employment and
  total child population).
    - Caveat: There is a mismatch because we only have employment data for
      children aged 7–14 but include the child population for ages 0–14
- Estimate the number of hours worked by children (from average working hours of
  working children).
- Estimate the number of hours worked by adults (from adult population and
  labour force participation).
    - Caveat: This assumes 48 worked hours per week for adults
- The quotient of the hours worked by children and the hours worked by adults
  gives the risk of child labour.

## Forced Labour

The indicator is "Estimated prevalence of modern slavery by country (victims per 1,000 population)" as provided by the Global Slavery Index 2018 Dataset, Walk Free Foundation, available from www.globalslaveryindex.org.

The individual values per country are based on surveys carried out in 48 countries of the 167 countries provided. The outcomes were extrapolated by the Walk Free Foundation “to countries with an equivalent risk profile” for the remaining. Some values are missing.

We used the list as is, converting from per mill to percent, and mapping the country names to ISO-2.

## Freedom of Association and Collective Bargaining

The indicator is "Share of employees covered by one or more collective agreement (in percent), latest year)" as provided by ILOSTAT in the infographic under https://ilostat.ilo.org/topics/collective-bargaining/ (accessed 10 2019).

The individual values per country stem from different years, the oldest from 2008.

We used the list as is, only removing unneeded columns "MEASURE" and "TIME_PERIOD", mapping the country names to ISO-2 (most automated, some manually).

## Fair Salary

The indicator is "Share of employment by economic class (in percent), ILO modelled estimates for 2018, with lowest economic class based on the World Bank's international poverty line of $1.90 a day (using 2011 PPPs)", as provided by ILOSTAT in the infographic under https://ilostat.ilo.org/topics/working-poor/ (accessed 10 2019).

We only used the values for "Extremely poor" and mapped the country names to ISO-2 (most automated, some manually)

## Hours of Work

The indicator is "Share of employed persons working 40 or more hours per week, latest year", as provided by ILOSTAT in the infographic under https://ilostat.ilo.org/topics/working-time/ (accessed 10 2019).

The individual values per country stem from different years, the oldest from 2015.

We only used the values for "49 or more hours per week" and dropped "40 to 48 hours per week". Most country names were mapped automatically to ISO-2, some manually.

## Social Protection

The indicator is "Share of population covered by at least one social protection benefit (in percent), latest year" as provided by ILOSTAT in the infographic under https://ilostat.ilo.org/topics/social-protection/ (accessed 10 2019).

The individual values per country stem from different years, the oldest from 2016.

We automatically mapped the provided ISO-3 codes to ISO-2 and dropped all columns except "OBS_VALUE".

## Discrimination

The indicator is "Proportion of women in managerial positions (%)" as provided by ILOSTAT under https://ilostat.ilo.org/data/ (5.5.2 Women in management, Last update 30Sep19).

The Excel file provides multiple entries for many countries with varying source type and time. We only used the latest available value per country for "Total management" (dropping "senior and middle management column).

## Health

The indicator is "Fatal occupational injuries per 100'000 workers" as provided by ILOSTAT under https://ilostat.ilo.org/data/ (accessed 10 2019).

The Excel file provides multiple entries for Source and  Time per country. We selected the latest available value. We only used the values for "Total" and "Fatal" (dropping the distinction between male/female and non-fatal injuries).

We mapped the country names to ISO-2 codes (most automated, some manually).s








