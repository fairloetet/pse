#!/usr/bin/env bash
# 
# Usage: import_all.sh
# 
# Imports all base data.
# 
# Requires that data have been converted using `make` and the resulting CSV
# files are in the `output` directory.
# 
# You must run this script in a virtual environment where the PSE backend is
# installed! E.g., move to the `backend` directory and run `poetry shell`,
# then run this script. 

set -e

SCRIPT_DIR="$(readlink -f $(dirname "$0"))"
OUTPUT="${SCRIPT_DIR}"

# Import entities

echo "Importing countries…"
python -m pse_backend import-entity country "${OUTPUT}/countries.csv"

echo "Importing indicators…"
python -m pse_backend import-entity indicator "${OUTPUT}/indicators.csv"

echo "Importing materials…"
python -m pse_backend import-entity material "${OUTPUT}/materials.csv"

echo "Importing components…"
python -m pse_backend import-entity component "${OUTPUT}/components.csv"

echo "Importing products…"
python -m pse_backend import-entity product "${OUTPUT}/products.csv"

echo "Importing indicator data…"
python -m pse_backend import-assignment --parent Country --attribute indicators "${OUTPUT}/indicator_data.csv"

echo "Importing material production data…"
python -m pse_backend import-assignment --parent Material --attribute countries "${OUTPUT}/material_shares.csv"

echo "Importing component material composition data…"
python -m pse_backend import-assignment --parent Component --attribute materials "${OUTPUT}/component_materials.csv"

echo "Importing product component data…"
python -m pse_backend import-assignment --parent Product --attribute components "${OUTPUT}/product_components.csv"
