#!/usr/bin/env python
"""
Generates `indicators.csv` from indicator data.

All CSV files in the `indicators` directory are read. The CSV file name is
expected to correspond to the slug of an indicator (as in `indicators.csv`). A
comprehensive output file for data import is then generated.
"""

import click
import pandas as pd
from pathlib import Path
import re

from scripts.util.country_codes import CountryCodes, AmbiguousError

country_codes = None

disambiguations = {
    'Cape Verde': 'Cabo Verde',
    'Czech Republic': 'Czechia',
    'Hong Kong, China': 'Hong Kong',
    'Iran, Islamic Republic of': 'Iran (Islamic Republic of)',
    'Korea, Democratic People\'s Republic of': 'Korea (Democratic People\'s Republic of)',
    'Macau, China': 'Macao',
    'Micronesia, Federated States of': 'Micronesia (Federated States of)',
    'Occupied Palestinian Territory': 'Palestine, State of',
    'Taiwan, China': 'Taiwan, Province of China',
    'United States': 'United States of America',
    'Venezuela, Bolivarian Republic of': 'Venezuela (Bolivarian Republic of)',
    'Swaziland': 'Eswatini',
    'Macedonia, the former Yugoslav Republic of': 'North Macedonia',
    'Viet Nam': 'Vietnam',
    'Bolivia, Plurinational State of': 'Bolivia (Plurinational State of)'
}


@click.command()
@click.argument('input_dir', type=click.Path(exists=True), default='input/indicator_data')
@click.argument('output_file', type=click.Path(), default='output/indicator_data.csv')
@click.option('--countries', type=click.Path(exists=True), default='input/country_codes.csv')
@click.option('--region-mapping', type=click.Path(exists=True), default='input/indicator_data/region-mapping.csv')
def main(input_dir, output_file, countries, region_mapping):
    # Read list of country codes
    global country_codes
    country_codes = CountryCodes.load(countries)
    global region_map
    region_map = pd.read_csv(region_mapping, dtype=str, keep_default_na=False)

    # Make an empty output data frame, to which we then append the contents of each input file
    output_df = pd.DataFrame(columns=['Country.code', 'Indicator.slug', 'amount'])

    input_path = Path(input_dir)
    for indicator_file_name, parsing_function in indicator_files.items():
        input_df = parsing_function(input_path / indicator_file_name)
        output_df = output_df.append(input_df)

    output_df.to_csv(output_file, index=False)


def read_child_labour(csv_file):
    global country_codes
    global disambiguations
    df = pd.read_csv(csv_file)
    df = df.assign(**{'Indicator.slug': 'child-labour'})

    try:
        df['Country.code'] = df['ISO3'].map(country_codes.iso3_to_iso2)
    except KeyError as e:
        raise Exception(f'Cannot process {csv_file}. Unknown country code encountered: {e}')

    df['amount'] = df['Total']
    return df[['Country.code', 'Indicator.slug', 'amount']]


def read_new_child_labour(csv_file):
    global country_codes
    global disambiguations

    # Define used column names
    country_code_column = 'Country Code'
    weekly_working_hours_of_employed_children_column = \
        'Average working hours of children, working only, ages 7-14 (hours per week) [SL.TLF.0714.WK.TM]'
    share_employed_children_in_all_children_column = \
        'Children in employment, total (% of children ages 7-14) [SL.TLF.0714.ZS]'
    # TODO There's no value for children population 7-14
    number_all_children_column = 'Population ages 0-14, total [SP.POP.0014.TO]'
    share_employed_adults_in_all_adults_column = 'Labor force participation rate, total (% of total population ages ' \
                                                 '15-64) (modeled ILO estimate) [SL.TLF.ACTI.ZS]'
    number_all_adults_column = 'Population ages 15-64, total [SP.POP.1564.TO]'

    # Read and tidy data
    df = pd.read_csv(csv_file, na_values="..")
    df = df.dropna(subset=[country_code_column, share_employed_children_in_all_children_column,
                           number_all_children_column, share_employed_adults_in_all_adults_column,
                           number_all_adults_column, weekly_working_hours_of_employed_children_column])

    # There are region and world summaries in the data as well. Ignore them, and convert codes of the remaining
    country_codes_to_be_ignored = ["WLD", "ARB", "CSS", "CEB", "EAR", "EAS", "EAP", "TEA", "EMU", "ECS", "ECA", "TEC",
                                   "EUU", "FCS", "HPC", "HIC", "IBD", "IBT", "IDB", "IDX", "IDA", "LTE", "LCN", "LAC",
                                   "TLA", "LDC", "LMY", "LIC", "LMC", "MEA", "MNA", "TMN", "MIC", "NAC", "INX",
                                   "OED", "OSS", "PSS", "PST", "PRE", "SST", "SAS", "TSA", "SSF", "SSA", "TSS", "UMC"]
    df = df.loc[~df[country_code_column].isin(country_codes_to_be_ignored)]
    try:
        df['Country.code'] = df[country_code_column].map(country_codes.iso3_to_iso2)
    except KeyError as e:
        raise Exception(f'Cannot process {csv_file}. Unknown country code encountered: {e}')

    # 1. Compute number of all working children
    # TODO Take 'Average working hours of children, working only, ages 7-14 (hours per week) [SL.TLF.0714.WK.TM]'
    #  into account as well
    # Get rid of the years and convert into number
    df[share_employed_children_in_all_children_column] = \
        df[share_employed_children_in_all_children_column].str.extract(r'^(.+)\((\d+)\)$')[0].astype('float')
    df[number_all_children_column] = df[number_all_children_column].str.extract(r'^(.+)\((\d+)\)$')[0].astype('float')
    df[weekly_working_hours_of_employed_children_column] = \
        df[weekly_working_hours_of_employed_children_column].str.extract(r'^(.+)\((\d+)\)$')[0].astype('float')
    # Multiply by share of weekly working hours, with 48h being 100%
    df['share_fully_employed_children'] = df[share_employed_children_in_all_children_column] * \
        (df[weekly_working_hours_of_employed_children_column] / 48)
    # Get number of child workers
    df['number_child_workers'] = \
        (df['share_fully_employed_children'] / 100) * df[number_all_children_column]

    # 2. Compute number of all working non-children
    # Get rid of the years and convert into number
    df[share_employed_adults_in_all_adults_column] = \
        df[share_employed_adults_in_all_adults_column].str.extract(r'^(.+)\((\d+)\)$')[0].astype('float')
    df[number_all_adults_column] = df[number_all_adults_column].str.extract(r'^(.+)\((\d+)\)$')[0].astype('float')
    # Get number of adult workers
    df['number_adult_workers'] = \
        (df[share_employed_adults_in_all_adults_column] / 100) * df[number_all_adults_column]

    # 3. Compute share of child workers within all (=child+adult) workers, in percent
    df['amount'] = 100 * df['number_child_workers'] / (df['number_child_workers'] + df['number_adult_workers'])

    # Take what we actually need
    df = df.assign(**{'Indicator.slug': 'new-child-labour'})
    return df[['Country.code', 'Indicator.slug', 'amount']]


def read_collective_bargaining(csv_file):
    global country_codes
    global disambiguations
    df = pd.read_csv(csv_file)
    df = df.assign(**{'Indicator.slug': 'collective-bargaining'})

    # Parse the COUNTRY (YEAR) column into two separate columns
    country_year = df['COUNTRY (YEAR)'].str.extract(r'^(.+) \((\d+)\)$')
    df['Country'] = country_year[0]
    df['Year'] = country_year[1]

    # Only use the latest value for every country
    idx = df.groupby(['Country'])['Year'].transform(max) == df['Year']
    df = df[idx]

    # Disambiguate country names
    df['Country'] = df['Country'].map(lambda x: disambiguations.get(x, x))

    try:
        df['Country.code'] = df['Country'].map(country_codes.name_to_iso2)
    except AmbiguousError as e:
        raise Exception(f'Cannot process {csv_file}. Ambiguous country name prefix: {e}')
    except KeyError as e:
        raise Exception(f'Cannot process {csv_file}. Unknown country: {e}')

    df['amount'] = df['OBS_VALUE']
    return df[['Country.code', 'Indicator.slug', 'amount']]


def read_discrimination(csv_file):
    global country_codes
    global disambiguations
    df = pd.read_csv(csv_file, header=None,
                     names=['country', 'survey_type', 'year', 'amount'])
    df = df.assign(**{'Indicator.slug': 'discrimination'})

    # Select only the highest year
    idx = df.groupby(['country'])['year'].transform(max) == df['year']
    df = df[idx]

    # Disambiguate country names
    df['country'] = df['country'].map(lambda x: disambiguations.get(x, x))

    df['Country.code'] = df['country'].map(lambda x: country_codes.name_to_iso2(x, error=False))

    # Filter out unknown countries
    df.dropna(inplace=True)

    return df[['Country.code', 'Indicator.slug', 'amount']]


def read_fair_salary(csv_file):
    global country_codes
    global disambiguations
    df = pd.read_csv(csv_file)
    df = df.assign(**{'Indicator.slug': 'fair-salary'})

    # Disambiguate country names
    df['Country'] = df['Country'].map(lambda x: disambiguations.get(x, x))

    df['Country.code'] = df['Country'].map(lambda x: country_codes.name_to_iso2(x, error=False))

    # Filter out unknown countries
    df.dropna(inplace=True)

    df['amount'] = df['Extremely poor']

    return df[['Country.code', 'Indicator.slug', 'amount']]


def read_forced_labour_ilo(csv_file):
    global region_map
    global disambiguations

    # Original data is given region-wise
    region_df = pd.read_csv(csv_file)
    # Remove garbage from dataframe
    region_df.dropna()

    # Create a new data frame with one row per country
    df = region_map.merge(region_df, on='Region', how='left')
    df = df.assign(**{'Indicator.slug': 'forced-labour'})

    # Rename columns
    df['amount'] = df['Prevalence (per thousand)']
    df['Country.code'] = df['Code']

    return df[['Country.code', 'Indicator.slug', 'amount']]


def read_forced_labour_walkfree(csv_file):
    global disambiguations
    global country_codes

    # Original data is given region-wise
    df = pd.read_csv(csv_file, header=None, skiprows=[0, 1, 2], sep=',', quotechar='"', thousands='.', decimal=',',
                     usecols=[0, 1, 2, 3, 4], names=['Country', 'Population', 'Region', 'Prevalence', 'Number'])

    # Disambiguate and code'ify country names
    try:
        df['Country'] = df['Country'].map(lambda x: disambiguations.get(x, x))
        df['Country.code'] = df['Country'].map(country_codes.name_to_iso2)
    except AmbiguousError as e:
        raise Exception(f'Cannot process {csv_file}. Ambiguous country name prefix: {e}')
    except KeyError as e:
        raise Exception(f'Cannot process {csv_file}. Unknown country: {e}')

    # Drop rows where there's no prevalence reported
    df.dropna(subset=['Prevalence'], inplace=True)

    # Convert prevalence and transform from per mill to percent
    df['amount'] = df['Prevalence'].astype('float') / 10

    df = df.assign(**{'Indicator.slug': 'forced-labour'})
    return df[['Country.code', 'Indicator.slug', 'amount']]


def read_health(csv_file):
    global country_codes
    global disambiguations
    df = pd.read_csv(csv_file)
    df = df.assign(**{'Indicator.slug': 'health'})

    # Select only the highest year
    idx = df.groupby(['Reference area'])['Time'].transform(max) == df['Time']
    df = df[idx]

    # Disambiguate country names
    df['Reference area'] = df['Reference area'].map(lambda x: disambiguations.get(x, x))

    df['Country.code'] = df['Reference area'].map(lambda x: country_codes.name_to_iso2(x, error=False))

    # Filter out unknown countries
    df.dropna(inplace=True)

    df['amount'] = df['Total']

    return df[['Country.code', 'Indicator.slug', 'amount']]


def read_hours_of_work(csv_file):
    global country_codes
    global disambiguations
    df = pd.read_csv(csv_file)
    df = df.assign(**{'Indicator.slug': 'hours-of-work'})

    # Parse the COUNTRY (YEAR) column into two separate columns
    country_year = df['COUNTRY (YEAR)'].str.extract(r'^(.+) \((\d+)\)$')
    df['Country'] = country_year[0]
    df['Year'] = country_year[1]

    # Only use the latest value for every country
    idx = df.groupby(['Country'])['Year'].transform(max) == df['Year']
    df = df[idx]

    # Disambiguate country names
    df['Country'] = df['Country'].map(lambda x: disambiguations.get(x, x))

    df['Country.code'] = df['Country'].map(lambda x: country_codes.name_to_iso2(x, error=False))

    # Filter out unknown countries
    df.dropna(inplace=True)

    df['amount'] = df['49 or more hours per week']
    return df[['Country.code', 'Indicator.slug', 'amount']]


def read_social_protection(csv_file):
    global country_codes
    global disambiguations
    df = pd.read_csv(csv_file)
    df = df.assign(**{'Indicator.slug': 'social-protection'})

    try:
        df['Country.code'] = df['REF_AREA'].map(country_codes.iso3_to_iso2)
    except KeyError as e:
        raise Exception(f'Cannot process {csv_file}. Unknown country code encountered: {e}')

    df['amount'] = df['OBS_VALUE']
    return df[['Country.code', 'Indicator.slug', 'amount']]


# Specifies the correct parser function for each indicator
indicator_files = {
    'child-labour.csv': read_child_labour,
    # 'new-child-labour.csv': read_new_child_labour,
    'collective-bargaining.csv': read_collective_bargaining,
    'discrimination.csv': read_discrimination,
    'fair-salary.csv': read_fair_salary,
    # Since they share the same slug, only one of the forced labour indicators
    # may be used at a time.
    # 'forced-labour-ilo.csv': read_forced_labour_ilo,
    'forced-labour-walkfree.csv': read_forced_labour_walkfree,
    'health.csv': read_health,
    'hours-of-work.csv': read_hours_of_work,
    'social-protection.csv': read_social_protection
}

if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
