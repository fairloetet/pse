#!/usr/bin/env python
"""
Processes component definition files to make them ready for import.
"""

from glob import glob
import shutil

import click
import pandas as pd


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('component_file', type=click.Path())
@click.argument('materials_file', type=click.Path())
def main(input_path, component_file, materials_file):
    """
    Prepares component data for data import.

    :param component_file: Destination file name for the component definition file
    :param materials_file: Destination file name for component-material assignments
    """
    # Process the component definition file
    components_df = pd.read_csv(input_path + '/components.csv')
    components_df = process_components(components_df)
    components_df.to_csv(component_file, index=False)
    
    # Process and copy the material list for each component
    materials_dfs = []
    for material_file in glob(input_path + '/*.materials.csv'):
        try:
            input_df = pd.read_csv(material_file)
            check_weights(components_df, input_df)
            output_df = process_material_file(input_df)
            materials_dfs.append(output_df)
        except Exception as e:
            print(f'Exception in {material_file}: {str(e)}')
    materials_df = pd.concat(materials_dfs, sort=False, ignore_index=True)
    materials_df.to_csv(materials_file, index=False)


def process_components(df):
    df['weight_in_g'] = df.apply(lambda row: normalize_weight(row['weight'], row['weight_unit']), axis=1)
    df['weight'] = df['weight_in_g']
    out_df = df[['name', 'slug', 'weight']]
    return out_df


def process_material_file(df):
    """
    Process a single material composition file and write it to the output path.
    """
    if 'amount' in df.columns:
        # If we have an amount (= percentage composition), just normalize it
        df['multiplied_amount'] = df['amount'] * df['quantity']
        df['amount'] = df['multiplied_amount'] / df['multiplied_amount'].sum()
    else:
        # Else derive the composition from the weight.
        df['weight_in_g'] = df.apply(lambda row: normalize_weight(row['weight'], row['weight_unit']), axis=1)
        df['multiplied_weight_in_g'] = df['weight_in_g'] * df['quantity']
        df['amount'] = df['multiplied_weight_in_g'] / df['multiplied_weight_in_g'].sum()
    out_df = df[['Component.slug', 'Material.slug', 'amount']]

    # Sum any contributions of the same material.
    #
    # We need reset_index() because the groupby() converts the grouped columns
    # into indices and reset_index() undoes that.
    out_df = out_df.groupby(['Component.slug', 'Material.slug']).sum().reset_index()

    return out_df


def normalize_weight(weight_numeric: float, weight_unit: str):
    factors = {
        'kg': 1000.0,
        'g': 1.0,
        'mg': 0.001,
    }
    factor = factors[weight_unit]
    return weight_numeric * factor


def check_weights(component_df, materials_df):
    slug = materials_df['Component.slug'][0]  # All are the same
    if 'amount' in materials_df.columns:
        # if 'amount' is specified, it is o.k. by design
        return
    materials_df['weight_in_g'] = \
        materials_df.apply(lambda row: normalize_weight(row['weight'], row['weight_unit']), axis=1)
    materials_df['multiplied_weight_in_g'] = materials_df['weight_in_g'] * materials_df['quantity']
    materials_weight = float(round(materials_df['multiplied_weight_in_g'].sum(), 2))
    component_weight = float(round(component_df.loc[component_df['slug'] == slug, 'weight'], 2))
    if abs(materials_weight - component_weight) > 0.011:
        print("Warning on " + slug + ": \n  component weight " + str(component_weight)
              + "g differs from materials weight " + str(materials_weight) + "g")


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
