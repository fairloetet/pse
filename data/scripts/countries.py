#!/usr/bin/env python
"""
Generates countries.csv from country_codes.csv.
"""

import click
import pandas as pd


@click.command()
@click.argument('input_file', type=click.Path(exists=True))
@click.argument('output_file', type=click.Path())
def main(input_file, output_file):
    countries = pd.read_csv(input_file, dtype=str, keep_default_na=False)
    selected = countries[['name', 'alpha-2']]
    renamed = selected.rename(columns={'alpha-2': 'code'})
    renamed['slug'] = renamed['code']  # add slug column to enable overwriting countries
    renamed.to_csv(output_file, index=False)


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
