#!/usr/bin/env python
"""
Processes component definition files to make them ready for import.
"""

from glob import glob
import shutil

import click
import pandas as pd


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('product_file', type=click.Path())
@click.argument('components_file', type=click.Path())
def main(input_path, product_file, components_file):
    """
    Prepares product data for data import.

    :param product_file: Destination file name for the product definition file
    :param components_file: Destination file name for product-component assignments
    """
    # Process the component definition file
    products_df = pd.read_csv(input_path + '/products.csv')
    products_df = process_products(products_df)
    products_df.to_csv(product_file, index=False)
    
    # Process and copy the material list for each component
    components_dfs = []
    for component_file in glob(input_path + '/*.components.csv'):
        try:
            input_df = pd.read_csv(component_file)
            output_df = process_component_file(input_df)
            components_dfs.append(output_df)
        except Exception as e:
            print(f'Exception in {component_file}: {str(e)}')
    materials_df = pd.concat(components_dfs, sort=False, ignore_index=True)
    materials_df.to_csv(components_file, index=False)


def process_products(df):
    out_df = df[['name', 'slug', 'description']]
    return out_df


def process_component_file(df):
    """
    Process a single material composition file and write it to the output path.
    """
    out_df = df[['Product.slug', 'Component.slug', 'amount']]
    return out_df


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
