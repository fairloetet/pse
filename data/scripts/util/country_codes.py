"""
Translates country codes between formats.
"""

import logging
import pandas as pd
import sys

from pathlib import Path


class CountryCodes:
    def __init__(self, countries_df):
        self.countries_df = countries_df

        tuples_3to2 = self.countries_df[['alpha-3', 'alpha-2']].itertuples()
        self.map_3to2 = {t._1: t._2 for t in tuples_3to2}

    @staticmethod
    def load(country_code_file: Path) -> 'CountryCodes':
        countries_df = pd.read_csv(country_code_file, dtype=str, keep_default_na=False)
        return CountryCodes(countries_df)

    def iso3_to_iso2(self, iso3_code: str) -> str:
        """
        Translate a three-letter country code to a two-letter code.

        :raises: KeyError if country code is unknown.
        """
        return self.map_3to2[iso3_code]

    def name_to_iso2(self, name: str, error: bool = True) -> str:
        """
        Translate the name of a country to its two-letter code.

        :param name: country name or prefix thereof
        :param error: whether to abort with an exception if the name was not found
        
        :raises: KeyError if country is unknown
        :raises: AmbiguousError if name is an ambiguous prefix of several countries
        """
        # First try to match exactly
        rows = self.countries_df[self.countries_df['name'] == name]

        if rows.empty:
            # Try to match by prefix
            rows = self.countries_df[self.countries_df['name'].str.startswith(name)]

        if rows.empty:
            if error:
                raise KeyError(name)
            else:
                logging.warn(f"Unknown country: '{name}'")
                return None
        if len(rows) > 1:
            if error:
                raise AmbiguousError(name)
            else:
                logging.warn(f"Ambiguous country: '{name}'")
                return None

        return rows.iloc[0]['alpha-2']


class AmbiguousError(Exception):
    def __init__(self, name: str):
        Exception.__init__(self, name)
        self.name = name

    def __repr__(self):
        return f"<AmbiguousError name='{self.name}'>"

    def __str__(self):
        return f"'{self.name}'"
