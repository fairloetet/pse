#!/usr/bin/env python
"""
Processes component definition files to make a list of the included materials getting guessed names
"""

from glob import glob
import click
import pandas as pd


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('materials_file', type=click.Path())
def main(input_path, materials_file):
    """
    Prepares materials data for data import.

    :param input_path: Path to the folder with source component's *.materials.csv files
    :param materials_file: Destination file name for materials
    """
    # Process and copy the material list for each component
    material_slugs = []
    for material_file in glob(input_path + '/*.materials.csv'):
        input_df = pd.read_csv(material_file)
        for material in input_df['Material.slug']:
            if material not in material_slugs:
                material_slugs.append(material)
    materials = pd.DataFrame(material_slugs, columns=['slug'])
    materials.sort_values("slug", inplace=True)
    materials['name'] = materials['slug'].str.replace('_', ' ').str.title()
    materials.to_csv(materials_file, index=False)


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
