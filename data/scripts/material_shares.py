#!/usr/bin/env python
"""
Generates `materials_shares.csv` from material share data.

All CSV files in the `material_shares` directory are read. The CSV file name is
expected to correspond to the slug of a material (as in `materials.csv`). A
comprehensive output file for data import is then generated.
"""

import click
import pandas as pd
from pathlib import Path
import re


@click.command()
@click.argument('input_dir', type=click.Path(exists=True), default='input/material_shares')
@click.argument('output_file', type=click.Path(), default='output/material_shares.csv')
def main(input_dir, output_file):
    # Make an empty output data frame, to which we then append the contents of each input file
    output_df = pd.DataFrame(columns=['Country.name', 'Material.slug', 'amount'])

    input_path = Path(input_dir)
    for csv_file in input_path.glob('*.csv'):
        input_df = read_share_file(csv_file)
        output_df = output_df.append(input_df)

    output_df.to_csv(output_file, index=False)


def read_share_file(csv_file):
    df = pd.read_csv(csv_file,
                     header=None,
                     names=['Country.name','absolute','share','percent'])
    # Filter out NAs
    df.dropna(inplace=True)
    # Filter out “other countries”
    df = df[df['Country.name'] != 'Other countries']
    material_slug = re.match(r'(.+)\.csv', csv_file.name).groups(1)[0]
    df = df.assign(**{'Material.slug': lambda x: material_slug}) 
    # Recompute the amount anyway
    df['amount'] = df['absolute'] / df['absolute'].sum()
    return df[['Country.name', 'Material.slug', 'amount']]


if __name__ == '__main__':
    main()  # pylint: disable=no-value-for-parameter
