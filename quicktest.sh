#!/usr/bin/env bash

# Runs quick tests that can be executed locally.

set -e

SCRIPT_PATH=`dirname "$0"`
SCRIPT_PATH=`( cd "$SCRIPT_PATH" && pwd )`

function finish {
    docker-compose down -v
}

trap finish EXIT

export COMPOSE_PROJECT_NAME=pse-test
docker-compose run --rm start_postgres
docker-compose up --build -d backend

docker-compose exec -T backend pytest

docker-compose down
