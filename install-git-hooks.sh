#!/usr/bin/env bash

# Installs git hooks that run tests locally before a commit can be made.

set -e

SCRIPT_PATH=`dirname "$0"`
SCRIPT_PATH=`( cd "$SCRIPT_PATH" && pwd )`

cp "${SCRIPT_PATH}"/git-hooks/* "${SCRIPT_PATH}/.git/hooks/"
